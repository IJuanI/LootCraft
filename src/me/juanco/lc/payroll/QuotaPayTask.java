package me.juanco.lc.payroll;

import java.util.Set;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.core.Main;
import me.juanco.lc.payroll.objects.PendientQuota;
import me.juanco.lc.payroll.objects.QuotaStack;

public class QuotaPayTask extends LootTask {

	private Set<PendientQuota> pendient = new ConcurrentSet<PendientQuota>();
	private final Main core;

	protected QuotaPayTask(Main core) {
		super(60, true, true);

		this.core = core;
	}

	@Override
	public void run() {
		for (PendientQuota quota : pendient) {
			if (quota.flush())
				pendient.remove(quota);
			Player player = quota.getPlayer();
			if (player != null) {
				core.getPlaceholders().callPlaceholder("payroll_amount", player);
				core.getPlaceholders().callPlaceholder("payroll_time", player);
			}
		}
	}

	public void addQuota(OfflinePlayer target, QuotaStack quota) {
		PendientQuota q = get(target);
		if (q == null)
			pendient.add(new PendientQuota(target, quota));
		else
			q.addQuota(quota);
	}

	public void clear() {
		pendient.clear();
	}

	void save() {
		for (PendientQuota quota : pendient)
			quota.save();
	}

	int getIndex(OfflinePlayer target) {
		return Main.ioUtils.indexOf(pendient, target);
	}

	PendientQuota get(OfflinePlayer target) {
		return Main.ioUtils.get(pendient, target);
	}
}

package me.juanco.lc.payroll;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.payroll.objects.PendientQuota;
import me.juanco.lc.payroll.objects.Quota;
import me.juanco.lc.payroll.objects.QuotaStack;

public class Payrolls {

	private final QuotaPayTask payTask;

	private List<Quota> quotas = new ArrayList<Quota>();

	public Payrolls(Main core) {
		loadQuotas();

		payTask = new QuotaPayTask(core);
		LootClock.addTask(payTask);

		resumePendient();
	}

	public void onDisable() {
		payTask.save();
	}

	public QuotaPayTask getPayTask() {
		return payTask;
	}

	void loadQuotas() {
		for (String id : Files.payroll.config.file.getConfigurationSection("Quotas").getKeys(false))
			quotas.add(new Quota(id));
	}

	void resumePendient() {
		for (String id : Files.payroll.temp.file.getKeys(false)) {
			OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(id));
			for (String q : Files.payroll.temp.file.getStringList(id)) {
				String[] props = q.split(", ");
				Quota quota = getQuota(props[0]);
				if (quota != null)
					payTask.addQuota(player, new QuotaStack(quota, Integer.parseInt(props[1])));
			}
			Files.payroll.temp.file.set(id, null);
		}
		Files.payroll.temp.save();
	}

	public Quota getQuota(String id) {
		return Main.ioUtils.get(quotas, id);
	}

	public int nextPayout(OfflinePlayer player) {
		PendientQuota quota = payTask.get(player);
		if (quota == null)
			return 0;
		else
			return quota.getNextPayout();
	}

	public int quotasLeft(OfflinePlayer player) {
		PendientQuota quota = payTask.get(player);
		if (quota == null)
			return 0;
		else
			return quota.getQuotasLeft();
	}
}

package me.juanco.lc.payroll.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;

public class PendientQuota {

	OfflinePlayer player;
	Set<QuotaStack> quotas = new ConcurrentSet<QuotaStack>();
	Message messager;

	public PendientQuota(OfflinePlayer target, QuotaStack stack) {
		player = target;
		messager = target.isOnline() ? new Message(Bukkit.getPlayer(target.getUniqueId())) : null;
		quotas.add(stack);
	}

	public void addQuota(QuotaStack stack) {
		QuotaStack quota = Main.ioUtils.get(quotas, stack);

		if (quota == null)
			quotas.add(stack);
		else
			quota.overStock(stack.getStock());
	}

	public boolean flush() {
		if (messager == null && player.isOnline())
			messager = new Message(Bukkit.getPlayer(player.getUniqueId()));

		boolean lock = messager == null;

		int payout = 0, stock = 0;

		for (QuotaStack stack : quotas) {
			if (!lock)
				stock = stack.getStock();

			if (!stack.pay(player))
				quotas.remove(stack);

			if (!lock && stock != stack.getStock())
				payout += stack.getPayout();
		}

		if (!lock)
			messager.msgWithReplaces(Messages.SUCCESS.payQuota, "%n", String.valueOf(payout));

		return quotas.isEmpty();
	}

	public int getNextPayout() {
		int payout = 0;
		for (QuotaStack stack : quotas)
			payout += stack.getPayout();
		return payout;
	}

	public int getQuotasLeft() {
		int left = 0;
		for (QuotaStack stack : quotas)
			if (stack.getStock() > left)
				left = stack.getStock();
		return left;
	}

	public Player getPlayer() {
		return player.getPlayer();
	}

	public void save() {
		List<String> q = new ArrayList<String>();
		for (QuotaStack quota : quotas)
			q.add(quota.toString());

		Files.payroll.temp.file.set(player.getUniqueId().toString(), q);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof OfflinePlayer)
			return player.equals(obj);
		return super.equals(obj);
	}
}

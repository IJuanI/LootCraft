package me.juanco.lc.payroll.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;

public class Quota {

	private final List<ItemStack> payment;
	private final String id;
	private int payout;

	public Quota(String id) {
		ConfigurationSection sec = Files.payroll.config.file.getConfigurationSection("Quotas");

		List<ItemStack> payment = new ArrayList<ItemStack>();
		if (sec.isConfigurationSection(id)) {
			sec = sec.getConfigurationSection(id);
			payout = sec.getInt("payout");
			for (String pay : sec.getStringList("cost")) {
				String[] s1 = pay.split("#");
				int amount = Integer.parseInt(s1[1]);

				if (s1[0].contains(":")) {
					String[] s2 = s1[0].split(":");
					payment.add(new ItemStack(Material.matchMaterial(s2[0]), amount, Short.parseShort(s2[1])));
				} else
					payment.add(new ItemStack(Material.matchMaterial(s1[0]), amount));
			}
		} else {
			sec = sec.createSection(id);
			payout = -1;
			payment.add(new ItemStack(Material.STONE));
		}

		this.id = id.startsWith("Quota ") ? id.substring(6) : id;
		this.payment = payment;
	}

	public int getPayout() {
		return payout;
	}

	public String getID() {
		return id;
	}

	public boolean pay(OfflinePlayer player) {
		return Main.money.add(player, payout);
	}

	List<ItemStack> check(Player player, boolean remove) {
		List<ItemStack> needed = new ArrayList<ItemStack>();

		for (ItemStack item : payment)
			needed.add(item.clone());

		int index = -1;
		for (ItemStack item : player.getInventory()) {
			index++;
			if (item == null || item.getType().equals(Material.AIR))
				continue;

			ItemStack need = getSimilar(needed, item);
			if (need != null)
				if (need.getAmount() > item.getAmount()) {
					need.setAmount(need.getAmount() - item.getAmount());
					if (remove)
						player.getInventory().setItem(index, null);
					;
				} else if (need.getAmount() == item.getAmount()) {
					needed.remove(need);
					if (remove)
						player.getInventory().setItem(index, null);
				} else {
					if (remove)
						item.setAmount(item.getAmount() - need.getAmount());
					needed.remove(need);
				}
		}

		return needed;
	}

	private ItemStack getSimilar(Iterable<ItemStack> items, ItemStack item) {
		for (ItemStack i : items)
			if (i.isSimilar(item))
				return i;
		return null;
	}

	public List<ItemStack> withdraw(Player player) {
		List<ItemStack> needed = check(player, false);

		if (needed.isEmpty())
			return check(player, true);

		return needed;
	}

	public void save() {
		ConfigurationSection sec = Files.payroll.config.file.getConfigurationSection("Quotas");
		if (!sec.isConfigurationSection(id))
			sec = sec.createSection(id);
		else
			sec = sec.getConfigurationSection(id);

		List<String> cost = new ArrayList<String>();

		for (ItemStack item : payment)
			cost.add(new StringBuilder(item.getType().toString()).append(":").append(item.getDurability()).append("#")
					.append(item.getAmount()).toString());

		sec.set("payout", payout);
		sec.set("cost", cost);
		Files.payroll.config.save();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return id.equals(obj) || String.valueOf(payout).equals(obj);
		else if (obj instanceof Integer)
			return obj.equals(payout);
		return super.equals(obj);
	}
}

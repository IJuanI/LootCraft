package me.juanco.lc.payroll.objects;

import org.bukkit.OfflinePlayer;

public class QuotaStack {

	private Quota quota;
	private int stock;

	public QuotaStack(Quota quota) {
		this(quota, 1);
	}
	
	public QuotaStack(Quota quota, int stock) {
		this.quota = quota;
		this.stock = stock;
	}
	
	public void overStock() {
		stock++;
	}

	protected void overStock(int amount) {
		stock += amount;
	}

	protected boolean pay(OfflinePlayer player) {
		if (quota.pay(player))
			stock--;
		return stock > 0;
	}

	protected int getStock() {
		return stock;
	}

	protected int getPayout() {
		return quota.getPayout();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Quota)
			return quota.equals(obj);
		else if (obj instanceof QuotaStack)
			return quota.equals(((QuotaStack) obj).quota);
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return quota.getID() + ", " + stock;
	}
}

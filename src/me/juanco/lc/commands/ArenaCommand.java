package me.juanco.lc.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;

public class ArenaCommand extends LootExecutor {

	public ArenaCommand(Main pl) {
		super("arena", pl, "arenas", "a");
		addSub(new SubCommand("create", Permissions.admin));
		addSub(new SubCommand("setwarp", Permissions.admin));
		addSub(new SubCommand("delete", Permissions.admin));
		addSub(new SubCommand("help", Permissions.admin));
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (sub == null && !executor.hasPermission(Permissions.admin) || sub != null && sub.isNull)
			message.msg(Messages.ERRORS.unknownCommand);
		else if (executor instanceof Player) {
			if (sub == null || sub.equals("help"))
				showHelp(message);
			else if (args.length < 1)
				message.msg(Messages.ERRORS.argument);
			else if (sub.equals("create")) {
				FileConfiguration conf = Files.matches.file;
				String id = args[0];

				if (conf.isConfigurationSection("Arenas." + id))
					message.msg(Messages.ALERTS.ARENA.exists);
				else {
					conf.createSection("Arenas." + id);
					Files.matches.save();

					message.msg(Messages.ALERTS.ARENA.create);
				}
			} else if (sub.equals("delete")) {
				FileConfiguration conf = Files.matches.file;
				String id = args[0];

				if (conf.isConfigurationSection("Arenas." + id)) {
					conf.set("Arenas." + id, null);
					core.getMatches().getManager().deleteArena(id);
					Files.matches.save();

					message.msg(Messages.ALERTS.ARENA.delete);
				} else
					message.msg(Messages.ALERTS.ARENA.notExists);
			} else if (args.length < 2)
				message.msg(Messages.ERRORS.argument);
			else if (!isInt(args[1]))
				message.msg(Messages.ERRORS.wrongArg);
			else if (sub.equals("setwarp")) {
				FileConfiguration conf = Files.matches.file;
				String id = args[0];
				int warp = Integer.parseInt(args[1]);
				if (warp < 0)
					warp = 0;
				else if (warp > 1)
					warp = 1;

				if (conf.isConfigurationSection("Arenas." + id)) {
					core.getUtils().saveLoc(((Player) executor).getLocation(),
							conf.getConfigurationSection("Arenas." + id), String.valueOf(warp));

					Files.matches.save();

					message.msg(Messages.ALERTS.ARENA.warp);
				} else
					message.msg(Messages.ALERTS.ARENA.notExists);

			}

		} else
			message.msg(Messages.ERRORS.permission);
	}

	private void showHelp(Message message) {
		message.rawMsg("", "&6&l/arena create <id>&7: &bCreates an Arena",
				"&e&l/arena setwarp <id> <n>&7: &dSets an arena warp", "&6&l/arena delete <id>&7: &bDelete an Arena");
	}

	private boolean isInt(String arg) {
		try {
			Integer.parseInt(arg);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}

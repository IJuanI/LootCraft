package me.juanco.lc.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.Chunk;
import net.minecraft.server.v1_8_R3.ChunkCoordIntPair;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.EnumSkyBlock;

public class LightCommand extends LootExecutor {

	public LightCommand(Main core) {
		super("light", core);

		addSub(new SubCommand("add", Permissions.admin));
		addSub(new SubCommand("remove", Permissions.admin));
		addSub(new SubCommand("update", Permissions.admin));

		for (String id : Files.light.file.getKeys(false))
			locs.add(core.getUtils().loadLoc(Files.light.file.getString(id)));
	}

	public final static List<Location> locs = new ArrayList<Location>();

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (!executor.hasPermission(Permissions.admin) || !(executor instanceof Player))
			message.msg(Messages.ERRORS.permission);
		else if (sub == null || !sub.equals("update") && args.length < 1)
			message.msg(Messages.ERRORS.argument);
		else if (sub.isNull)
			message.msg(Messages.ERRORS.wrongArg);
		else {
			String id = args.length > 0 ? args[0] : null;
			boolean exists = args.length > 0 ? Files.light.file.getString(id) != null : false;

			if (sub.equals("add"))
				if (exists)
					message.msgWithReplaces(Messages.ERRORS.exists, "%thing", "light");
				else {
					core.getUtils().saveLoc(((Player) executor).getLocation().getBlock().getLocation(),
							Files.light.file, id);
					Files.light.save();

					locs.add(((Player) executor).getLocation().getBlock().getLocation());

					message.msg(Messages.SUCCESS.addLight);
				}
			else if (sub.equals("remove"))
				if (!exists)
					message.msgWithReplaces(Messages.ERRORS.notExists, "%thing", "light");
				else {
					Files.light.file.set(id, null);
					Files.light.save();

					locs.remove(((Player) executor).getLocation().getBlock().getLocation());

					message.msg(Messages.SUCCESS.removeLight);
				}
			else if (sub.equals("update")) {
				List<Chunk> chunks = new ArrayList<Chunk>();
				
				for (Location loc : locs)
					if (loc.getChunk().isLoaded()) {
						Chunk chunk = ((CraftChunk)loc.getChunk()).getHandle();
						if (!chunks.contains(chunk))
							chunks.add(chunk);

						chunk.a(EnumSkyBlock.BLOCK, new BlockPosition(loc.getX(), loc.getY(), loc.getZ()), 15);
					}
				
				for (Chunk chunk : chunks)
					for (Player player : Bukkit.getOnlinePlayers()) {
						EntityPlayer entity = ((CraftPlayer) player).getHandle();
						if (entity.world.equals(chunk.world))
							entity.chunkCoordIntPairQueue.add(new ChunkCoordIntPair(chunk.locX, chunk.locZ));
					}
			}
		}
	}
}

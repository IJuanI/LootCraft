package me.juanco.lc.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.kits.LootKit;

public class KitsCommand extends LootExecutor {

	public KitsCommand(Main pl) {
		super("kits", pl, "kit");
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (!executor.hasPermission(Permissions.kit) || !(executor instanceof Player))
			message.msg(Messages.ERRORS.permission);
		else if (sub == null)
			message.msg(Messages.ERRORS.kit);
		else {
			LootKit kit = core.getKits().getKit(args[0].toLowerCase());

			if (kit == null)
				message.msg(Messages.ERRORS.kit);
			else {
				((Player) executor).getInventory().addItem(kit.getItem());
				message.msg(Messages.SUCCESS.kit);
			}
		}
	}

}

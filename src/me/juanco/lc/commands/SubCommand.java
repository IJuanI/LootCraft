package me.juanco.lc.commands;

import org.bukkit.command.CommandSender;

public class SubCommand {

	public static final SubCommand NULL = new SubCommand();
	public final String name, permission;
	public boolean isNull;

	public SubCommand(String name, String permission) {
		this.name = name.toLowerCase();
		this.permission = permission;
	}

	private SubCommand() {
		name = null;
		permission = null;
		isNull = true;
	}

	public boolean hasPermission(CommandSender sender) {
		return sender.isOp() || sender.hasPermission(permission);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return name.equals(((String) obj).toLowerCase());
		return super.equals(obj);
	}
}

package me.juanco.lc.commands.economy;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.commands.LootExecutor;
import me.juanco.lc.commands.SubCommand;
import me.juanco.lc.core.Main;

public class MoneyCommand extends LootExecutor {

	public MoneyCommand(Main pl) {
		super("money", pl, "balance", "bal");
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (executor instanceof Player)
			message.msgWithReplaces(Messages.SUCCESS.ECONOMY.get, "%player", executor.getName(), "%currency", "Money",
					"%n", String.valueOf(Main.money.getAmount((Player) executor)));
		else
			message.msg(Messages.ERRORS.permission);
	}
}

package me.juanco.lc.commands.economy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import com.google.common.collect.Lists;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.commands.LootExecutor;
import me.juanco.lc.commands.SubCommand;
import me.juanco.lc.core.Main;
import me.juanco.lc.economy.Currency;
import me.juanco.lc.economy.Economy;

public class EconomyCommand extends LootExecutor {

	public EconomyCommand(Main pl) {
		super("econ", pl, "economy");

		addSub(new SubCommand("get", null));
		addSub(new SubCommand("add", null));
		addSub(new SubCommand("remove", null));
		addSub(new SubCommand("set", null));
		addSub(new SubCommand("help", null));
	}

	private final List<String> currencies = Lists.newArrayList("tokens", "money");

	@SuppressWarnings("deprecation")
	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (!executor.hasPermission(Permissions.admin))
			message.msg(Messages.ERRORS.permission);
		else if (sub == null || sub.isNull || sub.equals("help"))
			showHelp(message);
		else if (args.length < 2)
			message.msg(Messages.ERRORS.argument);
		else if (!currencies.contains(args[0].toLowerCase()))
			message.msg(Messages.ERRORS.wrongArg);
		else {
			Currency currency = Currency.valueOf(StringUtils.capitalize(args[0]));
			OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);

			if (target == null)
				message.msg(Messages.ERRORS.target);
			else if (sub.equals("get")) {
				message.msgWithReplaces(Messages.SUCCESS.ECONOMY.get, "%player", target.getName(), "%currency",
						currency.name(), "%n", String.valueOf(fromCurrency(currency).getAmount(target)));
			} else if (args.length < 3)
				message.msg(Messages.ERRORS.argument);
			else if (isInt(args[2])) {
				int amount = Integer.parseInt(args[2]);
				Economy econ = fromCurrency(currency);

				switch (sub.name.toLowerCase()) {
				case "add":
					if (econ.add(target, amount))
						message.msgWithReplaces(Messages.SUCCESS.ECONOMY.add, "%player", target.getName(), "%currency",
								currency.name(), "%n", String.valueOf(amount));
					else
						message.msg(Messages.ERRORS.economy);
					break;
				case "remove":
					if (econ.withdraw(target, amount))
						message.msgWithReplaces(Messages.SUCCESS.ECONOMY.remove, "%player", target.getName(),
								"%currency", currency.name(), "%n", String.valueOf(amount));
					else
						message.msg(Messages.ERRORS.economy);
					break;
				case "set":
					if (econ.set(target, amount))
						message.msgWithReplaces(Messages.SUCCESS.ECONOMY.set, "%player", target.getName(), "%currency",
								currency.name(), "%n", String.valueOf(amount));
					else
						message.msg(Messages.ERRORS.economy);
					econ.set(target, amount);
					break;
				}

			} else
				message.msg(Messages.ERRORS.wrongArg);
		}
	}

	void showHelp(Message message) {
		message.rawMsg("", "&6&l/econ get C T&7: &bGet Player Money", "&e&l/econ add C T A&7: &dAdds Money",
				"&6&l/econ remove C T A&7: &bWithdraws Money", "&e&l/econ set C T A&7: &dSet Money",
				"&6&l/econ help&7: &bShows this page", "&c* &2&lC &2= &2&lMoney&2|&2&lTokens",
				"&c* &2&lT &2= &2&lTarget", "&c* &2&lA &2= &2&lAmount", "");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission(Permissions.admin))
			return new ArrayList<String>();
		else {
			List<String> possibilities;
			String arg = args[0].toLowerCase();

			switch (args.length) {
			case 1:
				possibilities = Lists.newArrayList("Get", "Set", "Add", "Remove", "Help");
				break;
			case 2:
				if (arg.equals("help"))
					possibilities = new ArrayList<String>();
				else
					possibilities = Lists.newArrayList("Tokens", "Money");
				break;
			case 3:
				possibilities = null;
				break;
			case 4:
				if (arg.equals("help") || args.equals("get"))
					possibilities = new ArrayList<String>();
				else
					possibilities = Lists.newArrayList("0", "1", "5", "10", "20", "50", "100");
				break;
			default:
				possibilities = new ArrayList<String>();
				break;
			}

			if (possibilities == null) {
				String lastWord = args[args.length - 1];

				Player executor = sender instanceof Player ? (Player) sender : null;

				ArrayList<String> matched = new ArrayList<String>();
				for (Player player : sender.getServer().getOnlinePlayers()) {
					String name = player.getName();
					if ((sender == null || executor.canSee(player)) && StringUtil.startsWithIgnoreCase(name, lastWord))
						matched.add(name);
				}

				Collections.sort(matched, String.CASE_INSENSITIVE_ORDER);

				return matched;
			} else if (possibilities.isEmpty())
				return possibilities;

			String last = args[args.length - 1];
			List<String> sorted = new ArrayList<String>();

			for (String possibility : possibilities)
				if (StringUtil.startsWithIgnoreCase(possibility, last))
					sorted.add(possibility);

			Collections.sort(sorted, String.CASE_INSENSITIVE_ORDER);

			return sorted;
		}
	}

	Economy fromCurrency(Currency currency) {
		if (currency.equals(Currency.Tokens))
			return Main.tokens;
		return Main.money;
	}

	boolean isInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}

package me.juanco.lc.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;

import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;

public abstract class LootExecutor implements CommandExecutor, TabCompleter {

	List<SubCommand> subs = new ArrayList<SubCommand>();
	public Main core;

	public LootExecutor(String command, Main pl, String... aliases) {
		PluginCommand cmd = pl.getCommand(command);
		cmd.setExecutor(this);
		cmd.setTabCompleter(this);
		core = pl;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length < 1)
			onCommand(sender, new Message(sender), null, args);
		else {
			int index = Main.ioUtils.indexOf(subs, args[0]);
			SubCommand sub;

			if (index < 0)
				sub = SubCommand.NULL;
			else {
				sub = subs.get(index);
				if (!sub.hasPermission(sender))
					return false;

				String[] newArgs = new String[args.length - 1];
				for (int i = 1; i < args.length; i++)
					newArgs[i - 1] = args[i];
				args = newArgs;
			}

			onCommand(sender, new Message(sender), sub, args);
		}
		return true;
	}

	public abstract void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args);

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		return null;
	}

	public void addSub(SubCommand sub) {
		subs.add(sub);
	}
}

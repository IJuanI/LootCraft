package me.juanco.lc.commands;

import java.text.DecimalFormat;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;

public class HologramsCommand extends LootExecutor {

	public HologramsCommand(Main core) {
		super("holo", core);
		addSub(new SubCommand("list", Permissions.admin));
		addSub(new SubCommand("help", Permissions.admin));

		addSub(new SubCommand("create", Permissions.admin));
		addSub(new SubCommand("delete", Permissions.admin));

		addSub(new SubCommand("addline", Permissions.admin));
		addSub(new SubCommand("removeline", Permissions.admin));

		addSub(new SubCommand("setcommand", Permissions.admin));

		addSub(new SubCommand("clone", Permissions.admin));
		addSub(new SubCommand("movehere", Permissions.admin));
		addSub(new SubCommand("moveto", Permissions.admin));
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (sub == null || sub.isNull || sub.equals("help"))
			showHelp(message);
		else if (sub.equals("list")) {
			int page = 0;
			if (args.length > 0 && Main.ioUtils.isInt(args[0]))
				page = Integer.parseInt(args[0]);

			World world = Bukkit.getWorlds().get(0);
			if (executor instanceof Player)
				world = ((Player) executor).getWorld();

			List<String> list = core.getHolograms().getHolos(world);
			int pages = list.size() > 1 ? (list.size() - 1) / 18 + 1 : list.size();

			if (page < 1)
				page = 1;
			else if (page > pages)
				page = pages;

			DecimalFormat format = new DecimalFormat("00");

			message.rawMsg("&b****************************************************************");
			for (int i = 0; i < 18 && (page - 1) * 18 + i < list.size(); i++)
				message.rawMsg(list.get((page - 1) * 18 + i));
			message.rawMsg(String.format("&b**************************** &c%s/%s &b****************************",
					format.format(page), format.format(pages)));
		} else if (args.length < 1)
			message.msg(Messages.ERRORS.argument);
		else if (sub.equals("create"))
			if (!(executor instanceof Player))
				message.msg(Messages.ERRORS.permission);
			else if (core.getHolograms().exists(args[0]))
				message.msg(Messages.ALERTS.HOLO.exists);
			else {
				core.getHolograms().create(((Player) executor).getLocation(), args[0]);
				message.msg(Messages.ALERTS.HOLO.create);
			}
		else if (sub.equals("moveto")) {
			Location loc = core.getHolograms().getLoc(args[0]);

			if (executor instanceof Player)
				((Player) executor).teleport(loc);
		} else if (!core.getHolograms().exists(args[0]))
			message.msg(Messages.ALERTS.HOLO.notExists);
		else if (sub.equals("delete")) {
			core.getHolograms().delete(args[0]);
			message.msg(Messages.ALERTS.HOLO.delete);
		} else if (sub.equals("movehere")) {
			Location loc = ((Player) executor).getLocation();
			loc.setY(loc.getY() + 1);

			core.getHolograms().teleport(loc, args[0]);
		} else if (args.length < 2)
			message.msg(Messages.ERRORS.argument);
		else if (sub.equals("clone"))
			if (!core.getHolograms().exists(args[1])) {
				core.getHolograms().clone(args[0], args[1], ((Player) executor).getLocation());
				message.msg(Messages.ALERTS.HOLO.delete);
			} else
				message.msg(Messages.ALERTS.HOLO.exists);
		else if (sub.equals("addline")) {
			int index = -1;

			if (args[1].startsWith(".") && Main.ioUtils.isInt(args[1].substring(1)))
				if (args.length < 3) {
					message.msg(Messages.ERRORS.wrongArg);
					return;
				} else
					index = Integer.parseInt(args[1].substring(1));

			int start = index < 0 ? 1 : 2;

			StringBuilder msg = new StringBuilder(args[start]);
			for (int i = start + 1; i < args.length; i++)
				msg.append(" ").append(args[i]);

			if (index < 0)
				core.getHolograms().addLine(args[0], msg.toString());
			else
				core.getHolograms().insertLine(args[0], msg.toString(), index);

			message.msg(Messages.ALERTS.HOLO.addLine);
		} else if (sub.equals("removeline"))
			if (Main.ioUtils.isInt(args[1])) {
				core.getHolograms().removeLine(args[0], Integer.parseInt(args[1]));
				message.msg(Messages.ALERTS.HOLO.removeLine);
			} else
				message.msg(Messages.ERRORS.wrongArg);
		else if (sub.equals("setcommand")) {
			boolean console = false;

			if (args[2].toLowerCase().equals("console"))
				if (args.length < 3) {
					message.msg(Messages.ERRORS.wrongArg);
					return;
				} else
					console = true;

			int start = console ? 2 : 1;
			StringBuilder command = new StringBuilder();
			if (console)
				command.append(">.<");
			command.append(args[start]);

			if (args.length > start + 1)
				for (int index = start + 1; index < args.length; index++)
					command.append(" ").append(args[index]);

			core.getHolograms().setCommand(args[0], command.toString());
			message.msg(Messages.ALERTS.HOLO.command);
		}
	}

	void showHelp(Message message) {
		message.rawMsg("&3/hd list (nPage)&2: &fList all holograms", "&3/hd help&2: &fShows this page",
				"&2/hd create <id>&3: &7Creates an hologram", "&2/hd delete <id>&3: &7Deletes an hologram",
				"&3/hd addLine <id> (.line) <msg>&2: &fRemember that dot",
				"&3/hd removeLine <id> <line>&2: &fRemoves a line",
				"&2/hd setCommand <id> (CONSOLE) <Command>&4: &7Bind a command",
				"&3/hd moveHere <id>&2: &fMoves an hologram to your loc",
				"&3/hd moveTo <id>&2: &fMoves you to an hologram",
				"&3/hd clone <id> <newID>&2: &fClones an hologram to your loc");
	}
}

package me.juanco.lc.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.match.objects.MatchUser;

public class MatchCommand extends LootExecutor {

	public MatchCommand(Main pl) {
		super("pvp", pl, "playervsplayer", "mm", "matchmaking", "1vs1", "am", "automatch");
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (!(executor instanceof Player))
			message.msg(Messages.ERRORS.permission);
		else {
			MatchUser user = core.getMatches().getUser((Player) executor);
			if (user.isFighting())
				message.msg(Messages.ALERTS.PVP.combat);
			else
				core.getMatches().getManager().switchQueue(user);
		}
	}

}

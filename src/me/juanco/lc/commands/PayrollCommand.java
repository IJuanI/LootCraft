package me.juanco.lc.commands;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.payroll.objects.Quota;
import me.juanco.lc.payroll.objects.QuotaStack;

public class PayrollCommand extends LootExecutor {

	public PayrollCommand(Main plugin) {
		super("payroll", plugin, "payrolls", "pr");
		addSub(new SubCommand("forcepay", Permissions.admin));
		addSub(new SubCommand("clearpays", Permissions.admin));
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (sub == null)
			message.msg(Messages.ERRORS.unknownCommand);
		else if (sub.isNull) {
			CommandSender target = executor;

			Quota quota = core.getPayrolls().getQuota(args[0]);
			
			if (quota == null) {
				message.msg(Messages.ERRORS.quotaFind);
				return;
			}
			
			if (args.length > 1)
				target = Bukkit.getPlayer(args[1]);

			if (target == null)
				message.msg(Messages.ERRORS.target);
			else if (!(target instanceof Player
					&& (executor.hasPermission(Permissions.payroll) && executor.equals(target)
							|| executor.hasPermission(Permissions.payroll_others))))
				message.msg(Messages.ERRORS.permission);
			else {

				int reps = -1;
				List<ItemStack> left = null;
				while (left == null || left.isEmpty()) {
					left = quota.withdraw((Player) target);
					reps++;
				}

				if (reps > 0) {
					core.getPayrolls().getPayTask().addQuota((Player) target, new QuotaStack(quota, reps));
					Message.sendWithReplaces(Messages.SUCCESS.addQuota, target, "%n", String.valueOf(reps), "(s)",
							reps > 1 ? "s" : "");
					core.getPlaceholders().callPlaceholder("payroll_amount", (Player) target);
					core.getPlaceholders().callPlaceholder("payroll_time", (Player) target);
				} else {
					StringBuilder leftItems = new StringBuilder();

					for (ItemStack item : left) {
						leftItems.append(", ");
						leftItems.append(item.getAmount()).append(" ");
						if (item.getType().equals(Material.RED_ROSE) && item.getDurability() > 0
								&& (item.getDurability() == 1 || item.getDurability() == 8)) {
							if (item.getDurability() == 1)
								leftItems.append("Orchid");
							else
								leftItems.append("Daisy");
						} else {
							leftItems.append(Main.ioUtils.capitalize(item.getType().toString()));
							if (item.getDurability() > 0)
								leftItems.append(":").append(item.getDurability());
						}
					}

					Message.sendWithReplaces(Messages.ERRORS.quotaNeed, target, "%left", leftItems.substring(2));
				}
			}


		} else if (sub.equals("forcepay"))
			core.getPayrolls().getPayTask().run();
		else if (sub.equals("clearpays")) {
			core.getPayrolls().getPayTask().clear();
			message.msg(Messages.SUCCESS.clearQuota);
		} else
			message.msg(Messages.ERRORS.unknownCommand);
	}
}

package me.juanco.lc.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.kits.Kits;
import me.juanco.lc.trades.Trades;

public class TradeCommand extends LootExecutor {

	public TradeCommand(Main pl) {
		super("trade", pl);

		addSub(new SubCommand("accept", null));
		addSub(new SubCommand("deny", null));
		addSub(new SubCommand("cancel", null));
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (executor instanceof Player) {
			int level = Kits.getUser((Player)executor).getLevel();
			if (level < Trades.reqLevel)
				message.msgWithReplaces(Messages.ALERTS.TRADE.level, "%level", String.valueOf(Trades.reqLevel));
			else if (sub == null)
				message.msg(Messages.ERRORS.specifyTarget);
			else if (sub.isNull)
				trade(executor, message, args[0]);
			else if (sub.equals("accept"))
				core.getTrades().accept((Player) executor);
			else if (sub.equals("deny"))
				core.getTrades().deny((Player) executor);
			else if (sub.equals("cancel"))
				core.getTrades().cancelRequest((Player) executor);
			else
				trade(executor, message, args[0]);
		} else
			message.msg(Messages.ERRORS.permission);
	}

	private void trade(CommandSender executor, Message message, String t) {
		Player target = Bukkit.getPlayer(t);
		if (target == null)
			message.msg(Messages.ERRORS.target);
		else if (target.equals(executor))
			message.msg(Messages.ERRORS.target);
		else
			core.getTrades().trade((Player) executor, target);
	}

}

package me.juanco.lc.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.match.objects.MatchUser;

public class VirtualCommand extends LootExecutor {

	public VirtualCommand(Main pl) {
		super("virtual", pl, "chest", "inv");
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (executor instanceof Player) {
			Player player = (Player) executor;
			MatchUser user = core.getMatches().getUser(player);
			if (user.isFighting() || user.isWaiting())
				message.msg(Messages.ERRORS.permission);
			else
				core.getVirtual().getChest(0).open((Player) executor);
		} else
			message.msg(Messages.ERRORS.permission);
	}

}

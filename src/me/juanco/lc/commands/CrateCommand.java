package me.juanco.lc.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.crates.CrateType;
import me.juanco.lc.crates.objects.CrateAdmin;

public class CrateCommand extends LootExecutor {

	public CrateCommand(Main plugin) {
		super("crate", plugin, "crate", "crates", "lootcrates");
		for (CrateType type : CrateType.values())
			addSub(new SubCommand(type.toString(), Permissions.crate));
		addSub(new SubCommand("chest", Permissions.admin));
	}

	@Override
	public void onCommand(CommandSender executor, Message message, SubCommand sub, String[] args) {
		if (sub == null || sub.isNull)
			message.msg(Messages.ERRORS.unknownCommand);
		else if (sub.equals("chest") && executor instanceof Player) {
			core.getCrates().addAdmin(new CrateAdmin((Player) executor));
			message.msg(Messages.ALERTS.CCTuto);
		} else {
			CrateType type = CrateType.valueOf(sub.name.toLowerCase());

			Player target = executor instanceof Player ? (Player) executor : null;
			if (args.length > 0)
				target = Bukkit.getPlayer(args[0]);

			if (target == null)
				message.msg(Messages.ERRORS.target);
			else if (sub.equals("common") && !target.hasPermission(Permissions.admin))
				core.getCrates().getDisplayer().preCommon(target);
			else if (!executor.hasPermission(Permissions.admin))
				message.msg(Messages.ERRORS.permission);
			else {
				ItemStack crate = type.crate.clone();
				if (args.length > 1)
					try {
						crate.setAmount(Integer.parseInt(args[1]));
					} catch (Exception ex) {
					}
				
				int left = Main.itemUtils.giveItems(target, crate);
				if (left > 0)
					message.msgWithReplaces(Messages.ERRORS.itemsDropped, "%n", String.valueOf(left), "(s)",
							left > 1 ? "s" : "");
			}
		}

		return;
	}
}

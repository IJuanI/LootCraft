package me.juanco.lc.holograms;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.holograms.listeners.HoloChunkListener;
import me.juanco.lc.holograms.listeners.HoloClickListener;
import me.juanco.lc.holograms.listeners.HoloFrameListener;
import me.juanco.lc.holograms.objects.Hologram;
import me.juanco.lc.holograms.objects.HologramLine;
import me.juanco.lc.holograms.objects.LootLine;
import me.juanco.lc.packets.PacketHandler;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityTypes;
import net.minecraft.server.v1_8_R3.World;

public class Holograms {

	private static List<Hologram> holograms = new ArrayList<Hologram>();

	private final Main core;

	public static void register() {
		try {
			putInPrivateStaticMap(EntityTypes.class, "d", HologramLine.class, "ArmorStand");
			putInPrivateStaticMap(EntityTypes.class, "f", HologramLine.class, Integer.valueOf(30));

			Method validateEntityMethod = World.class.getDeclaredMethod("a", Entity.class);
			validateEntityMethod.setAccessible(true);

			LootLine.setValidateMethod(validateEntityMethod);
		} catch (Exception ex) {
			System.out.println("Couldn't load holograms!");
			throw new RuntimeException(ex);
		}
	}

	public Holograms(Main core) {
		this.core = core;
		load();

		PacketHandler.register(new HoloClickListener("PacketPlayInArmAnimation", core));
		PacketHandler.register(new HoloClickListener("PacketPlayInUseEntity", null));
		PacketHandler.register(new HoloFrameListener("PacketPlayInLook", core));
		PacketHandler.register(new HoloFrameListener("PacketPlayInPosition", core));

		new HoloChunkListener();
	}

	public void disable() {
		for (Hologram holo : holograms)
			holo.disable();
		HoloFrameListener.disable();
	}

	public boolean exists(String id) {
		return Main.ioUtils.indexOf(holograms, id) > -1;
	}

	public void create(Location loc, String id) {
		loc.setY(loc.getY() + 1);
		Hologram holo = new Hologram(loc, id);
		holograms.add(holo);
		save(holo, true);
	}

	public void delete(String id) {
		erase(id);
		holograms.remove(getHolo(id));
	}

	public void addLine(String id, String line) {
		getHolo(id).append(line);
	}

	public void addLine(String id, String... line) {
		getHolo(id).appendAll(line);
	}

	public void insertLine(String id, String line, int index) {
		Hologram holo = getHolo(id);
		int size = holo.size();

		if (index > size)
			index = size;
		else if (index < 0)
			index = 0;

		holo.insert(index, line);
	}

	public void removeLine(String id, int index) {
		Hologram holo = getHolo(id);
		int size = holo.size();

		if (index >= size)
			index = size - 1;
		else if (index < 0)
			index = 0;

		holo.removeLine(index);
	}

	public static List<Hologram> getNearHolograms(Location loc) {
		List<Hologram> near = new ArrayList<Hologram>();

		for (Hologram holo : holograms)
			if (loc.getWorld().equals(holo.getLocation().getWorld()))
				if (loc.distance(holo.getLocation()) < 5)
					near.add(holo);

		return near;
	}

	public void setCommand(String id, String command) {
		Hologram holo = getHolo(id);
		holo.setCommand(command);
		save(holo, false);
	}

	public List<String> getHolos(org.bukkit.World world) {
		List<String> data = new ArrayList<String>();
		String format = "%s: %s, %s/%s/%s/%s";
		boolean a = true;
		int i = 0;

		for (Hologram holo : holograms) {
			Location loc = holo.getLocation();
			if (loc.getWorld().equals(world)) {
				data.add((a ? "&7" : "&f") + String.format(format, i, holo.getID(), loc.getWorld().getName(),
						loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
				a = !a;
				i++;
			}
		}

		return data;
	}

	public void teleport(Location loc, String id) {
		Hologram holo = getHolo(id);
		holo.teleport(loc);

		save(holo, true);
	}

	public Location getLoc(String id) {
		return getHolo(id).getLocation();
	}

	public void clone(String id, String newID, Location loc) {
		loc.setY(loc.getY() + 1);
		Hologram holo = getHolo(id).clone(loc, id);
		holograms.add(holo);

		save(holo, true);
	}

	private Hologram getHolo(String id) {
		return Main.ioUtils.get(holograms, id);
	}

	private void save(Hologram holo, boolean saveFile) {
		Files.holos.file.set(holo.getID(), core.getRSA('@').encrypt(holo.serialize()));
		if (saveFile)
			Files.holos.save();
	}

	private void erase(String id) {
		Files.holos.file.set(id, null);
		Files.holos.save();
	}

	private void load() {
		FileConfiguration conf = Files.holos.file;
		for (String id : conf.getKeys(false))
			holograms.add(Hologram.deserialize(id, core.getRSA('@').decrypt(conf.getString(id))));
	}

	public void load(Hologram holo) {
		holograms.add(holo);
	}

	public void onChunkLoad(Chunk chunk) {
		for (Hologram hologram : holograms)
			if (hologram.isInChunk(chunk))
				hologram.spawnEntities();
	}

	public void onChunkUnload(Chunk chunk) {
		for (Hologram hologram : holograms)
			if (hologram.isInChunk(chunk))
				hologram.despawnEntities();
	}

	// Some Reflection

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void putInPrivateStaticMap(Class<?> clazz, String fieldName, Object key, Object value)
			throws Exception {
		Field field = clazz.getDeclaredField(fieldName);
		field.setAccessible(true);
		Map map = (Map) field.get(null);
		map.put(key, value);
	}
}

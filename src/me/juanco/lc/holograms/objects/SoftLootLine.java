package me.juanco.lc.holograms.objects;

import javax.annotation.Nonnull;

import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import me.juanco.lc.chat.Message;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class SoftLootLine extends LootLine {

	protected SoftLootLine(Hologram parent, String text) {
		super(parent, text);
	}

	@Override
	public boolean spawn(@Nonnull World world, double x, double y, double z) {
		despawn();

		line = new HologramLine(((CraftWorld) world).getHandle(), this);
		line.setPosition(x, y, z);

		if (line == null)
			return false;

		isSpawned = true;

		line.setCustomName(Message.t(getText()));
		return true;
	}

	public void show(PlayerConnection conn) {
		if (line != null)
			conn.sendPacket(new PacketPlayOutSpawnEntityLiving(line));
	}

	public void hide(PlayerConnection conn) {
		conn.sendPacket(new PacketPlayOutEntityDestroy(getEntityID()));
	}

	public void update(PlayerConnection conn, boolean spawned) {
		if (spawned)
			conn.sendPacket(new PacketPlayOutEntityTeleport(line));
		else {
			hide(conn);
			show(conn);
		}
	}
}

package me.juanco.lc.holograms.objects;

import net.minecraft.server.v1_8_R3.ItemStack;

public class ItemHologram {

	private final ItemStack item;
	private final SoftHologram holo;

	public ItemHologram(ItemStack item, SoftHologram holo) {
		this.item = item;
		this.holo = holo;
	}

	public SoftHologram getHolo() {
		return holo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ItemStack)
			return item.equals(obj);
		return super.equals(obj);
	}
}

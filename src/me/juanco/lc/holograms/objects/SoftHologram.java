package me.juanco.lc.holograms.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import io.netty.util.internal.ConcurrentSet;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class SoftHologram extends Hologram {

	private Set<Player> players = new ConcurrentSet<Player>();
	private List<SoftLootLine> lines = new ArrayList<SoftLootLine>();

	public SoftHologram(Location loc, String id) {
		super(loc, id);
	}

	// Positioning

	@Override
	public void teleport(World world, double x, double y, double z) {
		updateLocation(world, x, y, z);
		refresh();
	}

	// Add Lines

	@Override
	public LootLine append(String text) {
		SoftLootLine line = new SoftLootLine(this, text);
		lines.add(line);

		refresh();
		return line;
	}

	@Override
	public LootLine insert(int index, String text) {
		SoftLootLine line = new SoftLootLine(this, text);
		lines.add(index, line);
		refresh();
		return line;
	}

	@Override
	public void appendAll(String... lines) {
		for (String line : lines)
			this.lines.add(new SoftLootLine(this, line));
		refresh();
	}

	// Remove Lines

	@Override
	public void removeLine(int index) {
		if (!players.isEmpty()) {
			SoftLootLine line = lines.remove(index);
			for (Player player : players)
				line.hide(((CraftPlayer) player).getHandle().playerConnection);
		}

		refresh();

		if (lines.isEmpty())
			delete();
	}

	@Override
	public void removeLine(LootLine line) {
		removeLine(lines.indexOf(line));
	}

	@Override
	public void clearLines() {
		if (spawned)
		for (Player player : players) {
			PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;
			for (SoftLootLine line : lines)
				line.hide(conn);
		}

		for (SoftLootLine line : lines)
			line.despawn();

		lines.clear();

		spawned = false;
	}

	// Get Lines

	@Override
	public LootLine getLine(int index) {
		return lines.get(index);
	}

	// Existence

	@Override
	public void spawnEntities() {
		if (spawned)
			despawnEntities();

		double currentY = y;
		boolean first = true;

		List<PlayerConnection> conns = new ArrayList<PlayerConnection>();

		for (Player player : players)
			conns.add(((CraftPlayer) player).getHandle().playerConnection);

		for (SoftLootLine line : lines) {
			if (first)
				first = false;
			else
				currentY -= 0.25;

			boolean spawned = line.isSpawned();

			if (spawned)
				line.teleport(x, currentY, z);
			else
				line.spawn(world, x, currentY, z);

			for (PlayerConnection conn : conns)
				line.update(conn, spawned);
		}

		spawned = true;
	}

	@Override
	public void despawnEntities() {
		List<PlayerConnection> conns = new ArrayList<PlayerConnection>();

		for (Player player : players)
			conns.add(((CraftPlayer) player).getHandle().playerConnection);

		for (SoftLootLine line : lines) {
			for (PlayerConnection conn : conns)
				line.hide(conn);

			line.despawn();
		}

		spawned = false;
	}

	@Override
	public void delete() {
		if (spawned)
			clearLines();
	}

	@Override
	protected List<String> getLines() {
		List<String> result = new ArrayList<String>();

		for (SoftLootLine line : lines)
			result.add(line.getText());

		return result;
	}

	@Override
	public Hologram clone(Location loc, String id) {
		SoftHologram holo = new SoftHologram(loc, id);
		holo.setCommand(getCommand());

		holo.appendAll(getLines().toArray(new String[0]));

		if (spawned)
			holo.spawnEntities();

		holo.players = players;

		return holo;
	}

	@Override
	public String serialize() {
		return null;
	}

	@Override
	public int size() {
		return lines.size();
	}

	public void addPlayer(Player player) {
		players.add(player);
		if (spawned) {
			PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;
			for (SoftLootLine line : lines)
				line.show(conn);
		}
	}

	public void removePlayer(Player player) {
		if (players.remove(player) && spawned) {
			PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;
			for (SoftLootLine line : lines)
				line.hide(conn);
		}
	}
}

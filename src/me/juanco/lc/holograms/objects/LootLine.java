package me.juanco.lc.holograms.objects;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import me.juanco.lc.chat.Message;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.WorldServer;

public class LootLine {

	private static Method validateMethod;

	private String text = null;
	protected boolean isSpawned = false;

	private Hologram parent;
	protected HologramLine line = null;

	public LootLine(Hologram parent, String text) {
		this.parent = parent;

		setText(text);
	}

	public Hologram getParent() {
		return parent;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;

		if (line != null)
			line.setCustomName(Message.t(text));
	}

	public boolean spawn(@Nonnull World world, double x, double y, double z) {

		if (world == null)
			return false;

		despawn();

		line = spawnHologramLine(((CraftWorld) world).getHandle(), x, y, z, this);

		if (line == null)
			return false;

		isSpawned = true;

		line.setCustomName(Message.t(text));
		return true;
	}

	public HologramLine getLine(@Nonnull Location loc) {
		if (line != null)
			return line;
		else {
			spawn(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
			return line;
		}
	}

	public void despawn() {
		if (line != null)
			line.die();
		isSpawned = false;
	}

	public boolean isSpawned() {
		return isSpawned && line != null;
	}

	public void teleport(double x, double y, double z) {
		if (line != null)
			line.setPosition(x, y, z);
	}

	public Integer getEntityID() {
		if (isSpawned())
			return line.getId();
		else
			return null;
	}

	// Entity Spawning

	public static void setValidateMethod(Method method) {
		validateMethod = method;
	}

	private static HologramLine spawnHologramLine(WorldServer world, double x, double y, double z, LootLine parent) {
		HologramLine holo = new HologramLine(world, parent);
		holo.setPosition(x, y, z);
		if (!addEntityToWorld(world, holo))
			return null;
		return holo;
	}

	private static boolean addEntityToWorld(WorldServer world, Entity entity) {
		final int chunkX = MathHelper.floor(entity.locX / 16.0);
		final int chunkZ = MathHelper.floor(entity.locZ / 16.0);

		if (!world.chunkProviderServer.isChunkLoaded(chunkX, chunkZ)) {
			entity.dead = true;
			return false;
		}

		world.getChunkAt(chunkX, chunkZ).a(entity);
		world.entityList.add(entity);

		try {
			validateMethod.invoke(world, entity);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}

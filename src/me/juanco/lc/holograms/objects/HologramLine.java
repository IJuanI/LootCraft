package me.juanco.lc.holograms.objects;

import java.lang.reflect.Field;

import net.minecraft.server.v1_8_R3.DamageSource;
import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_8_R3.WorldServer;

public class HologramLine extends EntityArmorStand {

	private static Field bi = null;

	private boolean lock = true;
	private final LootLine parent;

	public HologramLine(WorldServer world, LootLine parent) {
		super(world);

		setInvisible(true);
		setGravity(false);

		try {
			if (bi == null) {
				bi = EntityArmorStand.class.getField("bi");
				bi.setAccessible(true);
			}

			bi.set(this, Integer.MAX_VALUE);
		} catch (Exception ex) {
		}

		this.parent = parent;
	}

	public LootLine upper() {
		return parent;
	}

	// Lock Tick
	@Override
	public void t_() {
		if (!lock)
			super.t_();
	}

	// Remove Tags
	@Override
	public void b(NBTTagCompound nbttagcompound) {
	}

	@Override
	public boolean c(NBTTagCompound nbttagcompound) {
		return false;
	}

	@Override
	public boolean d(NBTTagCompound nbttagcompound) {
		return false;
	}

	@Override
	public void e(NBTTagCompound nbttagcompound) {
	}

	// This one is pretty Obvious
	@Override
	public boolean isInvulnerable(DamageSource source) {
		return true;
	}

	// An entity should tick to die
	@Override
	public void die() {
		lock = false;

		parent.isSpawned = false;
		parent.line = null;

		super.die();
	}

	// Exactly why would we need this one to work?
	@Override
	public void setCustomNameVisible(boolean visible) {
	}

	// This describes itself
	@Override
	public void setCustomName(String name) {

		if (name != null && name.length() > 300)
			name = name.substring(0, 300);

		super.setCustomName(name);
		super.setCustomNameVisible(name != null && !name.isEmpty());
	}

	// Just to make sure holograms will spawn
	@Override
	public void setPosition(double x, double y, double z) {
		super.setPosition(x, y, z);

		// Updating a near chunk.
		PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(getId(),
				MathHelper.floor(locX * 32.0D), MathHelper.floor(locY * 32.0D), MathHelper.floor(locZ * 32.0D),
				(byte) (yaw * 256.0F / 360.0F), (byte) (pitch * 256.0F / 360.0F), onGround);

		for (EntityHuman player : world.players)
			if (square(player.locX - locX) + square(player.locZ - locZ) < 8192)
				((EntityPlayer) player).playerConnection.sendPacket(teleportPacket);
	}

	private static double square(double i) {
		return i * i;
	}
}

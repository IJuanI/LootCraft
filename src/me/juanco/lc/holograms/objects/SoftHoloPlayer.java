package me.juanco.lc.holograms.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import me.juanco.lc.holograms.Holograms;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class SoftHoloPlayer {

	private final Player player;
	private SoftHologram holo;
	private List<Hologram> hided = new ArrayList<Hologram>();

	public SoftHoloPlayer(Player player, SoftHologram holo) {
		this.player = player;
		this.holo = holo;

		if (holo != null)
			holo.addPlayer(player);
	}

	public void setHologram(SoftHologram holo, boolean hide) {
		if (this.holo != null)
			this.holo.removePlayer(player);

		this.holo = holo;

		if (holo != null) {
			holo.addPlayer(player);

			if (hide) {
				PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;

				for (Hologram h : Holograms.getNearHolograms(holo.getLocation())) {
					h.hide(conn);
					hided.add(h);
				}
			}

		} else if (holo == null && hide) {
			PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;
			for (Hologram h : hided)
				h.show(conn);
			hided.clear();
		}
	}

	public SoftHologram getHologram() {
		return holo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		return super.equals(obj);
	}
}

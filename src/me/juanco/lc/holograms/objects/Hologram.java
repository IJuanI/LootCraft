package me.juanco.lc.holograms.objects;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import me.juanco.lc.core.Main;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class Hologram {

	protected World world;
	protected double x, y, z;
	protected int chunkX, chunkZ;

	private List<LootLine> lines;

	public boolean spawned = true;
	private String id, command;

	public Hologram(Location loc, String id) {
		updateLocation(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());

		lines = Lists.newArrayList();

		this.id = id == null ? null : id.toLowerCase();
	}

	// Location

	protected void updateLocation(World world, double x, double y, double z) {
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		chunkX = floor(x) >> 4;
		chunkZ = floor(z) >> 4;
	}

	public boolean isInChunk(Chunk chunk) {
		return chunk.getX() == chunkX && chunk.getZ() == chunkZ;
	}

	public Location getLocation() {
		return new Location(world, x, y, z);
	}

	// Positioning

	public void teleport(Location location) {
		teleport(location.getWorld(), location.getX(), location.getY(), location.getZ());
	}

	public void teleport(World world, double x, double y, double z) {
		if (this.world != world) {
			updateLocation(world, x, y, z);
			despawnEntities();
			refresh();
			return;
		}

		updateLocation(world, x, y, z);

		double currentY = y;
		boolean first = true;

		for (LootLine line : lines) {
			if (!line.isSpawned())
				continue;

			if (first)
				first = false;
			else
				currentY -= 0.25;

			line.teleport(x, currentY, z);
		}
	}

	// Add Lines

	public LootLine append(String text) {
		LootLine line = new LootLine(this, text);
		lines.add(line);

		refresh();
		return line;
	}


	public LootLine insert(int index, String text) {
		LootLine line = new LootLine(this, text);
		lines.add(index, line);
		refresh();
		return line;
	}

	public void appendAll(String... lines) {
		for (String line : lines)
			this.lines.add(new LootLine(this, line));
		refresh();
	}

	// Remove Lines

	public void removeLine(int index) {
		lines.remove(index).despawn();

		refresh();

		if (lines.isEmpty())
			delete();
	}

	public void removeLine(LootLine line) {
		lines.remove(line);
		line.despawn();

		refresh();

		if (lines.isEmpty())
			delete();
	}

	public void clearLines() {
		for (LootLine line : lines)
			line.despawn();

		lines.clear();
	}

	// Get Lines

	public LootLine getLine(int index) {
		return lines.get(index);
	}

	// Refresh Lines

	public void refresh() {
		if (world != null && world.isChunkLoaded(chunkX, chunkZ))
			spawnEntities();
	}

	// Existence

	public void spawnEntities() {
		if (spawned)
			despawnEntities();

		double currentY = y;
		boolean first = true;

		for (LootLine line : lines) {
			if (first)
				first = false;
			else
				currentY -= 0.25;

			if (line.isSpawned())
				line.teleport(x, currentY, z);
			else
				line.spawn(world, x, currentY, z);
		}

		spawned = true;
	}

	public void despawnEntities() {
		for (LootLine line : lines)
			line.despawn();

		spawned = false;
	}

	public void delete() {
		if (spawned) {
			spawned = false;
			clearLines();
		}
	}

	public boolean isSpawned() {
		return spawned;
	}

	// Utils

	/* Setters */
	public void setCommand(String command) {
		this.command = command;
	}

	protected void setID(String id) {
		this.id = id;
	}

	/* Checkers */
	public boolean hasCommand() {
		return command != null;
	}

	/* Getters */
	public String getCommand() {
		return command;
	}

	public String getID() {
		return id;
	}

	// Utils

	public void disable() {
		despawnEntities();
	}

	protected List<String> getLines() {
		List<String> result = new ArrayList<String>();

		for (LootLine line : lines)
			result.add(line.getText());

		return result;
	}

	public void clickLine(Player clicker) {
		if (command == null)
			return;

		try {
			String command = Main.ioUtils.hardReplace(this.command, "{player}", clicker.getName());

			if (command.startsWith(">.<"))
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.substring(3));
			else
				Bukkit.dispatchCommand(clicker, command);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// IO

	private int floor(double num) {
		int floor = (int) num;
		return floor == num ? floor : floor - (int) (Double.doubleToRawLongBits(num) >>> 63);
	}

	public int size() {
		return lines.size();
	}

	public Hologram clone(Location loc, String id) {
		Hologram holo = new Hologram(loc, id);
		holo.command = command;

		holo.appendAll(getLines().toArray(new String[0]));

		holo.spawnEntities();

		return holo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return id.equals(obj);
		return super.equals(obj);
	}

	public String serialize() {
		StringBuilder builder = new StringBuilder();

		DecimalFormat format = new DecimalFormat("#0.##");
		builder.append(world.getName()).append(',').append(format.format(x)).append(',').append(format.format(y))
				.append(',').append(format.format(z)).append((char) 251);

		List<String> lines = getLines();
		if (!lines.isEmpty()) {
			builder.append(lines.get(0));
			for (int i = 1; i < lines.size(); i++)
				builder.append(',').append(lines.get(i));
		}

		if (command != null)
			builder.append((char) 251).append(command);

		return builder.toString();
	}

	public static Hologram deserialize(String id, String serial) {
		String[] data = serial.split(String.valueOf((char) 251));

		String[] locData = data[0].split(",");
		Location loc = new Location(Bukkit.getWorld(locData[0]), Double.parseDouble(locData[1]),
				Double.parseDouble(locData[2]), Double.parseDouble(locData[3]));

		Hologram holo = new Hologram(loc, id);
		if (data.length > 1) {
			holo.appendAll(data[1].contains(",") ? data[1].split(",") : new String[] { data[1] });

			if (data.length > 2)
				holo.setCommand(data[2]);
		}

		return holo;
	}

	// Extra

	public void show(PlayerConnection conn) {
		if (spawned)
			for (LootLine line : lines)
				conn.sendPacket(new PacketPlayOutSpawnEntityLiving(line.line));
	}

	public void hide(PlayerConnection conn) {
		if (spawned)
			for (LootLine line : lines)
				conn.sendPacket(new PacketPlayOutEntityDestroy(line.getEntityID()));
	}
}

package me.juanco.lc.holograms.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import me.juanco.lc.api.LootListener;

public class HoloChunkListener extends LootListener {

	@EventHandler
	public void onChunkLoad(ChunkLoadEvent e) {
		getMain().getHolograms().onChunkLoad(e.getChunk());
	}

	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent e) {
		getMain().getHolograms().onChunkUnload(e.getChunk());
	}
}

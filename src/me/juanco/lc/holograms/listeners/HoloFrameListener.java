package me.juanco.lc.holograms.listeners;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.core.Main;
import me.juanco.lc.holograms.objects.ItemHologram;
import me.juanco.lc.holograms.objects.SoftHoloPlayer;
import me.juanco.lc.holograms.objects.SoftHologram;
import me.juanco.lc.packets.PacketListener;
import net.minecraft.server.v1_8_R3.EntityItemFrame;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

public class HoloFrameListener extends PacketListener {

	private final Main core;

	public HoloFrameListener(String name, Main core) {
		super(name);
		this.core = core;
	}

	private static Set<SoftHoloPlayer> players = new ConcurrentSet<SoftHoloPlayer>();
	static ConcurrentSet<ItemHologram> holograms = new ConcurrentSet<ItemHologram>();

	@Override
	public boolean listen(Object message, Player target) {
		try {
			EntityItemFrame iFrame = core.getUtils().getEntityOnSight(target, 5, "EntityItemFrame", 0, true);

			SoftHoloPlayer holoPlayer = Main.ioUtils.get(players, target);

			if (holoPlayer == null)
				players.add(holoPlayer = new SoftHoloPlayer(target, null));

			ItemHologram holo = getHolo(iFrame, target.getWorld());

			if (holoPlayer.getHologram() == null && holo != null || holoPlayer.getHologram() != null && holo == null
					|| holo != null && !holo.getHolo().equals(holoPlayer.getHologram()))
				holoPlayer.setHologram(holo == null ? null : holo.getHolo(), true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

	private ItemHologram getHolo(EntityItemFrame iFrame, World world) {
		if (iFrame == null || iFrame.getItem() == null)
			return null;

		NBTTagCompound display = iFrame.getItem().hasTag() ? iFrame.getItem().getTag().getCompound("display") : null;
		if (display == null || !display.hasKey("Lore"))
			return null;

		ItemHologram holo = Main.ioUtils.get(holograms, iFrame.getItem());

		if (holo == null) {
			Location loc = new Location(world, iFrame.locX, iFrame.locY, iFrame.locZ);
			loc.setY(loc.getY() - 1.7);

			switch (iFrame.direction) {
			case NORTH:
				loc.setZ(loc.getZ() - 0.5);
				break;
			case SOUTH:
				loc.setZ(loc.getZ() + 0.5);
				break;
			case EAST:
				loc.setX(loc.getX() + 0.5);
				break;
			case WEST:
				loc.setX(loc.getX() - 0.5);
				break;
			default:
				break;
			}

			SoftHologram sHolo = new SoftHologram(loc, null);

			if (display.hasKey("Name"))
				sHolo.append(display.getString("Name"));

			NBTTagList lores = (NBTTagList) display.get("Lore");
			for (int i = 0; i < lores.size(); i++)
				sHolo.append(lores.getString(i));

			sHolo.spawnEntities();

			holo = new ItemHologram(iFrame.getItem(), sHolo);
			holograms.add(holo);
		}

		return holo;
	}

	public static void disable() {
		for (ItemHologram holo : holograms)
			holo.getHolo().disable();
	}

	public static void remove(Player player) {
		SoftHoloPlayer sPlayer = Main.ioUtils.get(players, player);
		if (sPlayer != null) {
			players.remove(sPlayer);
			sPlayer.setHologram(null, true);
		}
	}
}

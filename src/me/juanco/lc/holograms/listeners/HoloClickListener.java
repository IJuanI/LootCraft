package me.juanco.lc.holograms.listeners;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;

import me.juanco.lc.core.Main;
import me.juanco.lc.holograms.objects.HologramLine;
import me.juanco.lc.packets.PacketListener;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.PacketPlayInArmAnimation;
import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity;
import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity.EnumEntityUseAction;

public class HoloClickListener extends PacketListener {

	private final Main core;

	public HoloClickListener(String name, Main core) {
		super(name);
		this.core = core;
	}

	@Override
	public boolean listen(Object message, Player target) {
		if (message instanceof PacketPlayInUseEntity
				&& ((PacketPlayInUseEntity) message).a().equals(EnumEntityUseAction.ATTACK))
			return false;
		else if (message instanceof PacketPlayInArmAnimation
				&& target.getLineOfSight((Set<Material>) null, 5).isEmpty())
			return false;

		if (message instanceof PacketPlayInUseEntity) {
			Entity e = ((PacketPlayInUseEntity) message).a(((CraftWorld) target.getWorld()).getHandle());
			if (e instanceof HologramLine)
				((HologramLine) e).upper().getParent().clickLine(target);
		} else
			try {
				HologramLine line = core.getUtils().getEntityOnSight(target, 60, "HologramLine", 2.1, false);

				if (line == null)
					return false;

				line.upper().getParent().clickLine(target);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		return false;
	}

}

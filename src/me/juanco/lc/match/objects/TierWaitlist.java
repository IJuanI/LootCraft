package me.juanco.lc.match.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import me.juanco.lc.match.MatchManager;
import me.juanco.lc.match.enums.MatchTier;

public class TierWaitlist {

	private final MatchTier tier;
	private final MatchManager manager;

	private final Queue<MatchUser> waitlist = new ConcurrentLinkedQueue<MatchUser>();
	
	public TierWaitlist(MatchTier tier, MatchManager manager) {
		this.tier = tier;
		this.manager = manager;
	}

	public TierWaitlist queue(MatchUser user) {
		waitlist.add(user);
		user.setWaitlist(this);
		return this;
	}

	public void dequeue(MatchUser user) {
		waitlist.remove(user);
		user.setWaitlist(null);
	}

	public void flush() {
		if (waitlist.size() > 1) {

			List<MatchUser> users = new ArrayList<MatchUser>();

			for (int i = 0; i < 2; i++) {
				users.add(waitlist.remove());
			}

			if (!manager.match(tier, users.toArray(new MatchUser[0])))
				for (MatchUser user : users)
					queue(user);
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MatchTier)
			return tier.equals(obj);
		return super.equals(obj);
	}
}

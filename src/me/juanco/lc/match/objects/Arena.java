package me.juanco.lc.match.objects;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

import me.juanco.lc.api.LootProperties;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;

public class Arena {

	private final LootProperties props = new LootProperties();

	protected Main core;

	public Arena(Main core, String id) {
		if (!Files.matches.file.isConfigurationSection("Arenas." + id))
			return;

		ConfigurationSection section = Files.matches.file.getConfigurationSection("Arenas." + id);

		if (!section.contains("0") || !section.contains("1"))
			return;

		props.set("0", core.getUtils().loadLoc(section.getString("0")));
		props.set("1", core.getUtils().loadLoc(section.getString("1")));
		props.set("id", id);

		this.core = core;
	}

	public Location get(Object obj) {
		return get(obj.toString());
	}

	public Location get(String id) {
		return props.get(id);
	}

	public String getID() {
		return props.get("id");
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return props.get("id").equals(obj);
		return super.equals(obj);
	}
}

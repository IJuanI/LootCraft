package me.juanco.lc.match.objects;

public class DisplayedMatch {

	private final Match match;
	private long length;

	public DisplayedMatch(Match match, long length) {
		this.match = match;
		this.length = length;
	}

	public boolean flush() {
		length--;
		match.display(length);
		if (length == 0)
			return true;
		return false;
	}
}

package me.juanco.lc.match.objects;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.LootProperties;
import me.juanco.lc.api.inventory.LootInventoryContents;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.kits.KitPlayer;
import me.juanco.lc.kits.Kits;
import me.juanco.lc.utils.IOUtils;

public class MatchUser {

	private final Player player;

	private Match match = null;
	private Arena arena = null;
	private MatchUser[] oponents = null;

	private final LootProperties props = new LootProperties();

	public MatchUser(Player player) {
		this.player = player;

		load();
	}

	public Player getPlayer() {
		return player;
	}

	public Match getMatch() {
		return match;
	}

	public Arena getArena() {
		return arena;
	}

	public TierWaitlist getWaitlist() {
		return props.get("wait");
	}

	public boolean hasOponents() {
		return oponents != null;
	}

	public boolean hasAccepted() {
		Object obj = props.get("accept");
		return obj != null && obj instanceof Boolean && (boolean) obj;
	}

	public MatchUser[] getOponents() {
		return oponents;
	}

	public int getReward() {
		return props.get("reward");
	}

	public boolean isFighting() {
		return match != null && arena != null;
	}

	public boolean isWaiting() {
		return getWaitlist() != null;
	}

	public void setMatch(Match match) {
		this.match = match;
		arena = match.getArena();
	}

	public void setArena(Arena arena) {
		this.arena = arena;
	}

	public void accept() {
		props.set("accept", true);
	}

	public void setOponents(MatchUser... oponents) {
		this.oponents = oponents;
	}

	public void setWaitlist(TierWaitlist waitlist) {
		props.set("wait", waitlist);
	}

	public void setReward(int reward) {
		props.set("reward", reward);
	}

	public void teleport(Location loc) {
		props.setRoot("old");
		props.set("loc", player.getLocation());
		props.resetRoot();

		player.teleport(loc);
	}

	public void setContents(LootInventoryContents contents) {
		props.setRoot("old");
		props.set("inv", player.getInventory().getContents());
		props.resetRoot();

		player.getInventory().setContents(contents.getContents());
	}

	public void win() {
		props.set("wins", 1 +  (int) props.get("wins"));

		getNext();
	}

	public void lose(boolean cleanExit) {
		props.set("lose", 1 +  (int) props.get("lose"));

		if (cleanExit)
			getNext();
		else {
			if (props.get("next") == null)
				return;

			int next = props.get("next");
			if (next > -1)
				props.set("next", next - 1);
			else {
				KitPlayer kPlayer = Kits.getUser(player);

				if (kPlayer.getBestItem() != null) {
					player.getInventory().remove(kPlayer.getBestItem());
					props.set("cleared", true);
				} else if (kPlayer.getLevel() > 0) {
					ItemStack[] armor = player.getInventory().getArmorContents();

					for (int i = 0; i < armor.length; i++)
						if (armor[i] != null) {
							armor[i] = null;
							break;
						}
					props.set("cleared", true);
				} else if (next > -2)
					props.set("next", next - 1);
				else
					arena.core.getBans().ban(player, "&4&lBANNED: &c&lDisconnected During Combat", IOUtils.hourMs);
			}
		}
	}

	private void getNext() {
		int next = props.get("next");
		int n = next;

		if (next < 4)
			next += 1;
		else if (next < 1)
			next = 1;

		if (n != next)
			props.set("next", next);
	}

	public void save() {
		UserFile user = UserFile.getFile(player.getUniqueId());

		FileConfiguration fc = user.file;
		fc.set("Match.Next", (int) props.get("next"));
		fc.set("Match.Wins", (int) props.get("wins"));
		fc.set("Match.Lose", (int) props.get("lose"));

		user.save();

	}

	private void load() {
		UserFile user = UserFile.getFile(player.getUniqueId());
		FileConfiguration fc = user.file;
		boolean modificated = false;

		if (fc.contains("Match.Next"))
			props.set("next", fc.getInt("Match.Next"));
		else {
			fc.set("Match.Next", 3);
			props.set("next", 3);
			modificated = true;
		}

		if (fc.contains("Match.Wins"))
			props.set("wins", fc.getInt("Match.Wins"));
		else {
			fc.set("Match.Wins", 0);
			props.set("wins", 0);
			modificated = true;
		}

		if (fc.contains("Match.Lose"))
			props.set("lose", fc.getInt("Match.Lose"));
		else {
			fc.set("Match.Lose", 0);
			props.set("lose", 0);
			modificated = true;
		}

		if (modificated)
			user.save();
	}

	public void reset() {
		match = null;
		arena = null;

		props.setRoot("old");

		if (props.get("loc") != null)
			player.teleport((Location) props.remove("loc"));

		if (props.get("inv") != null)
			player.getInventory().setContents((ItemStack[]) props.remove("inv"));

		props.resetRoot();

		props.remove("accept");
		props.remove("reward");
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		else if (obj instanceof Match)
			return match.equals(obj);
		else if (obj instanceof Arena)
			return arena.equals(obj);
		return super.equals(obj);
	}
}

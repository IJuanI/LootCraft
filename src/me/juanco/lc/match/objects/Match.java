package me.juanco.lc.match.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.display.Title;
import me.juanco.lc.api.inventory.LootInventoryContents;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.match.MatchGUI;
import me.juanco.lc.match.enums.MatchState;
import me.juanco.lc.match.tasks.MatchDisplayerTask;
import me.juanco.lc.match.tasks.MatchStateTask;

public class Match {

	private final Arena arena;
	private final MatchUser[] rivals = new MatchUser[2];

	private MatchState state = MatchState.Waiting;

	public Match(Arena arena, MatchGUI gui, MatchUser... rivals) {
		this.arena = arena;

		for (int i = 0; i < rivals.length; i++) {
			MatchUser rival = rivals[i];
			gui.get(rival.getPlayer()).setData(2);

			rival.teleport(arena.get(i));

			ItemStack weapon = arena.core.getKits().getPlayer(rival.getPlayer()).getBestItem();
			LootInventoryContents inv = new LootInventoryContents(36);

			if (weapon != null)
				inv.add(weapon);

			rival.setContents(inv);
			this.rivals[i] = rival;
		}

		changeState(6);
		MatchDisplayerTask.display(this, 6);
	}

	public MatchUser[] getRivals() {
		return rivals;
	}

	public Arena getArena() {
		return arena;
	}

	public MatchState getState() {
		return state;
	}

	private void changeState(long delay) {
		LootClock.addTask(new MatchStateTask(this, delay));
	}

	public void display(long length) {
		if (state == MatchState.Ending) {

		} else {
			String title;
			int in = 0, show = 40, out = 10;
			switch ((int) length) {
			case 5:
				title = "&2* Ready *";
				in = 10;
				break;
			case 4:
				title = "&a* &2Ready &a*";
				break;
			case 3:
				title = "&6* Set *";
				break;
			case 2:
				title = "&e* &6Set &e*";
				break;
			case 1:
				title = "&4* Fight *";
				break;
			default:
				title = "&c* &4Fight &c*";
				show = 20;
				break;
			}

			String sub = length > 0 ? String.format("&d&l%s second%s left!", length, length > 1 ? "s" : "") : null;

			List<Player> players = new ArrayList<Player>();

			for (MatchUser rival : rivals)
				players.add(rival.getPlayer());

			Title.send(title, sub, in, show, out, players.toArray(new Player[0]));
		}
	}

	public void updateState() {
		if (state == MatchState.Ending)
			end();
		else
			state = MatchState.values()[state.ordinal() + 1];
	}

	public void end() {
		arena.core.getMatches().getManager().end(this);
	}

	public void win(MatchUser user) {
		updateState();
		changeState(6);

		MatchDisplayerTask.display(this, 6);

		user.win();
		Message.send(Messages.ALERTS.PVP.win, user.getPlayer());

		if (Main.tokens.add(user.getPlayer(), user.getReward()))
			Message.sendWithReplaces(Messages.SUCCESS.ECONOMY.added, user.getPlayer(), "%currency", "Tokens", "%n",
					String.valueOf(user.getReward()));
		else
			Message.send(Messages.ERRORS.economy, user.getPlayer());
	}

	public void lose(MatchUser user) {
		MatchUser winner;
		if (rivals[0].equals(user))
			winner = rivals[1];
		else
			winner = rivals[0];

		user.lose(true);
		Message.send(Messages.ALERTS.PVP.lose, user.getPlayer());

		win(winner);
	}

	public void quit(MatchUser user) {
		MatchUser winner;
		if (rivals[0].equals(user))
			winner = rivals[1];
		else
			winner = rivals[0];

		user.lose(false);

		win(winner);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Arena)
			return arena.equals(obj);
		return super.equals(obj);
	}
}

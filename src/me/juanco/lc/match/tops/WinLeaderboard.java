package me.juanco.lc.match.tops;

import me.juanco.lc.api.leaderboard.LootLeaderboard;
import me.juanco.lc.files.Files;

public class WinLeaderboard extends LootLeaderboard {

	public WinLeaderboard() {
		super(Files.storage, "leaderboards.win", "lL1");
	}

	@Override
	protected void registerAnimations() {
		addAnimation("&a&lWin Top");
		addAnimation("&a&lWin Top");
		addAnimation("&a&lWin Top");
		addAnimation("&2&lW&a&lin Top");
		addAnimation("&a&lW&2&li&a&ln Top");
		addAnimation("&a&lWi&2&ln &a&lTop");
		addAnimation("&a&lWin &2&lT&a&lop");
		addAnimation("&a&lWin T&2&lo&a&lp");
		addAnimation("&a&lWin To&2&lp");
		addAnimation("&a&lWin Top");
		addAnimation("&a&lWin Top");
		addAnimation("&a&lWin Top");
		addAnimation("&a&lWin Top");
	}

	@Override
	protected int getRealValue(String player) {
		return 1;
	}

}

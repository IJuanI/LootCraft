package me.juanco.lc.match.tops;

import me.juanco.lc.api.leaderboard.LootLeaderboard;
import me.juanco.lc.files.Files;

public class LoseLeaderboard extends LootLeaderboard {

	public LoseLeaderboard() {
		super(Files.storage, "leaderboards.lose", "lL0");
	}

	@Override
	protected void registerAnimations() {
		addAnimation("&c&lLose Top");
		addAnimation("&c&lLose Top");
		addAnimation("&4&lL&c&lose Top");
		addAnimation("&c&lL&4&lo&c&lse Top");
		addAnimation("&c&lLo&4&ls&c&le Top");
		addAnimation("&c&lLos&4&le &c&lTop");
		addAnimation("&c&lLose &4&lT&c&lop");
		addAnimation("&c&lLose T&4&lo&c&lp");
		addAnimation("&c&lLose To&4&lp");
		addAnimation("&c&lLose Top");
		addAnimation("&c&lLose Top");
		addAnimation("&c&lLose Top");
		addAnimation("&c&lLose Top");
	}

	@Override
	protected int getRealValue(String player) {
		return 2;
	}

}

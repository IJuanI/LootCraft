package me.juanco.lc.match.tasks;

import java.util.Set;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.match.objects.DisplayedMatch;
import me.juanco.lc.match.objects.Match;

public class MatchDisplayerTask extends LootTask {

	private static Set<DisplayedMatch> displayed = new ConcurrentSet<DisplayedMatch>();

	public MatchDisplayerTask() {
		super(1, true);
		LootClock.addTask(this);
	}

	@Override
	public void run() {
		for (DisplayedMatch disp : displayed)
			if (disp.flush())
				displayed.remove(disp);
	}

	public static void display(Match match, int length) {
		displayed.add(new DisplayedMatch(match, length));
	}
}

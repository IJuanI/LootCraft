package me.juanco.lc.match.tasks;

import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.match.objects.Match;

public class MatchStateTask extends LootTask {

	private final Match match;

	public MatchStateTask(Match match, long delay) {
		super(delay, false);
		this.match = match;
	}

	@Override
	public void run() {
		match.updateState();
	}

}

package me.juanco.lc.match.enums;

public enum MatchState {

	Waiting, Fighting, Ending;
}

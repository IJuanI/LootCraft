package me.juanco.lc.match.enums;

public enum MatchTier {

	Tier1(9, 1), Tier2(10, 1), Tier3(11, 1), Tier4(12, 2), Tier5(13, 2), Tier6(14, 2), Tier7(15, 3), Tier8(16,
			3), Tier9(17, 3);

	private final int base;
	private final int modifier;

	private MatchTier(int baseReward, int modifier) {
		base = baseReward;
		this.modifier = modifier;
	}

	public int getReward(int l1, int l2) {
		if (l1 == l2)
			return base;
		else if (l1 > l2)
			return base - modifier;
		else
			return base + modifier;
	}
}

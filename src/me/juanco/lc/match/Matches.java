package me.juanco.lc.match;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.match.listeners.MatchDamageListener;
import me.juanco.lc.match.listeners.MatchEntranceListener;
import me.juanco.lc.match.listeners.MatchItemListener;
import me.juanco.lc.match.listeners.MatchMoveListener;
import me.juanco.lc.match.objects.Arena;
import me.juanco.lc.match.objects.MatchUser;
import me.juanco.lc.match.tasks.MatchDisplayerTask;

public class Matches {

	private List<MatchUser> users = new ArrayList<MatchUser>();

	private MatchManager manager;

	public Matches(Main core) {
		manager = new MatchManager(core);

		loadArenas(core);

		new MatchDisplayerTask();

		registerListeners();
	}

	public MatchManager getManager() {
		return manager;
	}

	public MatchUser getUser(Player player) {
		Validate.notNull(player);

		MatchUser user = Main.ioUtils.get(users, player);
		if (user == null) {
			user = new MatchUser(player);
			users.add(user);
		}

		return user;
	}

	public void remove(Player player) {
		int i = Main.ioUtils.indexOf(users, player);
		if (i > -1) {
			MatchUser user = users.get(i);

			if (user.getWaitlist() != null)
				user.getWaitlist().dequeue(user);

			if (user.getMatch() != null)
				user.getMatch().quit(user);

			user.save();
			users.remove(i);
		}
	}

	public void onDisable() {
		manager.disable();
	}

	private void loadArenas(Main core) {
		FileConfiguration conf = Files.matches.file;
		if (conf.contains("Arenas")) {
			ConfigurationSection sec = conf.getConfigurationSection("Arenas");
			for (String id : sec.getKeys(false))
				if (sec.contains(id + ".1") && sec.contains(id + ".0"))
					manager.addArena(new Arena(core, id));
		}
	}

	private void registerListeners() {
		new MatchDamageListener();
		new MatchItemListener();
		new MatchEntranceListener();
		new MatchMoveListener();
	}
}

package me.juanco.lc.match;

import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.LootProperties;
import me.juanco.lc.api.inventory.LootInventory;
import me.juanco.lc.api.inventory.LootInventoryContents;
import me.juanco.lc.api.inventory.LootViewer;
import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;

public class MatchGUI extends LootInventory {

	private final LootInventoryContents general = new LootInventoryContents(54);
	private final MatchManager manager;

	public MatchGUI(MatchManager manager) {
		ItemStack a = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5), "&2&lAccept Battle");
		ItemStack d = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), "&4&lNext");
		ItemStack m = Main.itemUtils.get(Material.STAINED_GLASS_PANE, "&lMatchmaking");
		ItemStack y = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), "&a&lYour Loot");
		ItemStack o = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15),
				"&c&lOpponent's Loot");
		ItemStack l = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 11), "&3&lItem Level");

		for (int i = 0; i < 54; i++)
			switch (i) {
			case 1:
			case 10:
			case 19:
			case 28:
			case 36:
			case 37:
			case 46:
				general.setItem(i, y);
				break;
			case 7:
			case 16:
			case 25:
			case 34:
			case 43:
			case 44:
			case 52:
				general.setItem(i, o);
				break;
			case 3:
			case 4:
			case 5:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 20:
			case 24:
			case 29:
			case 33:
			case 38:
			case 42:
			case 47:
			case 51:
				general.setItem(i, m);
				break;
			case 21:
			case 22:
			case 23:
			case 30:
			case 31:
			case 32:
				general.setItem(i, a);
				break;
			case 39:
			case 40:
			case 41:
			case 48:
			case 49:
			case 50:
				general.setItem(i, d);
				break;
			case 2:
			case 6:
				general.setItem(i, l);
				break;
			default:
				continue;
			}

		this.manager = manager;
	}

	private LootProperties props = new LootProperties();

	public void open(int l1, int l2, int r1, int r2, ItemStack w1, ItemStack w2, Player p1, Player p2) {
		props.setRoot("l");
		props.set("1", l1);
		props.set("2", l2);

		props.setRoot("r");
		props.set("1", r1);
		props.set("2", r2);

		props.setRoot("w");
		props.set("1", w1);
		props.set("2", w2);

		props.setRoot("p");
		props.set("1", p1);
		props.set("2", p2);

		props.setRoot("eq");
		props.set("1", p1.getInventory().getArmorContents());
		props.set("2", p2.getInventory().getArmorContents());

		props.resetRoot();

		open(p1);
		open(p2);

		props = new LootProperties();
	}

	@Override
	protected void preOpenInventory(Player player) {
	}

	@Override
	protected void openInventory(Player player) {
		LootInventoryContents contents = new LootInventoryContents(54);
		general.insertInto(contents);

		props.setRoot("p");
		int i1 = player.equals(props.get("1")) ? 1 : 2;
		int i2 = i1 == 1 ? 2 : 1;

		props.setRoot("r");
		int r = props.get(i1);

		props.setRoot("l");
		int l1 = props.get(i1);
		int l2 = props.get(i2);

		props.setRoot("w");
		ItemStack w1 = props.get(i1);
		ItemStack w2 = props.get(i2);

		props.setRoot("eq");
		ItemStack[] eq1 = props.get(i1);
		ItemStack[] eq2 = props.get(i2);

		props.resetRoot();

		for (int i = 0; i < eq1.length; i++) {
			contents.setItem(i * 9, eq1[3 - i]);
			contents.setItem(i * 9 + 8, eq2[3 - i]);
		}

		contents.setItem(45, w1);
		contents.setItem(53, w2);

		ItemStack ol1 = contents.getItem(2);
		ItemStack ol2 = contents.getItem(6);

		Main.itemUtils.setLore(ol1, "&l" + l1);
		Main.itemUtils.setLore(ol2, "&l" + l2);

		contents.setItem(2, ol1);
		contents.setItem(6, ol2);

		Inventory inv = contents.getInventory(Message.t("&a&lReward: &f&l" + (r > 0 ? r : "none")));

		player.openInventory(inv);
	}

	public LootViewer get(HumanEntity entity) {
		return LootInventory.getViewer(entity);
	}

	@Override
	protected void closeInventory(Player player) {
		player.closeInventory();
	}

	@Override
	protected boolean inventoryClick(InventoryClickEvent e) {
		int slot = e.getSlot();
		if (slot % 9 > 2 && slot % 9 < 6) {
			int i = slot / 9;
			if (i > 3)
				close((Player) e.getWhoClicked());
			else if (i > 1)
				manager.accept((Player) e.getWhoClicked());
		}
		return true;
	}

	@Override
	protected void inventoryClose(InventoryCloseEvent e) {
		Integer val = getData(e.getPlayer());
		if (val != null && val == 2)
			return;

		manager.deny((Player) e.getPlayer());
	}

}

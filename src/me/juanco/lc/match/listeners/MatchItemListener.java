package me.juanco.lc.match.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.match.objects.MatchUser;

public class MatchItemListener extends LootListener {

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		MatchUser user = getMain().getMatches().getUser(e.getPlayer());
		if (user.isFighting() || user.isWaiting())
			e.setCancelled(true);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (e.getClickedInventory() != null && e.getClickedInventory().equals(e.getView().getBottomInventory())) {
			MatchUser user = getMain().getMatches().getUser((Player) e.getWhoClicked());
			if (user.isFighting() || user.isWaiting())
				e.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)
				&& e.getClickedBlock().getType().name().toLowerCase().contains("chest")) {
			MatchUser user = getMain().getMatches().getUser(e.getPlayer());
			if (user.isFighting() || user.isWaiting())
				e.setCancelled(true);
		}
	}
}

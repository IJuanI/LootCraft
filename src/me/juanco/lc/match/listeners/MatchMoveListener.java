package me.juanco.lc.match.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.match.enums.MatchState;
import me.juanco.lc.match.objects.MatchUser;

public class MatchMoveListener extends LootListener {

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if (e.getFrom().getBlock() != e.getTo().getBlock()) {
			MatchUser user = getMain().getMatches().getUser(e.getPlayer());
			if (user.isFighting() && user.getMatch().getState() == MatchState.Waiting)
				block(e.getPlayer(), e.getFrom(), e.getTo());
		}
	}

	void block(Player player, Location from, Location to) {
		player.teleport(
				new Location(from.getWorld(), from.getX(), from.getY(), from.getZ(), to.getYaw(), to.getPitch()));
	}
}

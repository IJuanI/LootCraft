package me.juanco.lc.match.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.match.objects.MatchUser;

public class MatchDamageListener extends LootListener {

	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player)
			if (e.getFinalDamage() >= ((Player) e.getEntity()).getHealth()) {
				MatchUser user = getMain().getMatches().getUser((Player) e.getEntity());
				if (user.isFighting()) {
					user.getMatch().lose(user);
					e.setDamage(0);
				}
			}
	}
}

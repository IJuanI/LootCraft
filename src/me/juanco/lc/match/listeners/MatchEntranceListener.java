package me.juanco.lc.match.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import me.juanco.lc.api.LootListener;

public class MatchEntranceListener extends LootListener {

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		getMain().getMatches().remove(e.getPlayer());
	}
}

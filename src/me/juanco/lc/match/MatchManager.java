package me.juanco.lc.match;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.kits.KitPlayer;
import me.juanco.lc.match.enums.MatchTier;
import me.juanco.lc.match.objects.Arena;
import me.juanco.lc.match.objects.Match;
import me.juanco.lc.match.objects.MatchUser;
import me.juanco.lc.match.objects.TierWaitlist;

public class MatchManager {

	private final List<TierWaitlist> waitlists = new ArrayList<TierWaitlist>();

	private final List<Arena> available = new ArrayList<Arena>();
	private final List<Match> fighting = new ArrayList<Match>();

	private final Random random = new Random();

	private final Main core;
	private final MatchGUI gui;

	protected MatchManager(Main core) {
		this.core = core;
		gui = new MatchGUI(this);
	}

	protected void addArena(Arena arena) {
		available.add(arena);
	}

	public void deleteArena(String id) {
		int index = Main.ioUtils.indexOf(available, id);

		if (index > -1)
			available.remove(index);
	}

	protected void disable() {
		for (Match match : fighting)
			end(match);
	}

	public void switchQueue(MatchUser user) {
		if (user.isWaiting())
			dequeue(user);
		else
			queue(user);
	}

	public void queue(MatchUser user) {
		KitPlayer kitPlayer = core.getKits().getPlayer(user.getPlayer());

		int level = kitPlayer.getLevel();
		if (level < 0)
			level = 0;
		else if (level > MatchTier.values().length * 15)
			level = MatchTier.values().length * 15;

		MatchTier tier = MatchTier.values()[level / 15];

		int bestItemLevel = kitPlayer.getBestItemLevel();

		int bestItemTier = 0;

		if (bestItemLevel < 8)
			bestItemTier = 1;
		else if (bestItemLevel < 13)
			bestItemTier = 2;
		else if (bestItemLevel < 18)
			bestItemTier = 3;
		else if (bestItemLevel < 22)
			bestItemTier = 4;
		else
			bestItemTier = 5;
		
		if (tier.ordinal() + 3 < bestItemTier * 2) {
			tier = MatchTier.values()[bestItemTier * 2 - 3];
			Message.send(Messages.ALERTS.PVP.highWep, user.getPlayer());
		}
		
		int index = Main.ioUtils.indexOf(waitlists, tier);
		if (index < 0) {
			waitlists.add(new TierWaitlist(tier, this));
			index = waitlists.size() - 1;
		}

		waitlists.get(index).queue(user).flush();

		Message.send(Messages.ALERTS.PVP.queue, user.getPlayer());
		if (available.isEmpty() && fighting.isEmpty())
			Message.send(Messages.ALERTS.PVP.arena, user.getPlayer());
	}

	public void dequeue(MatchUser user) {
		if (user.isWaiting()) {
			user.getWaitlist().dequeue(user);
			Message.send(Messages.ALERTS.PVP.dequeue, user.getPlayer());
		}
	}

	public boolean match(MatchTier tier, MatchUser... rivals) {
		if (!available.isEmpty()) {
			Arena arena;
			int size = available.size();

			if (size > 1)
				arena = available.remove(random.nextInt(size));
			else
				arena = available.remove(0);

			for (MatchUser user : rivals) {
				user.setArena(arena);
				Message.send(Messages.ALERTS.PVP.found, user.getPlayer());
			}

			rivals[0].setOponents(rivals[1]);
			rivals[1].setOponents(rivals[0]);

			KitPlayer k1 = core.getKits().getPlayer(rivals[0].getPlayer());
			KitPlayer k2 = core.getKits().getPlayer(rivals[1].getPlayer());

			int l1 = k1.getLevel();
			int l2 = k2.getLevel();

			int r1 = tier.getReward(l1, l2);
			int r2 = tier.getReward(l2, l1);

			rivals[0].setReward(r1);
			rivals[1].setReward(r2);

			gui.open(l1, l2, r1, r2, k1.getBestItem(), k2.getBestItem(), k1.getPlayer(), k2.getPlayer());

			return true;
		}
		return false;
	}

	public void accept(Player player) {
		MatchUser user = core.getMatches().getUser(player);
		boolean a = user.hasAccepted();
		user.accept();

		if (user.getOponents()[0].hasAccepted())
			combat(user.getArena(), user, user.getOponents()[0]);
		else if (a)
			Message.send(Messages.ALERTS.PVP.wait2, user.getPlayer());
		else
			Message.send(Messages.ALERTS.PVP.wait1, user.getPlayer());
	}

	public void deny(Player player) {
		MatchUser user = core.getMatches().getUser(player);
		if (user.hasOponents()) {
			MatchUser oponent = user.getOponents()[0];

			user.setOponents((MatchUser[]) null);
			oponent.setOponents((MatchUser[]) null);

			gui.close(oponent.getPlayer());

			Message.send(Messages.ALERTS.PVP.exitSelf, user.getPlayer());
			Message.send(Messages.ALERTS.PVP.exitOther, oponent.getPlayer());

			user.reset();
			oponent.reset();

			queue(oponent);
		}
	}


	private void combat(Arena arena, MatchUser... rivals) {
		Match match = new Match(arena, gui, rivals);

		for (MatchUser rival : rivals)
			rival.setMatch(match);

		fighting.add(match);
	}

	public void end(Match match) {
		fighting.remove(match);
		available.add(match.getArena());
		for (MatchUser user : match.getRivals())
			user.reset();
	}

}

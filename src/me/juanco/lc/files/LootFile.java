package me.juanco.lc.files;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import me.juanco.lc.core.Main;

public class LootFile {

	public FileConfiguration file = null;
	public final File ioFile;
	public final String name;

	public LootFile(Plugin pl, String name) {
		this((Main) pl, name, false, null);
	}

	public LootFile(Main pl, String name, Class<?> parser) {
		this(pl, name, false, parser);
	}

	public LootFile(Main pl, String name, boolean temp, Class<?> parser) {
		this.name = name;
		File t;
		try {
			if (temp)
				t = File.createTempFile("t", name, Main.folder);
			else
				t = new File(Main.folder.getPath() + "/" + name);
		} catch (Exception ex) {
			t = new File(Main.folder.getPath() + "/" + name);
		}

		ioFile = t;

		if (!Main.folder.exists()) Main.folder.mkdirs();
		if (!ioFile.exists())
			try {
				InputStream in;
				if ((in = pl.getResource(name)) != null) {
					OutputStream out = new FileOutputStream(ioFile);
					try {
						byte[] buf = new byte['?'];
						int result;
						while ((result = in.read(buf)) > 0) {
							out.write(buf, 0, result);
							buf = new byte['?'];
						}
					} catch(Exception ex) { ioFile.createNewFile();
					} finally { out.close(); in.close(); }
				} else {
					if (!ioFile.exists())
						ioFile.createNewFile();

					file = YamlConfiguration.loadConfiguration(ioFile);

					try {
						if (parser != null)
							parser.getConstructor(Main.class, LootFile.class).newInstance(pl, this);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				if (file == null)
					file = YamlConfiguration.loadConfiguration(ioFile);
				defaults();
			} catch(Exception ex) { Bukkit.getConsoleSender().sendMessage("Error when creating the file " + name); }
		else {
			InputStream in = pl.getResource(name);
			file = YamlConfiguration.loadConfiguration(ioFile);

			if (in != null)
				try {
					File f = new File(name);
					OutputStream out = new FileOutputStream(f);

					try {
						byte[] buf = new byte['?'];
						int result;
						while ((result = in.read(buf)) > 0) {
							out.write(buf, 0, result);
							buf = new byte['?'];
						}
					} finally {
						out.close();
						in.close();
					}

					FileConfiguration conf = YamlConfiguration.loadConfiguration(f);

					for (String l1 : conf.getKeys(false))
						if (!file.isConfigurationSection(l1))
							if (conf.getConfigurationSection(l1).getKeys(false).isEmpty())
								file.set(l1, conf.get(l1));
							else {
								List<String> pendient = new ArrayList<String>();
								for (String key : conf.getConfigurationSection(l1).getKeys(false))
									pendient.add(l1 + "." + key);

								while (!pendient.isEmpty()) {
									String some = pendient.remove(0);
									Set<String> keys = conf.getConfigurationSection(some).getKeys(false);
									if (!keys.isEmpty())
										for (String key : keys)
											pendient.add(some + "." + key);
									else
										file.set(some, conf.get(some));
								}
							}
						else
							continue;

					save();
				} catch (Exception ex) {
				}
		}

	}

	public void save() {
		try { file.save(ioFile);
		} catch (IOException e) { e.printStackTrace(); }
	}

	protected void defaults() {

	}
}

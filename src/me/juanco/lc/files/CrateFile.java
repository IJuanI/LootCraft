package me.juanco.lc.files;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.parser.CrateFileParser;

public class CrateFile extends LootFile {

	public final long money;
	public final long tokens;

	public CrateFile(Main pl) {
		super(pl, "crate.config", CrateFileParser.class);

		money = file.getLong("Crates.common.cost.money");
		tokens = file.getLong("Crates.common.cost.tokens");
	}
}

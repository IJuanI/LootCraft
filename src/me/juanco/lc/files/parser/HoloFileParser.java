package me.juanco.lc.files.parser;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.LootFile;
import me.juanco.lc.rsa.RSA;

public class HoloFileParser {

	public HoloFileParser(Main core, LootFile lFile) {
		File file = new File("plugins/Mine Regen/holograms.data");
		RSA rsa = new RSA('@');

		if (!file.exists())
			return;

		FileConfiguration conf = YamlConfiguration.loadConfiguration(file);

		for (String id : conf.getKeys(false)) {
			ConfigurationSection sec = conf.getConfigurationSection(id);
			StringBuilder builder = new StringBuilder();
			if (sec.contains("a")) {
				String[] locData = sec.getString("a").split(",");
				Location loc = new Location(Bukkit.getWorld(locData[0]), Double.valueOf(locData[1]),
						Double.valueOf(locData[2]), Double.valueOf(locData[3]));
				loc.setY(loc.getY() - 1);

				DecimalFormat format = new DecimalFormat("#0.##");
				builder.append(loc.getWorld().getName()).append(',').append(format.format(loc.getX())).append(',')
						.append(format.format(loc.getY())).append(',').append(format.format(loc.getZ()));
			}

			builder.append((char) 251);

			List<String> lines = sec.getStringList("c");
			if (!lines.isEmpty()) {
				builder.append(lines.get(0));
				for (int i = 1; i < lines.size(); i++)
					builder.append(',').append(lines.get(i));
			}

			if (sec.contains("d"))
				builder.append((char) 251).append(sec.getString("d"));

			String result = builder.toString();

			lFile.file.set(id, rsa.encrypt(result));
		}


		lFile.save();
	}
}

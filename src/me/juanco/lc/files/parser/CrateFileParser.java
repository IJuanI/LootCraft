package me.juanco.lc.files.parser;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.LootFile;

public class CrateFileParser {

	public CrateFileParser(Main core, LootFile lFile) {
		File crateFile = new File("plugins/LootCrates/config.yml");

		if (!crateFile.exists())
			return;

		FileConfiguration crateConf = YamlConfiguration.loadConfiguration(crateFile);

		File localFile = new File(Main.folder.getPath() + "/crate.config");

		try {
			localFile.createNewFile();
		} catch (Exception ex) {
			return;
		}

		double c1 = crateConf.getDouble("Crates.common.cost");
		double c2 = crateConf.getDouble("Crates.common.costTokens");

		crateConf.set("Crates.common.cost", null);
		crateConf.set("Crates.common.costTokens", null);

		crateConf.set("Crates.common.cost.money", (int) c1);
		crateConf.set("Crates.common.cost.tokens", (int) c2);

		try {
			crateConf.save(localFile);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

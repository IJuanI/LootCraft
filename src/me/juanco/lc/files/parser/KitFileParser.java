package me.juanco.lc.files.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.LootFile;

public class KitFileParser {

	public KitFileParser(Main core, LootFile lFile) {
		File kitFile = new File("plugins/Kit Modifier/config.yml");

		if (!kitFile.exists())
			return;

		FileConfiguration kitConf = YamlConfiguration.loadConfiguration(kitFile);

		File localFile = new File(Main.folder.getPath() + "/kits.data");

		try {
			localFile.createNewFile();
		} catch (Exception ex) {
			return;
		}

		FileConfiguration localConf = YamlConfiguration.loadConfiguration(localFile);

		for (String id : kitConf.getConfigurationSection("Kits").getKeys(false)) {
			ConfigurationSection localSec = localConf.createSection(id);
			ConfigurationSection kitSec = kitConf.getConfigurationSection("Kits." + id);

			if (kitSec.contains("damage"))
				localSec.set("damage", kitSec.getDouble("damage"));

			if (kitSec.contains("max hearts"))
				localSec.set("max hearts", kitSec.getDouble("max hearts"));

			if (kitSec.contains("command"))
				localSec.set("command", kitSec.getString("command"));

			if (kitSec.contains("item level"))
				localSec.set("item level", kitSec.getInt("item level"));

			if (kitSec.contains("items")) {
				String serial = kitSec.getStringList("items").get(0);
				String[] deserialized = serial.split(" ");
				localSec.set("item.id", deserialized[0]);

				for (int i = 2; i < deserialized.length; i++) {
					String[] current = deserialized[i].split(":");
					if (current[0].equals("name")) {
						String name = current[1].replaceAll("_", " ");
						localSec.set("item.name", name);
						continue;
					} else if (current[0].equals("lore")) {
						List<String> localLore = new ArrayList<String>();
						String[] kitLore = current[1].split("\\|");
						for (String kLore : kitLore)
							localLore.add(kLore.replaceAll("_", " "));
						localSec.set("item.lore", localLore);
						continue;
					} else if (current[0].equals("color")) {
						String[] colors = current[1].split(",");
						int r = Integer.parseInt(colors[0]);
						int g = Integer.parseInt(colors[1]);
						int b = Integer.parseInt(colors[2]);

						localSec.set("item.color.r", r);
						localSec.set("item.color.g", g);
						localSec.set("item.color.b", b);
						continue;
					} else
						continue;
				}
			}
		}

		try {
			localConf.save(localFile);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

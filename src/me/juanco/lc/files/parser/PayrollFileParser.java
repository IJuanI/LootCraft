package me.juanco.lc.files.parser;

import java.io.File;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.LootFile;

public class PayrollFileParser {

	public PayrollFileParser(Main core, LootFile lFile) {
		File payFile = new File("plugins/PayRolls/config.yml");

		if (!payFile.exists())
			return;

		File localFile = new File(Main.folder.getPath() + "/payroll.config");

		try {
			localFile.createNewFile();
		} catch (Exception ex) {
			return;
		}

		FileConfiguration payConf = YamlConfiguration.loadConfiguration(payFile);
		FileConfiguration localConf = YamlConfiguration.loadConfiguration(localFile);

		for (String id : payConf.getKeys(false)) {
			ConfigurationSection pay = payConf.getConfigurationSection(id);
			ConfigurationSection local = localConf.createSection("Quotas." + id);

			local.set("payout", pay.getInt("Payout"));

			int amount = pay.isInt("Quantity") ? pay.getInt("Quantity")
					: pay.contains("Quantity") ? Integer.parseInt(pay.getString("Quantity").split(",")[0]) : 1;

			List<String> items = pay.getStringList("Item");
			for (int i = 0; i < items.size(); i++)
				items.set(i, items.get(i) + "#" + amount);

			local.set("cost", items);
		}

		try {
			localConf.save(localFile);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

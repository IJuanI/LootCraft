package me.juanco.lc.files;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.parser.KitFileParser;

public class KitFile extends LootFile {

	public KitFile(Main pl) {
		super(pl, "kits.data", KitFileParser.class);
	}
}

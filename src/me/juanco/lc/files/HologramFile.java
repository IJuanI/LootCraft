package me.juanco.lc.files;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.parser.HoloFileParser;

public class HologramFile extends LootFile {

	public HologramFile(Main pl) {
		super(pl, "loot.holo", HoloFileParser.class);
	}

}

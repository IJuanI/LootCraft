package me.juanco.lc.files;

import org.bukkit.plugin.Plugin;

import me.juanco.lc.core.Main;
import me.juanco.lc.kits.leveltop.LevelTopFile;


public class Files {

	public static LootConfig config;
	public static LootLang lang;
	public static LootStorage storage;

	public static CrateFile crate;
	public static PayrollFiles payroll;
	public static VirtualFile virtual;
	public static KitFile kits;
	public static MatchFile matches;
	public static RegenFile regen;

	public static HologramFile holos;
	public static LightFile light;

	public static LevelTopFile levelTop;

	public Files(Main pl) {
		config = new LootConfig(pl);
		lang = new LootLang(pl);
		storage = new LootStorage(pl);

		crate = new CrateFile(pl);
		payroll = new PayrollFiles(pl);
		virtual = new VirtualFile(pl);
		kits = new KitFile(pl);
		matches = new MatchFile(pl);
		regen = new RegenFile(pl);

		holos = new HologramFile(pl);
		light = new LightFile(pl);

		levelTop = new LevelTopFile(pl);
	}

	public static class LootStorage extends LootFile {
		public LootStorage(Plugin pl) {
			super(pl, "loot.storage");
		}
	}

	public static class LootConfig extends LootFile {
		public LootConfig(Plugin pl) {
			super(pl, "loot.config");
		}
	}

	public class LootLang extends LootFile {
		public LootLang(Plugin pl) {
			super(pl, "loot.lang");
		}

		public String[] get(String path) {
			return file.isList(path) ? file.getStringList(path).toArray(new String[0])
					: new String[] { file.getString(path) };
		}
	}

	public static void disable() {
		payroll.disable();

		levelTop.save();
	}
}

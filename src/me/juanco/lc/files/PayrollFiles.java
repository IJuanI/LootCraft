package me.juanco.lc.files;

import org.bukkit.plugin.Plugin;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.parser.PayrollFileParser;

public class PayrollFiles {

	public final Config config;
	public final Temp temp;

	protected PayrollFiles(Main pl) {
		config = new Config(pl);
		temp = new Temp(pl);
	}

	public class Config extends LootFile {
		public Config(Main pl) {
			super(pl, "payroll.config", PayrollFileParser.class);
		}
	}

	public class Temp extends LootFile {
		public Temp(Plugin pl) {
			super(pl, "payroll.temp");
		}
	}

	protected void disable() {
		temp.save();
	}
}

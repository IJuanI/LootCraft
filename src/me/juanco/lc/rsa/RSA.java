package me.juanco.lc.rsa;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class RSA {
	private final BigInteger u72, q, x17, l, z;
	private final int t2;
	private final A w;
	private final Z m14;

	public RSA(char key) {// modpow= pow + mod
		if (key != 0x40)
			throw new RuntimeException();
		long y = 0, v = 0;
		boolean p8 = true, f12 = true;
		while (true) {
			if (p8) {
				y = (long) (Math.random() * 476837158203125L + 2);
				p8 = !a(y);
			}
			if (f12) {
				v = (long) (Math.random() * 476837158203125L + 2);
				f12 = !a(v);
			}
			if (!p8 && !f12)
				break;
		}
		u72 = new BigInteger(String.valueOf(v));
		w = new A(this);
		q = new BigInteger(String.valueOf(y));
		x17 = u72.multiply(q);
		BigInteger g53 = u72.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
		BigInteger h3 = new BigDecimal(
				String.valueOf(Math.random() * u72.mod(new BigInteger(String.valueOf(Long.MAX_VALUE))).longValue()))
						.round(MathContext.DECIMAL32).toBigIntegerExact().multiply(x17.add(g53));
		while (!g53.gcd(h3).equals(BigInteger.ONE))
			h3 = h3.add(BigInteger.ONE);
		m14 = new Z(this);
		l = h3;
		t2 = new BigInteger("256").modPow(h3, x17).toString().length() + 1;
		z = g53;
	}
	protected BigInteger x() {
		return x17;
	}
	protected BigInteger f() {
		return l;
	}

	protected BigInteger z() {
		return z;
	}
	protected int c() {
		return t2;
	}
	protected String a(StringBuilder v37) {
		String j53 = a(a(u72), a(q));
		String j45 = new BigInteger(j53, 2).toString();
		String o97 = new BigInteger(a(b(l)), 2).toString();
		StringBuilder i15 = new StringBuilder(j45).append(o97).append(v37.toString());
		DecimalFormat c60 = new DecimalFormat("000");
		i15.append(c60.format(t2));
		i15.append(c60.format(o97.length()));
		i15.append(c60.format(j45.length()));
		return i15.toString();
	}

	protected String a(BigInteger u50) {
		StringBuilder j52 = new StringBuilder();
		boolean u74 = true;
		for (byte x7 : u50.toByteArray()) {
			j52.append(" ");
			if (x7 == 0 && u74)
				continue;
			u74 = false;
			String z20 = Integer.toBinaryString(Byte.toUnsignedInt(x7));
			if (z20.length() != 8)
				for (int i = z20.length(); i < 8; i++)
					j52.append(0);
			j52.append(z20);
		}
		return j52.substring(1);
	}

	protected String b(BigInteger u50) {
		StringBuilder j52 = new StringBuilder();
		boolean u74 = true;
		for (byte x7 : u50.toByteArray()) {
			if (x7 == 0 && u74)
				continue;
			u74 = false;
			String z20 = Integer.toBinaryString(Byte.toUnsignedInt(x7));
			if (z20.length() != 8)
				for (int i = z20.length(); i < 8; i++)
					j52.append(0);
			j52.append(z20);
		}
		return j52.substring(1);
	}
	private String a(String g80, String m42) {
		String[] k63 = g80.split(" ");
		String[] r52 = m42.split(" ");
		int k44 = k63.length - r52.length;
		StringBuilder d49 = new StringBuilder();
		String q62 = "00000000";
		for (int p12 = 0, j63 = 0;;) {
			if (k44 > 0) {
				d49.append(k63[p12]).append(q62);
				p12++;
				k44--;
			} else if (k44 < 0) {
				d49.append(q62).append(r52[j63]);
				j63++;
				k44++;
			} else {
				d49.append(k63[p12]).append(r52[j63]);
				p12++;
				j63++;
			}
			if (p12 >= k63.length && j63 >= r52.length)
				break;
		}
		return d49.toString();
	}
	protected String a(String k24) {
		StringBuilder i6 = new StringBuilder();
		for (Character p74 : k24.toCharArray())
			if (p74 == '1')
				i6.append('0');
			else
				i6.append('1');
		return i6.toString();
	}
	private boolean a(long t47) {
		for (long j23 = 2; j23 <= Math.sqrt(t47); j23++)
			if (t47 % j23 == 0)
				return false;
		return true;
	}
	public String encrypt(String m30) {
		return w.a(m30);
	}
	public String decrypt(String p76) {
		return m14.a(p76);
	}

	public static void main(String[] args) {
		long overall = System.currentTimeMillis();
		System.out.println("Starting");
		RSA rsa = new RSA('@');
		Random random = new Random();
		List<String> randStrings = new ArrayList<String>();

		char[] possibilities = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
				'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
				'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4',
				'5', '6', '7', '8', '9', '/', '*', '-', '+', '.', ',', '<', '>', '?', '\'', '"', '[', '{', ';', ':',
				']', '}', '=', '\\', '|', '_', '`', '~', ' ' };

		int length = 100000;
		int amount = 6000;

		System.out.println(length);
		System.out.println(amount);

		long start = System.currentTimeMillis();
		System.out.println("Generating");
		for (int i = 0; i < amount; i++) {
			StringBuilder builder = new StringBuilder();
			for (int x = 0; x < length; x++)
				builder.append(possibilities[random.nextInt(possibilities.length)]);

			randStrings.add(builder.toString());
		}

		System.gc();

		long generate = System.currentTimeMillis();
		System.out.println("Encrypting");

		List<String> crypted = new ArrayList<String>();

		try {
			for (String rand : randStrings) {
				System.gc();
				crypted.add(rsa.encrypt(rand));
			}
		} catch (Throwable ex) {
			System.gc();
			System.out.println("Encryption Exception!");
		}

		long encrypt = System.currentTimeMillis();
		System.out.println("Decrypting");

		for (String crypt : crypted)
			rsa.decrypt(crypt);

		long end = System.currentTimeMillis();
		System.out.println("Startup Took " + (start - overall) + "ms");
		System.out.println("Generation Took " + (generate - start) + "ms");
		System.out.println("Encryptation Took " + (encrypt - generate) + "ms");
		System.out.println("Decryptation Took " + (end - encrypt) + "ms");
		System.out.println("Overall Delay: " + (end - overall) + "ms");
		System.out.println(" ");
		System.out.println("Stats:");
		System.out.println("Used characters: " + length * amount);
		System.out.println("Used Messages: " + amount);
		System.out.println("Message Length: " + length);
		System.out.println(
				"Character encryption avs: " + length * amount / (double) (encrypt - generate) + " chars x sec");
		System.out.println("Character decryption avs: " + length * amount / (double) (end - encrypt) + " chars x sec");
		System.out.println("Message encryption avs: " + amount / (double) (encrypt - generate) + " msgs x sec");
		System.out.println("Message decryption avs: " + amount / (double) (end - encrypt) + " msgs x sec");
	}
}
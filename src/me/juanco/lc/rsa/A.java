package me.juanco.lc.rsa;

import java.math.BigInteger;

public final class A {
	private final RSA a;
	protected A(RSA b) {
		a = b;
	}
	public String a(String b) {
		if (b == null || b.isEmpty())
			return "";
		StringBuilder d = new StringBuilder();
		for (int a = 0; a < b.length(); a++) {
			BigInteger h = new BigInteger(Integer.toString(b.charAt(a))).modPow(this.a.f(), this.a.x());
			int c = h.toString().length();
			if (this.a.c() > c)
				for (; this.a.c() > c; c++)
					d.append(0);
			d.append(h);
			if (a % 30 == 29)
				System.gc();
		}
		return a.a(d);
	}
}
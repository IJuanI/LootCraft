package me.juanco.lc.rsa;

import java.math.BigInteger;

public final class Z {
	private final RSA k;
	protected Z(RSA a) {
		k = a;
	}
	public String a(String f) {
		for (Character j : f.toCharArray())
			if (!Character.isDigit(j))
				return f;
		Integer d = Integer.parseInt(f.substring(f.length() - 3));
		BigInteger a = new BigInteger(f.substring(0, d));
		f = f.substring(d, f.length() - 3);
		String i = k.a(a);
		StringBuilder t = new StringBuilder(), k = new StringBuilder();
		Boolean l = true;
		for (String x : i.split(" ")) {
			if (l)
				t.append(x);
			else
				k.append(x);
			l = !l;
		}
		BigInteger u = new BigInteger(t.toString(), 2);
		BigInteger b12 = new BigInteger(k.toString(), 2);
		BigInteger b11 = u.subtract(BigInteger.ONE).multiply(b12.subtract(BigInteger.ONE));
		Integer b7 = Integer.parseInt(f.substring(f.length() - 3));
		BigInteger b = new BigInteger(this.k.a(this.k.b(new BigInteger(f.substring(0, b7)))), 2);
		f = f.substring(b7, f.length() - 3);
		BigInteger pn = b.modPow(new BigInteger("-1"), b11);
		BigInteger q = u.multiply(b12);
		Integer r = Integer.parseInt(f.substring(f.length() - 3));
		f = f.substring(0, f.length() - 3);

		StringBuilder w = new StringBuilder();
		for (Integer j = 0; j < f.length() / r; j++)
			w.append((char) new BigInteger(f.substring(j * r, (j + 1) * r)).modPow(pn, q).intValueExact());
		return w.toString();
	}
}
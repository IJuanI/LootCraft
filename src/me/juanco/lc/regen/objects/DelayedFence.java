package me.juanco.lc.regen.objects;

import org.bukkit.Sound;
import org.bukkit.block.Block;

public class DelayedFence {

	Block fence;
	int delay;

	public DelayedFence(Block fence) {
		this.fence = fence;
		delay = 5;
	}

	public void update() {
		delay = 5;
	}

	@SuppressWarnings("deprecation")
	public boolean flush() {
		delay--;
		if (delay == 0) {
			byte data = fence.getData();
			if (data > 4) data -= 4;
			if (data == 4)
				data = 0;
			fence.setData(data);
			fence.getWorld().playSound(fence.getLocation(), Sound.DOOR_CLOSE, 1, 1);
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Block)
			return fence.equals(obj);
		return super.equals(obj);
	}
}

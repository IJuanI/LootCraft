package me.juanco.lc.regen.objects;

import org.bukkit.Material;

public class ListedID {

	private final Material mat;
	private final byte data;

	public ListedID(Material mat, byte data) {
		this.mat = mat;
		this.data = data;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ListedID) {
			ListedID id = (ListedID) obj;
			return id.data == data && (id.mat == null && mat == null || id.mat.equals(mat));
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return mat.toString() + ":" + data;
	}
}

package me.juanco.lc.regen.objects;

import org.bukkit.Bukkit;
import org.bukkit.entity.Sheep;
import org.bukkit.event.entity.SheepRegrowWoolEvent;

public class RegenSheep {

	private final Sheep sheep;
	private int delay = 20;

	public RegenSheep(Sheep sheep) {
		this.sheep = sheep;
	}

	public boolean flush() {
		delay--;
		if (delay < 1) {
			Bukkit.getPluginManager().callEvent(new SheepRegrowWoolEvent(sheep));
			return true;
		}
		return false;
	}

	public void end() {
		Bukkit.getPluginManager().callEvent(new SheepRegrowWoolEvent(sheep));
		delay = 0;
	}
}

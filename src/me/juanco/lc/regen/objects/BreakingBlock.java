package me.juanco.lc.regen.objects;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import me.juanco.lc.core.Main;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockBreakAnimation;

public class BreakingBlock {

	private static final int totalDuration = 15;
	private static Integer[] anims = new Integer[10];

	static {
		for (int index = 0; index < 10; index++)
			anims[index] = (int) (totalDuration / 10D * (index + 1));
	}

	public final Block block;
	private BlockPosition blockPos;

	public int life = 0;
	public CraftPlayer breaker;

	public BreakingBlock(Block block, Player breaker) {
		this.block = block;
		this.breaker = (CraftPlayer) breaker;

		Location loc = block.getLocation();
		blockPos = new BlockPosition(loc.getX(), loc.getY(), loc.getZ());
	}

	public boolean flush() {
		life++;

		int index = Main.ioUtils.indexOf(anims, life);
		if (index > -1)
			if (index == 9) {
				animate(-1);
				block.breakNaturally();

				return true;
			} else
				animate(index + 1);

		return false;
	}

	private void animate(int animation) {
		breaker.getHandle().playerConnection.sendPacket(new PacketPlayOutBlockBreakAnimation(11, blockPos, animation));
	}

	public void animate() {
		animate(-1);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return ((Player) obj).equals(breaker);
		return super.equals(obj);
	}
}

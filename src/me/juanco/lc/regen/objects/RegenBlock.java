package me.juanco.lc.regen.objects;

import org.bukkit.Material;
import org.bukkit.block.Block;

@SuppressWarnings("deprecation")
public class RegenBlock {

	private final Block block;

	private final Material mat;
	private final byte data;

	private int delay;

	public RegenBlock(Block block, int regenTime) {
		this.block = block;

		mat = block.getType();
		data = block.getData();
		delay = regenTime;
	}

	public boolean flush() {
		delay--;
		if (delay < 0) {
			end();
			return true;
		}
		return false;
	}

	public void end() {
		block.setType(mat, false);
		block.setData(data);
	}
}

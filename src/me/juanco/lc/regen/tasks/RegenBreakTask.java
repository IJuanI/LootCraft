package me.juanco.lc.regen.tasks;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.core.Main;
import me.juanco.lc.regen.objects.BreakingBlock;

public class RegenBreakTask extends BukkitRunnable {

	private static ConcurrentSet<BreakingBlock> breaking = new ConcurrentSet<BreakingBlock>();

	@Override
	public void run() {
		for (BreakingBlock block : breaking)
			if (block.flush())
				breaking.remove(block);
	}

	public static void addWood(BreakingBlock wood) {
		breaking.add(wood);
	}

	public static boolean contains(Player broker) {
		return Main.ioUtils.indexOf(breaking, broker) > -1;
	}

	public static void remove(Player player) {
		BreakingBlock wood = Main.ioUtils.get(breaking, player);
		
		if (wood != null) {
			breaking.remove(wood);
			wood.animate();
		}
	}
}

package me.juanco.lc.regen.tasks;

import java.util.Set;

import org.bukkit.block.Block;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.core.Main;
import me.juanco.lc.regen.objects.DelayedFence;

public class RegenFenceTask extends LootTask {

	private static Set<DelayedFence> fences = new ConcurrentSet<DelayedFence>();

	public RegenFenceTask() {
		super(1, true);
	}

	@Override
	public void run() {
		for (DelayedFence fence : fences)
			if (fence.flush())
				fences.remove(fence);
	}

	public static void update(Block block) {
		DelayedFence fence = Main.ioUtils.get(fences, block);

		if (fence == null)
			fences.add(new DelayedFence(block));
		else
			fence.update();
	}

}

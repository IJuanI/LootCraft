package me.juanco.lc.regen.tasks;

import java.util.Set;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.regen.objects.RegenSheep;

public class RegenSheepTask extends LootTask {

	private static Set<RegenSheep> sheeps = new ConcurrentSet<RegenSheep>();

	public RegenSheepTask() {
		super(1, true);
	}

	@Override
	public void run() {
		for (RegenSheep sheep : sheeps)
			if (sheep.flush())
				sheeps.remove(sheep);
	}

	public static void add(RegenSheep sheep) {
		sheeps.add(sheep);
	}

	public static void disable() {
		for (RegenSheep sheep : sheeps)
			sheep.end();
		sheeps.clear();
	}

}

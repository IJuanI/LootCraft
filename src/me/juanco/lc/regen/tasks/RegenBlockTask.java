package me.juanco.lc.regen.tasks;

import java.util.Set;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.regen.objects.RegenBlock;

public class RegenBlockTask extends LootTask {

	private static final Set<RegenBlock> pendient = new ConcurrentSet<RegenBlock>();

	public RegenBlockTask() {
		super(1, true);
	}

	@Override
	public void run() {
		for (RegenBlock block : pendient)
			if (block.flush())
				pendient.remove(block);
	}

	public static void disable() {
		for (RegenBlock block : pendient)
			block.end();
	}

	public static void add(RegenBlock block) {
		pendient.add(block);
	}
}

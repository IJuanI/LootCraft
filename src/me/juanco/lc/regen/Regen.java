package me.juanco.lc.regen;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;

import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.packets.PacketHandler;
import me.juanco.lc.regen.listeners.events.RegenAnimalListener;
import me.juanco.lc.regen.listeners.events.RegenBreakListener;
import me.juanco.lc.regen.listeners.events.RegenCropListener;
import me.juanco.lc.regen.listeners.events.RegenDropListener;
import me.juanco.lc.regen.listeners.events.RegenFenceListener;
import me.juanco.lc.regen.listeners.events.RegenMobListener;
import me.juanco.lc.regen.listeners.events.RegenSuffocationListener;
import me.juanco.lc.regen.listeners.packets.RegenInDigListener;
import me.juanco.lc.regen.listeners.packets.RegenOutBreakListener;
import me.juanco.lc.regen.objects.ListedID;
import me.juanco.lc.regen.tasks.RegenBlockTask;
import me.juanco.lc.regen.tasks.RegenBreakTask;
import me.juanco.lc.regen.tasks.RegenFenceTask;
import me.juanco.lc.regen.tasks.RegenSheepTask;

public class Regen {

	private int regenTime;

	private final List<ListedID> ids = new ArrayList<ListedID>();
	private final List<World> worlds = new ArrayList<World>();
	private final List<EntityType> mobs = new ArrayList<EntityType>();

	public Regen(Main core) {
		readFile();

		registerListeners();
		registerTasks();

		new RegenBreakTask().runTaskTimer(core, 20, 1);
	}

	private void registerListeners() {
		new RegenAnimalListener();
		new RegenBreakListener();
		new RegenCropListener();
		new RegenFenceListener();
		new RegenSuffocationListener();
		new RegenDropListener();
		new RegenMobListener();

		PacketHandler.register(new RegenInDigListener());
		PacketHandler.register(new RegenOutBreakListener());
	}

	private void registerTasks() {
		LootClock.addTask(new RegenBlockTask());
		LootClock.addTask(new RegenFenceTask());
		LootClock.addTask(new RegenSheepTask());
	}

	@SuppressWarnings("deprecation")
	private void readFile() {
		ConfigurationSection sec = Files.regen.file.getConfigurationSection("Settings");

		regenTime = sec.getInt("Regen Time");

		for (String world : sec.getStringList("Worlds")) {
			World w = Bukkit.getWorld(world);
			if (!worlds.contains(w))
				worlds.add(w);
		}

		sec = sec.getConfigurationSection("Ids");

		for (String mob : sec.getStringList("Mobs")) {
			EntityType type = Main.ioUtils.isInt(mob) ? EntityType.fromId(Integer.parseInt(mob))
					: EntityType.fromName(mob);
			if (type != null && !mobs.contains(type))
				mobs.add(type);
		}

		for (String id : sec.getStringList("General")) {
			String[] values = id.contains(":") ? id.split(":") : new String[] { id };
			Material mat = Material.matchMaterial(values[0]);
			byte data = values.length > 1 ? Byte.valueOf(values[1]) : 0;

			ListedID listed = new ListedID(mat, data);
			if (!ids.contains(listed))
				ids.add(listed);
		}

		for (String crop : sec.getStringList("Crops")) {
			Material mat = Material.matchMaterial(crop);

			if (mat == null)
				continue;

			for (byte data = 0; data < 8; data++) {
				ListedID listed = new ListedID(mat, data);
				if (!ids.contains(listed))
					ids.add(listed);
			}
		}
	}

	public int getRegenTime() {
		return regenTime;
	}

	public boolean isUsed(World world) {
		return worlds.contains(world);
	}

	public boolean isUsed(String world) {
		return worlds.contains(Bukkit.getWorld(world));
	}

	public boolean isUsed(EntityType type) {
		return mobs.contains(type);
	}

	public boolean isUsed(ListedID id) {
		return ids.contains(id);
	}
}

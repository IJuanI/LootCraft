package me.juanco.lc.regen.listeners.packets;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;

import me.juanco.lc.packets.PacketListener;
import me.juanco.lc.regen.objects.BreakingBlock;
import me.juanco.lc.regen.tasks.RegenBreakTask;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.Chunk;
import net.minecraft.server.v1_8_R3.PacketPlayInBlockDig;
import net.minecraft.server.v1_8_R3.PacketPlayInBlockDig.EnumPlayerDigType;

public class RegenInDigListener extends PacketListener {

	public RegenInDigListener() {
		super("PacketPlayInBlockDig");
	}

	@Override
	public boolean listen(Object message, Player target) {
		try {
			PacketPlayInBlockDig packet = (PacketPlayInBlockDig) message;

			if (packet.c().equals(EnumPlayerDigType.ABORT_DESTROY_BLOCK))
				RegenBreakTask.remove(target);
			else if (packet.c().equals(EnumPlayerDigType.START_DESTROY_BLOCK)) {
				BlockPosition pos = packet.a();
				Chunk chunk = ((CraftWorld) target.getWorld()).getHandle().chunkProviderServer
						.getChunkIfLoaded(pos.getX() >> 4, pos.getZ() >> 4);

				if (chunk == null)
					return false;

				Block block = chunk.bukkitChunk.getBlock(pos.getX() & 0xF, pos.getY(), pos.getZ() & 0xF);

				if (block.getType().equals(Material.LOG) || block.getType().equals(Material.LOG_2))
					if (target.getItemInHand().getType().equals(Material.WOOD_SPADE))
						RegenBreakTask.addWood(new BreakingBlock(block, target));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

}

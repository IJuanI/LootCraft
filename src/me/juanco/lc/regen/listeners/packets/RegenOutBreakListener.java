package me.juanco.lc.regen.listeners.packets;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import me.juanco.lc.packets.PacketListener;
import me.juanco.lc.regen.tasks.RegenBreakTask;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockBreakAnimation;

public class RegenOutBreakListener extends PacketListener {

	private final Field f;

	public RegenOutBreakListener() {
		super("PacketPlayOutBlockBreakAnimation");

		Field field = null;
		try {
			field = PacketPlayOutBlockBreakAnimation.class.getField("a");
			field.setAccessible(true);
		} catch (Exception ex) {
		} finally {
			f = field == null ? null : field;
		}
	}

	@Override
	public boolean listen(Object message, Player target) {
		try {
			if (f != null && RegenBreakTask.contains(target)) {
				PacketPlayOutBlockBreakAnimation packet = (PacketPlayOutBlockBreakAnimation) message;

				try {
					int a = f.getInt(packet);

					if (a != 11)
						return true;
				} catch (Exception ex) {
					return false;
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

}

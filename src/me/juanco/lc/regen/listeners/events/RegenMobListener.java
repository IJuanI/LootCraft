package me.juanco.lc.regen.listeners.events;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityCombustEvent;

import me.juanco.lc.api.LootListener;
import net.minecraft.server.v1_8_R3.EntityLiving;

public class RegenMobListener extends LootListener {

	@EventHandler
	public void onEntitySpawn(CreatureSpawnEvent e) {
		if (!e.getSpawnReason().equals(SpawnReason.CUSTOM) && e.getEntity() instanceof EntityLiving
				&& !(e.getEntity() instanceof Player) && !e.getEntityType().equals(EntityType.ARMOR_STAND))
			e.setCancelled(true);
	}

	@EventHandler
	public void onEntityBurn(EntityCombustEvent e) {
		if (e.getEntity() instanceof Zombie)
			e.setCancelled(true);
	}
}

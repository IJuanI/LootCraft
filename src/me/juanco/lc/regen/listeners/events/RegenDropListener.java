package me.juanco.lc.regen.listeners.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.api.Permissions;

public class RegenDropListener extends LootListener {

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		if (!e.getPlayer().hasPermission(Permissions.helper))
			e.setCancelled(true);
	}
}

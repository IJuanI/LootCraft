package me.juanco.lc.regen.listeners.events;

import org.bukkit.Material;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import me.juanco.lc.api.LootListener;

public class RegenCropListener extends LootListener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.PHYSICAL && e.getClickedBlock().getType().equals(Material.SOIL)) {
			e.setUseInteractedBlock(Result.DENY);
			e.setCancelled(true);
		}
	}
}

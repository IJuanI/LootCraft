package me.juanco.lc.regen.listeners.events;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import me.juanco.lc.api.LootListener;

public class RegenSuffocationListener extends LootListener {

	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player && e.getCause().equals(DamageCause.SUFFOCATION)) {
			e.setCancelled(true);

			Location loc = e.getEntity().getLocation();
			Location loc2 = loc.clone();
			loc2.setY(loc.getY() + 1);
			while (loc.getBlock().getType().isBlock() || loc2.getBlock().getType().isBlock()) {
				loc.setY(loc.getY() + 1);
				loc2.setY(loc2.getY() + 1);
			}

			e.getEntity().teleport(loc);
		}
	}
}

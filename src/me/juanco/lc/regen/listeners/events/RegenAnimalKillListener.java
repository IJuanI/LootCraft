package me.juanco.lc.regen.listeners.events;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;

public class RegenAnimalKillListener extends LootListener {

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		Player killer = e.getEntity().getKiller();
		if (killer != null && getMain().getRegen().isUsed(e.getEntity().getType())) {
			killer.giveExp(e.getDroppedExp());
			killer.playSound(e.getEntity().getLocation(), Sound.ORB_PICKUP, 1, 3);
			e.setDroppedExp(0);

			int i = Main.itemUtils.giveItems(killer, e.getDrops().toArray(new ItemStack[0]));
			if (i > -1)
				Message.sendWithReplaces(Messages.ERRORS.itemsDropped, killer, "%n", String.valueOf(i), "(s)",
						i > 1 ? "s" : "");

			e.getDrops().clear();
		}
	}
}

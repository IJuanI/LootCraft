package me.juanco.lc.regen.listeners.events;

import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.regen.objects.ListedID;
import me.juanco.lc.regen.objects.RegenBlock;
import me.juanco.lc.regen.tasks.RegenBlockTask;

public class RegenBreakListener extends LootListener {

	private final List<Integer> fortune1 = Lists.newArrayList(1,1,2);
	private final List<Integer> fortune2 = Lists.newArrayList(1,1,2,3);
	private final List<Integer> fortune3 = Lists.newArrayList(1,1,2,3,4);
	private final Random random = new Random();

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Block block;

		if (e.getBlock() != null && getMain().getRegen().isUsed(e.getPlayer().getWorld()))
			if (getMain().getRegen().isUsed(new ListedID((block = e.getBlock()).getType(), block.getData()))) {
				e.getPlayer().giveExp(e.getExpToDrop());
				e.getPlayer().playSound(block.getLocation(), Sound.ORB_PICKUP, 1, 2);
				e.setExpToDrop(0);

				ItemStack item;

				switch(block.getType()) {
				case GOLD_ORE:
					item = new ItemStack(Material.GOLD_INGOT, 1);
					break;
				case IRON_ORE:
					item = new ItemStack(Material.IRON_INGOT, 1);
					break;
				default:
					item = block.getDrops().iterator().next();
					break;
				}

				List<Integer> chances;

				switch(e.getPlayer().getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS)) {
				case -1: case 0:
					chances = Lists.newArrayList(1);
					break;
				case 1:
					chances = fortune1;
					break;
				case 2:
					chances = fortune2;
					break;
				default:
					chances = fortune3;
					break;
				}

				int multiplier = chances.size() > 1 ? chances.get(random.nextInt(chances.size())) : 1;

				item.setAmount(item.getAmount() * multiplier);

				RegenBlockTask.add(new RegenBlock(block, getMain().getRegen().getRegenTime()));

				block.setType(Material.AIR, false);

				int left = Main.itemUtils.giveItems(e.getPlayer(), item);
				if (left > 0)
					Message.sendWithReplaces(Messages.ERRORS.itemsDropped, e.getPlayer(), "%n", String.valueOf(left),
							"(s)", left > 1 ? "s" : "");
			}
	}
}

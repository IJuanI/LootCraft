package me.juanco.lc.regen.listeners.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Gate;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.regen.tasks.RegenFenceTask;

public class RegenFenceListener extends LootListener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock() != null
				&& e.getClickedBlock().getType().toString().endsWith("_GATE")) {

			Gate gate = (Gate) e.getClickedBlock().getState().getData();

			if (gate.isOpen())
				e.setCancelled(true);

			RegenFenceTask.update(e.getClickedBlock());
		}
	}
}

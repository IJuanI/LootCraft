package me.juanco.lc.regen.listeners.events;

import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.regen.objects.RegenSheep;
import me.juanco.lc.regen.tasks.RegenSheepTask;

public class RegenAnimalListener extends LootListener {

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player && !(e.getEntity() instanceof Player)
				&& getMain().getRegen().isUsed(e.getEntityType()) && !(e.getEntity() instanceof Sheep))
			e.setDamage(250D);
		else if (!(e.getDamager() instanceof Player && e.getEntity() instanceof Player))
			e.setCancelled(true);
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		if (e.getEntity() instanceof Player)
			return;

		Player killer = e.getEntity().getKiller();
		if (killer != null && getMain().getRegen().isUsed(e.getEntity().getType())
				&& !(e.getEntity() instanceof Sheep)) {
			killer.giveExp(e.getDroppedExp());
			killer.playSound(e.getEntity().getLocation(), Sound.ORB_PICKUP, 1, 3);
			e.setDroppedExp(0);

			int i = Main.itemUtils.giveItems(killer, e.getDrops().toArray(new ItemStack[0]));
			if (i > -1)
				Message.sendWithReplaces(Messages.ERRORS.itemsDropped, killer, "%n", String.valueOf(i), "(s)",
						i > 1 ? "s" : "");

			e.getDrops().clear();
		}
	}

	@EventHandler
	public void onPlayerShearEntity(PlayerShearEntityEvent e) {
		if (getMain().getRegen().isUsed(EntityType.SHEEP))
			RegenSheepTask.add(new RegenSheep((Sheep) e.getEntity()));
	}
}

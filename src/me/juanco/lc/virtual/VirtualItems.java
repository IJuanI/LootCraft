package me.juanco.lc.virtual;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;

public class VirtualItems {

	String[] nameColors = new String[] { "&6&l", "&e&l", "&b&l", "&d&l", "&c&l", "&f&l" };
	String[] loreColors = new String[] { "&f&l", "&a&l", "&d&l", "&c&l", "&e&l", "&6&l" };

	ItemStack selected = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5), "&a");
	ItemStack bin = Main.itemUtils.get(Material.HOPPER, "&7&lTRASH&8&l-&7&lCAN");

	ItemStack libraryBook = Main.itemUtils.get(Material.BOOK_AND_QUILL,
			"&a&lL&b&lo&c&lo&d&lt&e&l-&a&lL&b&li&c&lb&d&lr&a&la&b&lr&c&ly");
	String libraryWep = "&b&lWeapon";
	String libraryArmor = "&c&lArmor";
	String libraryProf = "&d&lProfession";

	private Virtual main;

	protected VirtualItems(Virtual main) {
		this.main = main;
	}

	public String getName(int index) {
		return Message.t(nameColors[index] + "Chest " + name(index));
	}

	public ItemStack getSeparator() {
		return Main.itemUtils.getSeparator();
	}

	public ItemStack getSelected() {
		return selected;
	}

	public ItemStack getTrashCan() {
		return bin;
	}

	public ItemStack getChest(int index, boolean locked) {
		Material mat;
		String name;
		String lore;

		int price = main.getUtils().getPrice(index);
		
		if (!locked || price < 1) {
			mat = Material.CHEST;
			name = nameColors[index] + "Chest " + name(index);
			lore = "&a&lUnlocked";
		} else {
			mat = Material.COBBLESTONE;
			name = nameColors[index] + "Not Unlocked";
			lore = loreColors[index] + Main.ioUtils.getPrice(price);
		}

		return Main.itemUtils.get(mat, name, lore);
	}

	public ItemStack getBook() {
		return libraryBook;
	}

	@SuppressWarnings("deprecation")
	public ItemStack getWep(int index) {
		return Main.itemUtils.get(Material.getMaterial(VirtualType.Weapon.getID(index)), libraryWep);
	}

	public ItemStack getArmor(int index) {
		ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
		meta.setDisplayName(Message.t(libraryArmor));

		int hex = Integer.parseInt(VirtualType.Armor.getHex(index), 16);
		int r = (hex & 0xFF0000) >> 16;
		int g = (hex & 0xFF00) >> 8;
		int b = hex & 0xFF;

		meta.setColor(Color.fromRGB(r, g, b));
		item.setItemMeta(meta);

		return item;
	}

	@SuppressWarnings("deprecation")
	public ItemStack getProf(int index) {
		return Main.itemUtils.get(Material.getMaterial(VirtualType.Profession.getID(index)), libraryProf);
	}

	String name(int index) {
		switch(index) {
		case 0:
			return "One";
		case 1:
			return "Two";
		case 2:
			return "Three";
		case 3:
			return "Four";
		case 4:
			return "Five";
		default:
			return "Six";
		}
	}
}

package me.juanco.lc.virtual;

import java.util.ArrayList;
import java.util.List;

import me.juanco.lc.virtual.gui.LibraryPage;

public class LibrarySection {

	private final List<LibraryPage> pages = new ArrayList<LibraryPage>();

	protected LibrarySection() {

	}

	protected void addPage(LibraryPage lPage, int page) {
		while (pages.size() < page)
			pages.add(null);

		pages.set(page - 1, lPage);
	}

	protected LibraryPage getPage(int page) {
		if (page < 1)
			page = 1;
		else if (page > pages.size())
			page = pages.size();

		return pages.get(page - 1);
	}

	protected int getSize() {
		return pages.size();
	}
}

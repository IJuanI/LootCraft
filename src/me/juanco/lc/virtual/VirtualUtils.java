package me.juanco.lc.virtual;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.inventory.LootInventoryContents;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;

public class VirtualUtils {

	private Virtual main;

	protected VirtualUtils(Virtual main) {
		this.main = main;
	}

	public LootInventoryContents getVirtualContents(Player player, int index) {
		UserFile user = getFile(player);

		LootInventoryContents contents = new LootInventoryContents(54);

		if (user.file.isConfigurationSection("Virtual Inventory." + index)) {
			ConfigurationSection section = user.file.getConfigurationSection("Virtual Inventory." + index);
			for (String id : section.getKeys(false)) {
				int slot = Integer.parseInt(id);
				ItemStack item = section.getItemStack(id);
				contents.setItem(slot, item);
			}
		}

		return contents;
	}

	public void saveVirtualChest(LootInventoryContents contents, Player player, int index) {
		UserFile user = getFile(player);
		String path = "Virtual Inventory." + index;
		ConfigurationSection section = user.file.isConfigurationSection(path) ? user.file.getConfigurationSection(path) : user.file.createSection(path);

		ItemStack[] conts = contents.getContents();

		for (int slot = 0; slot < conts.length; slot++) {
			if (slot % 9 == 8 || slot % 9 == 7)
				continue;
			ItemStack item = conts[slot];
			if (Main.itemUtils.validate(item))
				section.set(String.valueOf(slot), item);
			else
				section.set(String.valueOf(slot), null);
		}

		user.save();
	}

	public void appendChests(LootInventoryContents contents, UserFile user) {
		for (int i = 1; i < 4; i++)
			contents.setItem(26 + i * 9, main.getItems().getChest(i, !hasChest(i, user)));
	}

	public boolean hasChest(int chest, UserFile user) {
		return chest == 0 || user.file.contains("Chests") && user.file.getIntegerList("Chests").contains(chest);
	}

	public int getPrice(int index) {
		return Files.virtual.file.getInt("Chest Prices." + main.getItems().name(index));
	}

	UserFile getFile(Player player) {
		return UserFile.getFile(player.getUniqueId());
	}
}

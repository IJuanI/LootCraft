package me.juanco.lc.virtual;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;

public enum VirtualRarity {

	Common("&7&lCommon", 8), Uncommon("&2&lUncommon", 5), Rare("&1&lRare", 11), Epic("&5&lEpic",
			10), Legendary("&6&lLegendary", 1);

	private ItemStack item;
	private String name;

	private VirtualRarity(String name, int durability) {
		item = Main.itemUtils.get(new ItemStack(Material.WOOL, 1, (short) durability), name);
		this.name = Message.t(name);
	}

	public ItemStack getItem() {
		return item;
	}

	public String getName() {
		return name;
	}
}

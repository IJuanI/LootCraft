package me.juanco.lc.virtual.gui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.juanco.lc.api.inventory.LootPurchaser;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;

public class VirtualPurchaser extends LootPurchaser {

	@Override
	public void accept(Player player, int cost, byte[] data) {
		int index = data[0];

		if (Main.money.withdraw(player, cost)) {
			UserFile user = UserFile.getFile(player.getUniqueId());
			FileConfiguration file = user.file;

			List<Integer> chests = file.contains("Chests") ? file.getIntegerList("Chests") : new ArrayList<Integer>();
			chests.add(index);

			file.set("Chests", chests);

			user.save();

			new Message(player).msg(Messages.SUCCESS.purchase);
		} else
			new Message(player).msg(Messages.ERRORS.afford);
	}

	@Override
	public void deny(Player player, int cost, byte[] data) {
	}

	@Override
	public void close(Player player, byte[] data) {
	}

}

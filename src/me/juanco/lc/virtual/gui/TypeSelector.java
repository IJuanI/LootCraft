package me.juanco.lc.virtual.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.inventory.LootInventory;
import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;
import me.juanco.lc.virtual.Virtual;
import me.juanco.lc.virtual.VirtualRarity;

public class TypeSelector extends LootInventory {

	private final Inventory inv;
	private final Virtual main;
	private final int index;

	public TypeSelector(Virtual main, int index) {
		inv = Bukkit.createInventory(null, 27, Message.t("&b&lChoose Type"));

		VirtualRarity rarity = VirtualRarity.values()[index];

		short data = 0;

		if (index > 0)
			data = rarity.getItem().getDurability();

		ItemStack sep1 = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15),
				rarity.getName());
		ItemStack sep2 = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, data), rarity.getName());

		for (int slot = 0; slot < 27; slot++)
			if (slot == 11)
				inv.setItem(slot, main.getItems().getWep(index));
			else if (slot == 13)
				inv.setItem(slot, main.getItems().getArmor(index));
			else if (slot == 15)
				inv.setItem(slot, main.getItems().getProf(index));
			else if (slot % 9 == 0 || slot % 9 == 8)
				inv.setItem(slot, sep2);
			else
				inv.setItem(slot, sep1);

		this.main = main;
		this.index = index;
	}

	@Override
	protected void preOpenInventory(Player player) {
	}

	@Override
	protected void openInventory(Player player) {
		player.openInventory(inv);
	}

	@Override
	protected void closeInventory(Player player) {
		player.closeInventory();
	}

	@Override
	protected boolean inventoryClick(InventoryClickEvent e) {
		int t = -1;
		if (e.getSlot() == 11)
			t = 0;
		else if (e.getSlot() == 13)
			t = 1;
		else if (e.getSlot() == 15)
			t = 2;
		if (t >= 0)
			main.getPage(index, t, 1).open((Player) e.getWhoClicked());
		return true;
	}

	@Override
	protected void inventoryClose(InventoryCloseEvent e) {
	}
}

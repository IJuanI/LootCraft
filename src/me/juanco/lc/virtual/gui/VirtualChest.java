package me.juanco.lc.virtual.gui;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import me.juanco.lc.api.inventory.LootInventory;
import me.juanco.lc.api.inventory.LootInventoryContents;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.virtual.Virtual;

public class VirtualChest extends LootInventory {

	private final LootInventoryContents preset;
	private final Virtual main;
	private final int index;
	private final String name;

	public VirtualChest(Virtual main, int index) {
		preset = new LootInventoryContents(54);
		
		this.index = index;
		this.main = main;

		preset.multiSetItem(main.getItems().getSeparator(), 7, 16, 25, 34, 43, 52);
		preset.setItem(25 + index * 9, main.getItems().getSelected());

		preset.setItem(8, main.getItems().getBook());
		preset.setItem(17, main.getItems().getTrashCan());
		preset.setItem(26, main.getItems().getChest(0, true));

		name = main.getItems().getName(index);
	}

	@Override
	protected void preOpenInventory(Player player) {
	}

	@Override
	protected void openInventory(Player player) {
		LootInventoryContents content = main.getUtils().getVirtualContents(player, index);
		preset.insertInto(content);

		main.getUtils().appendChests(content, UserFile.getFile(player.getUniqueId()));

		player.openInventory(content.getInventory(name));
	}

	@Override
	protected void closeInventory(Player player) {
		player.closeInventory();
	}

	@SuppressWarnings("deprecation")
	@Override
	protected boolean inventoryClick(InventoryClickEvent e) {
		int slot = e.getSlot();
		if (slot % 9 == 7)
			return true;
		else if (slot % 9 == 8)
			switch (slot / 9) {
			case 0:
				main.getRaritySelector().open((Player) e.getWhoClicked());
				break;
			case 1:
				e.setCursor(null);
				break;
			default:
				int i = slot / 9 - 2;
				if (i != index)
					if (e.getCurrentItem().getType().equals(Material.CHEST))
						main.getChest(i).open((Player) e.getWhoClicked());
					else if (Main.money.hasEnough((OfflinePlayer) e.getWhoClicked(), main.getUtils().getPrice(i)))
						main.getPurchase(i).open((Player) e.getWhoClicked());
					else {
						new Message(e.getWhoClicked()).msg(Messages.ERRORS.afford);
						close((Player) e.getWhoClicked());
					}

				break;
			}
		else
			return false;
		return true;
	}

	@Override
	protected void inventoryClose(InventoryCloseEvent e) {
		main.getUtils().saveVirtualChest(new LootInventoryContents(e.getInventory().getContents()),
				(Player) e.getPlayer(), index);
	}

}

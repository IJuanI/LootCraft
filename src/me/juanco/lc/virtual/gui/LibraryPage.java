package me.juanco.lc.virtual.gui;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import me.juanco.lc.api.inventory.LootInventory;
import me.juanco.lc.api.inventory.LootInventoryContents;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.LootKit;
import me.juanco.lc.virtual.Virtual;
import me.juanco.lc.virtual.VirtualRarity;
import me.juanco.lc.virtual.VirtualType;

public class LibraryPage extends LootInventory {

	private final Inventory inv;
	private final int page, pages, index, type;
	private final Virtual main;

	public LibraryPage(Virtual main, String path, String name, int index, int type, int page) {
		hasDebug = true;

		this.main = main;
		this.index = index;
		this.type = type;
		this.page = page;

		try {
			List<String> items = Files.virtual.file.getStringList(path);

			int pages = 1 + (items.size() - 1) / 52;
			this.pages = pages;

			int pageLength = 6;
			int start = (page - 1) * 52;

			if (page > pages)
				page = pages;
			else if (page < 1)
				page = 1;

			if (page == pages)
				pageLength = (items.size() - start) / 9 + 1;

			if (pageLength > 6)
				pageLength = 6;

			LootInventoryContents contents = new LootInventoryContents(pageLength * 9);

			contents.setItem(0, Main.itemUtils.getPrevious());
			contents.setItem((pageLength - 1) * 9 + 8, Main.itemUtils.getNext());

			for (int i = 0; start < items.size() && i < 52; start++, i++) {
				LootKit kit = Kits.get(items.get(start).toLowerCase());
				if (kit != null)
					contents.add(kit.getItem());
				else
					System.out.println(items.get(start).toLowerCase());
			}

			inv = contents.getInventory(name);

		} catch (Exception ex) {
			onException(ex);
			throw ex;
		}
	}

	@Override
	protected void debug(Exception ex) {
		System.out.println("Page: " + page);
		System.out.println("Pages: " + pages);
		System.out.println("Index: " + index);
		System.out.println("Type: " + type);
	}

	@Override
	protected void preOpenInventory(Player player) {
	}

	@Override
	protected void openInventory(Player player) {
		player.openInventory(inv);
	}

	@Override
	protected void closeInventory(Player player) {
		player.closeInventory();
	}

	@Override
	protected boolean inventoryClick(InventoryClickEvent e) {
		if (e.getSlot() == 0) {
			int i = index;
			int t = type;
			int p = page;

			if (p > 1)
				p--;
			else {
				if (t > 0)
					t--;
				else {
					t = 2;
					if (i > 0)
						i--;
					else
						i = 4;
				}

				VirtualRarity vRarity = VirtualRarity.values()[i];
				VirtualType vType = VirtualType.values()[t];

				String path = new StringBuilder("Library.").append(vRarity.name()).append('.').append(vType.name())
						.toString();

				p = 1 + (Files.virtual.file.getStringList(path).size() - 1) / 52;
			}

			main.getPage(i, t, p).open((Player) e.getWhoClicked());
		} else if (e.getSlot() == inv.getSize() - 1) {
			int i = index;
			int t = type;
			int p = page;

			if (p < pages)
				p++;
			else {
				p = 1;
				if (t < 2)
					t++;
				else {
					t = 0;
					if (i < 4)
						i++;
					else
						i = 0;
				}
			}

			main.getPage(i, t, p).open((Player) e.getWhoClicked());
		}

		return true;
	}

	@Override
	protected void inventoryClose(InventoryCloseEvent e) {
	}
}

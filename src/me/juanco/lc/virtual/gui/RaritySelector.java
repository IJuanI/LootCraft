package me.juanco.lc.virtual.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.inventory.LootInventory;
import me.juanco.lc.chat.Message;
import me.juanco.lc.virtual.Virtual;
import me.juanco.lc.virtual.VirtualRarity;

public class RaritySelector extends LootInventory {

	private final Inventory inv;
	private final Virtual main;

	public RaritySelector(Virtual main) {
		inv = Bukkit.createInventory(null, 9, Message.t("&6&lChoose Rarity"));

		int slot = 0;

		for (VirtualRarity rarity : VirtualRarity.values()) {
			inv.setItem(slot, rarity.getItem());
			slot += 2;
		}

		ItemStack separator = main.getItems().getSeparator();

		for (slot = 1; slot < 8; slot += 2)
			inv.setItem(slot, separator);

		this.main = main;
	}

	@Override
	protected void preOpenInventory(Player player) {
	}

	@Override
	protected void openInventory(Player player) {
		player.openInventory(inv);
	}

	@Override
	protected void closeInventory(Player player) {
		player.closeInventory();
	}

	@Override
	protected boolean inventoryClick(InventoryClickEvent e) {
		if (e.getSlot() % 2 == 0)
			main.getSelector(e.getSlot() / 2).open((Player) e.getWhoClicked());
		return true;
	}

	@Override
	protected void inventoryClose(InventoryCloseEvent e) {
	}

}

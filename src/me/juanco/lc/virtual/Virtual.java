package me.juanco.lc.virtual;

import org.bukkit.Material;

import me.juanco.lc.api.inventory.PurchaseGUI;
import me.juanco.lc.core.Main;
import me.juanco.lc.virtual.gui.LibraryPage;
import me.juanco.lc.virtual.gui.RaritySelector;
import me.juanco.lc.virtual.gui.TypeSelector;
import me.juanco.lc.virtual.gui.VirtualChest;
import me.juanco.lc.virtual.gui.VirtualPurchaser;

public class Virtual {

	private final VirtualItems items;
	private final VirtualUtils utils;

	private final RaritySelector raritySelector;
	private final VirtualChest[] chests = new VirtualChest[4];
	private final TypeSelector[] selectors = new TypeSelector[5];
	private final LibrarySection[][] pages = new LibrarySection[5][3];
	private final PurchaseGUI[] purchase = new PurchaseGUI[3];

	public Virtual(Main main) {
		items = new VirtualItems(this);
		utils = new VirtualUtils(this);

		for (int i = 0; i < chests.length; i++)
			chests[i] = new VirtualChest(this, i);

		raritySelector = new RaritySelector(this);

		for (int i = 0; i < selectors.length; i++)
			selectors[i] = new TypeSelector(this, i);

		for (int i = 0; i < purchase.length; i++)
			purchase[i] = new PurchaseGUI(new VirtualPurchaser(), utils.getPrice(i + 1), Material.CHEST,
					"Virtual Chest " + items.name(i + 1), new byte[] { (byte) (i + 1) });
	}

	public VirtualItems getItems() {
		return items;
	}

	public VirtualUtils getUtils() {
		return utils;
	}

	public RaritySelector getRaritySelector() {
		return raritySelector;
	}

	public VirtualChest getChest(int index) {
		return chests[index];
	}

	public TypeSelector getSelector(int index) {
		return selectors[index];
	}

	public LibraryPage getPage(int index, int type, int page) {
		LibrarySection sec = pages[index][type];
		if (sec == null)
			pages[index][type] = sec = new LibrarySection();

		LibraryPage p = null;

		if (sec.getSize() <= page || (p = sec.getPage(page)) == null) {
			VirtualRarity vRarity = VirtualRarity.values()[index];
			VirtualType vType = VirtualType.values()[type];

			String name = new StringBuilder(vRarity.getName()).append(' ').append(vType.name()).toString();
			String path = new StringBuilder("Library.").append(vRarity.name()).append('.').append(vType.name())
					.toString();

			sec.addPage(p = new LibraryPage(this, path, name, index, type, page), page);
		}

		return p;
	}

	public PurchaseGUI getPurchase(int index) {
		return purchase[index - 1];
	}
}

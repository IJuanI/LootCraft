package me.juanco.lc.virtual;

public enum VirtualType {

	Weapon(279, 291, 293, 275, 272), Armor("C2C2C2", "921D", "28C3", "5A00C3", "C36900"), Profession(359, 398, 357, 372,
			341);

	private final int[] ids;
	private final String[] colors;

	private VirtualType(int... ids) {
		this.ids = ids;
		colors = new String[5];
	}

	private VirtualType(String... colors) {
		ids = new int[5];
		this.colors = colors;
	}

	public int getID(int index) {
		return ids[index];
	}

	public String getHex(int index) {
		return colors[index];
	}
}

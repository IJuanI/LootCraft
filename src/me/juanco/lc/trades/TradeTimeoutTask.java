package me.juanco.lc.trades;

import me.juanco.lc.api.clock.LootTask;

public class TradeTimeoutTask extends LootTask {

	private final Trades main;

	public TradeTimeoutTask(Trades main) {
		super(1, true);
		this.main = main;
	}

	@Override
	public void run() {
		main.tick();
	}

}

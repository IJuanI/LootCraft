package me.juanco.lc.trades;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.trades.objects.Trade;
import me.juanco.lc.trades.objects.TradeQueryEntry;

public class Trades {

	private final TradeGUI gui;

	private Set<TradeQueryEntry> tradeQuery = new ConcurrentSet<TradeQueryEntry>();
	private List<Trade> trades = new ArrayList<Trade>();
	public static int reqLevel;

	public Trades(Main core) {
		gui = new TradeGUI(this);
		LootClock.addTask(new TradeTimeoutTask(this));

		reqLevel = Files.config.file.getInt("Trade Level");
	}

	public TradeGUI getGui() {
		return gui;
	}

	public void trade(Player playerA, Player playerB) {
		TradeQueryEntry entryA = Main.ioUtils.get(tradeQuery, playerA);
		TradeQueryEntry entryB = Main.ioUtils.get(tradeQuery, playerB);
		Trade tradeA = Main.ioUtils.get(trades, playerA);

		if (entryA == null && entryB == null && tradeA == null) {
			tradeQuery.add(new TradeQueryEntry(playerA, playerB));

			Message.sendWithReplaces(Messages.ALERTS.TRADE.acceptO, playerA, "%target", playerB.getDisplayName());
			Message.sendWithReplaces(Messages.ALERTS.TRADE.acceptT, playerB, "%owner", playerA.getName());
		} else if (tradeA != null)
			Message.sendWithReplaces(Messages.ALERTS.TRADE.trading, playerA, "%target", playerB.getDisplayName());
		else if (entryB == null)
			if (entryA.getOwner().equals(playerA))
				Message.sendWithReplaces(Messages.ALERTS.TRADE.alreadyOQ, playerA, "%target",
						entryA.getTarget().getDisplayName());
			else
				Message.send(Messages.ALERTS.TRADE.alreadyOI, playerA);
		else if (entryA == null)
			if (entryB.getOwner().equals(playerB))
				Message.sendWithReplaces(Messages.ALERTS.TRADE.alreadyTQ, playerA, "%target", playerB.getDisplayName());
			else
				Message.sendWithReplaces(Messages.ALERTS.TRADE.alreadyTI, playerA, "%target",
						entryB.getTarget().getDisplayName());
		else if (!entryA.equals(entryB))
			if (entryA.getOwner().equals(playerA))
				Message.sendWithReplaces(Messages.ALERTS.TRADE.alreadyOQ, playerA, "%target",
						entryA.getTarget().getDisplayName());
			else
				Message.send(Messages.ALERTS.TRADE.alreadyOI, playerA);
		else if (!entryA.accept(playerA))
			Message.sendWithReplaces(Messages.ALERTS.TRADE.still, playerA, "%target", playerB.getDisplayName());
		else {
			Message.send(Messages.ALERTS.TRADE.started, playerA);
			Message.send(Messages.ALERTS.TRADE.started, playerB);

			tradeQuery.remove(entryA);
			start(playerA, playerB);
		}
	}

	public Trade getTrade(Player player) {
		return Main.ioUtils.get(trades, player);
	}

	private void start(Player owner, Player target) {
		trades.add(new Trade(owner, target));

		gui.open(owner);
		gui.open(target);
	}

	protected void cancel(Player canceler) {
		cancel(canceler, Main.ioUtils.get(trades, canceler));
	}

	private void cancel(Player canceler, Trade trade) {
		if (trade != null) {
			trades.remove(trade);
			trade.cancel();
		}
	}

	public void cancelRequest(Player player) {
		TradeQueryEntry entry = Main.ioUtils.get(tradeQuery, player);
		if (entry != null)
			if (entry.getOwner().equals(player)) {
				Message.send(Messages.ALERTS.TRADE.cancelQY, player);
				Message.sendWithReplaces(Messages.ALERTS.TRADE.cancelQO, entry.getTarget(), "%target",
						player.getDisplayName());
			} else
				deny(player);
		else
			Message.send(Messages.ALERTS.TRADE.request, player);
	}

	protected void end(Trade trade) {
		remove(trade);
		trade.endTrade(this);
	}

	protected void remove(Trade trade) {
		trades.remove(trade);
	}

	public void restore(HumanEntity player, Inventory inv) {
		List<ItemStack> items = new ArrayList<ItemStack>();
		ItemStack temp;

		for (int l = 2; l < 6; l++)
			for (int i = 0; i < 3; i++)
				if ((temp = inv.getItem(l * 9 + i)) != null)
					items.add(temp);

		int i = Main.itemUtils.giveItems((Player) player, items.toArray(new ItemStack[0]));
		if (i > -1)
			Message.sendWithReplaces(Messages.ERRORS.itemsDropped, player, "%n", String.valueOf(i), "(s)",
					i > 1 ? "s" : "");
	}

	public void accept(Player player) {
		TradeQueryEntry entry = Main.ioUtils.get(tradeQuery, player);
		if (entry == null || !entry.accept(player))
			Message.send(Messages.ALERTS.TRADE.request, player);
		else {
			Message.send(Messages.ALERTS.TRADE.started, entry.getOwner());
			Message.send(Messages.ALERTS.TRADE.started, entry.getTarget());

			tradeQuery.remove(entry);
			start(entry.getOwner(), entry.getTarget());
		}
	}

	public void deny(Player player) {
		TradeQueryEntry entry = Main.ioUtils.get(tradeQuery, player);

		if (entry == null) {
			Trade trade = Main.ioUtils.get(trades, player);
			if (trade != null)
				cancel(player, trade);
			else
				Message.send(Messages.ALERTS.TRADE.request, player);
		} else if (entry.accept(player)) {
			tradeQuery.remove(entry);
			Message.send(Messages.ALERTS.TRADE.declineY, player);
			Message.sendWithReplaces(Messages.ALERTS.TRADE.declineO,
					entry.getOwner().equals(player) ? entry.getTarget() : entry.getOwner(), "%target",
					player.getDisplayName());
		} else
			Message.send(Messages.ALERTS.TRADE.request, player);
	}

	public void disable() {
		String message = Message.getMsg(Messages.ALERTS.TRADE.cancel);

		for (TradeQueryEntry entry : tradeQuery) {
			new Message(entry.getOwner()).rawMsg(message);
			new Message(entry.getTarget()).rawMsg(message);
		}

		for (Trade trade : trades)
			trade.send(message).cancel();
	}

	protected void tick() {
		for (TradeQueryEntry entry : tradeQuery)
			if (entry.flush()) {
				tradeQuery.remove(entry);
				Message.send(Messages.ALERTS.TRADE.expireO, entry.getOwner());
				Message.sendWithReplaces(Messages.ALERTS.TRADE.expireT, entry.getTarget(), "%target",
						entry.getOwner().getDisplayName());
			}
	}
}

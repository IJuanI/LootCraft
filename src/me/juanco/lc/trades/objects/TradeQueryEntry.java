package me.juanco.lc.trades.objects;

import org.bukkit.entity.Player;

public class TradeQueryEntry {

	private final Player owner, target;
	private int life = 40;

	public TradeQueryEntry(Player owner, Player target) {
		this.owner = owner;
		this.target = target;
	}

	public boolean flush() {
		life--;
		if (life < 1)
			return true;
		return false;
	}

	public boolean accept(Player player) {
		if (target.equals(player))
			return true;
		return false;
	}

	public Player getOwner() {
		return owner;
	}

	public Player getTarget() {
		return target;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return owner.equals(obj) || target.equals(obj);
		return super.equals(obj);
	}
}

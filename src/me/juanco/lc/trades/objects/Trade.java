package me.juanco.lc.trades.objects;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.trades.Trades;

public class Trade {

	private final Player owner, target;
	private boolean a1 = false, a2 = false;

	public Trade(Player owner, Player target) {
		this.owner = owner;
		this.target = target;
	}

	public void cancel() {
		owner.closeInventory();
	}

	public Trade send(String message) {
		new Message(owner).rawMsg(message);
		new Message(target).rawMsg(message);

		return this;
	}

	public boolean accept(Player player) {
		if (owner.equals(player))
			a1 = true;
		else if (target.equals(player))
			a2 = true;

		return a1 && a2;
	}

	public Player getOther(HumanEntity player) {
		if (player.equals(owner))
			return target;
		else
			return owner;
	}

	public void endTrade(Trades main) {
		main.restore(owner, target.getOpenInventory().getTopInventory());
		main.restore(target, owner.getOpenInventory().getTopInventory());

		cancel();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return owner.equals(obj) || target.equals(obj);
		return super.equals(obj);
	}
}

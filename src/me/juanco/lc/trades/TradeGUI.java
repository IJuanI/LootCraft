package me.juanco.lc.trades;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.juanco.lc.api.inventory.LootInventory;
import me.juanco.lc.api.inventory.LootInventoryContents;
import me.juanco.lc.api.inventory.PurchaseGUI;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.trades.objects.Trade;

public class TradeGUI extends LootInventory {

	private final LootInventoryContents base;
	private final Trades main;

	public TradeGUI(Trades main) {
		base = new LootInventoryContents(54);

		base.multiSetItem(Main.itemUtils.getSeparator(), 1, 7, 9, 11, 12, 14, 15, 17, 21, 23, 30, 32, 39, 41, 48, 50);
		base.multiSetItem(Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), "&a"), 0, 2, 6,
				8);

		base.multiSetItem(PurchaseGUI.accept, 22, 31);
		base.multiSetItem(PurchaseGUI.deny, 40, 49);

		base.setItem(4, Main.itemUtils.get(Material.STAINED_GLASS_PANE, "&f&lLoot Trades"));
		base.multiSetItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 11), 10, 16);
		base.multiSetItem(
				Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 10), "&9&lLoot Trades"), 3, 5,
				13);

		this.main = main;
	}

	@Override
	protected void preOpenInventory(Player player) {
	}

	@Override
	protected void openInventory(Player player) {
		Player other = main.getTrade(player).getOther(player);

		setData(player, new Object[] { other, (byte) 0 });

		Inventory inv = base.getInventory(Message.t("&9&lLoot Trade"));

		setName(inv.getItem(10), "&9&l" + player.getDisplayName() + "&9&l's Offer");
		setName(inv.getItem(16), "&9&l" + other.getDisplayName() + "&9&l's Offer");

		player.openInventory(inv);
	}

	@Override
	protected void closeInventory(Player player) {
		player.closeInventory();
	}

	@Override
	protected boolean onInventoryClick(InventoryClickEvent e) {
		if (e.getClickedInventory().equals(e.getView().getTopInventory()))
			return inventoryClick(e);
		return false;
	}

	@Override
	protected boolean inventoryClick(InventoryClickEvent e) {
		if (e.getSlot() % 9 > 2 || e.getSlot() / 9 < 2) {
			if (e.getSlot() % 9 == 4 && e.getSlot() / 9 > 1)
				if (e.getSlot() / 9 > 3)
					main.cancel((Player) e.getWhoClicked());
				else {
					((Object[]) getData(e.getWhoClicked()))[1] = (byte) 1;
					Player who = (Player) e.getWhoClicked();
					Trade trade = main.getTrade(who);

					if (trade.accept(who))
						main.end(trade);
					else {
						Inventory inv = trade.getOther(who).getOpenInventory().getTopInventory();

						setName(setDurability(e.getClickedInventory().getItem(3), 3), "&3&lWaiting for Partner");
						setName(setDurability(e.getClickedInventory().getItem(5), 3), "&3&lWaiting for Partner");
						setName(setDurability(e.getClickedInventory().getItem(13), 3), "&3&lWaiting for Partner");

						setName(setDurability(inv.getItem(3), 4), "&6&lPartner Accepted");
						setName(setDurability(inv.getItem(5), 4), "&6&lPartner Accepted");
						setName(setDurability(inv.getItem(13), 4), "&6&lPartner Accepted");
					}
				}
			return true;
		} else if ((byte) ((Object[]) getData(e.getWhoClicked()))[1] == 1)
			return true;

		Player other = (Player) ((Object[])getData(e.getWhoClicked()))[0];

		other.getOpenInventory().getTopInventory().setItem(e.getSlot() + 6, e.getCursor());

		return false;
	}

	@Override
	protected void inventoryClose(InventoryCloseEvent e) {
		Player a = (Player) e.getPlayer();
		Trade trade = main.getTrade(a);

		if (trade != null) {
			Player b = trade.getOther(a);
			main.remove(trade);

			main.restore(a, e.getView().getTopInventory());
			main.restore(b, b.getOpenInventory().getTopInventory());

			Message.send(Messages.ALERTS.TRADE.cancelY, a);
			Message.send(Messages.ALERTS.TRADE.cancelO, b);

			b.closeInventory();
		}
	}

	private void setName(ItemStack item, String name) {
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(Message.t(name));
		item.setItemMeta(im);
	}

	private ItemStack setDurability(ItemStack item, int durability) {
		item.setDurability((short) durability);
		return item;
	}
}

package me.juanco.lc.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.commons.lang3.math.NumberUtils;

public class IOUtils {

	Random random = new Random();

	public static long monthMs = 2592000000L;
	public static long dayMs = 86400000L;
	public static long hourMs = 3600000L;
	public static long minuteMs = 60000L;
	public static long secondMs = 1000L;

	public <T> T get(Iterable<T> iterable, Object value) {
		Iterator<T> iterator = iterable.iterator();
		T current;
		while (iterator.hasNext())
			if ((current = iterator.next()) != null && current.equals(value))
				return current;
		return null;
	}

	public <T> T get(Iterable<T> iterable, int index) {
		Iterator<T> iterator = iterable.iterator();

		int i = 0;
		while (i <= index)
			if (i == index)
				return iterator.next();
			else {
				iterator.next();
				i++;
			}

		return null;
	}

	public String capitalize(String original) {
		if (original.contains("_"))
			original = original.replaceAll("_", " ");

		String[] words = original.split(" ");

		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < words.length; i++)
			builder.append(' ').append(words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase());

		return builder.substring(1);
	}

	public String getPrice(int price) {
		StringBuilder builder = new StringBuilder('$');
		String sPrice = String.valueOf(price);

		if (sPrice.length() < 4)
			builder.append(sPrice);
		else {
			int head = sPrice.length() % 3;
			if (head == 0)
				head = 3;
			builder.append(sPrice, 0, head);

			int i = head;
			while (i < sPrice.length()) {
				builder.append(',').append(sPrice, i, i + 3);
				i += 3;
			}
		}

		return builder.toString();
	}

	public int indexOf(Iterable<?> iterable, Object value) {
		Iterator<?> iterator = iterable.iterator();
		int index = 0;
		while (iterator.hasNext())
			if (iterator.next().equals(value))
				return index;
			else
				index++;
		return -1;
	}

	public int indexOf(String iterable, char object) {
		int index = 0;
		for (char character : iterable.toCharArray())
			if (character == object)
				return index;
			else
				index++;
		return -1;
	}

	public <T> int indexOf(T[] array, Object object) {
		int index = 0;
		while (index < array.length)
			if (array[index] != null && array[index].equals(object))
				return index;
			else
				index++;
		return -1;
	}

	public <T> int defaultIndexOf(T[] array, Object object) {
		int index = 0;
		while (index < array.length)
			if (array[index] != null && object.equals(array[index]))
				return index;
			else
				index++;
		return -1;
	}

	public int total(int[] array) {
		int count = 0;
		for (int i : array)
			count += i;
		return count;
	}

	public int ultraRandom(int range) {
		return ultraRandom(range, 10);
	}

	public int ultraRandom(int range, int randomness) {
		if (range == 0)
			return 0;
		return (int) ((System.currentTimeMillis() / randomness * random.nextInt(randomness) + 1) % range);
	}

	public List<Integer> randomList(int size) {
		List<Integer> array = new ArrayList<Integer>();

		while (array.size() < size)
			array.add((int) (System.currentTimeMillis() / 10 * random.nextInt(10) + 1) % 1000);

		return array;
	}

	public <K, V> String serializeMap(Map<K, V> map) {
		Iterator<Entry<K, V>> localIterator = map.entrySet().iterator();

		if (!localIterator.hasNext())
			return "";

		StringBuilder builder = new StringBuilder();

		for (;;) {
			Entry<K, V> localEntry = localIterator.next();

			Object key = localEntry.getKey();
			Object value = localEntry.getValue();

			builder.append(key).append(':').append(value);

			if (!localIterator.hasNext())
				return builder.toString();

			builder.append(',').append(' ');
		}
	}

	public Map<String, Object> deserializeMap(String serial) {
		Map<String, Object> map = new HashMap<String, Object>();

		for (String entry : serial.split(", ")) {
			String[] data = entry.split(":");
			String value = data[1];

			if (NumberUtils.isNumber(value))
				try {
					map.put(data[0], NumberFormat.getInstance().parse(data[1]));
					continue;
				} catch (Exception ex) {
				}

			map.put(data[0], data[1]);
		}

		return map;
	}

	public String format(long value) {
		if (value < 1000)
			return String.valueOf(value);
		else {
			char key;
			long div;

			if (value < 1000000) {
				key = 'K';
				div = 1000;
			} else if (value < 1000000000) {
				key = 'M';
				div = 1000000;
			} else if (value < 1000000000000L) {
				key = 'B';
				div = 1000000000;
			} else if (value < 1000000000000000L) {
				key = 'T';
				div = 1000000000000L;
			} else if (value < 1000000000000000000L) {
				key = 'Q';
				div = 1000000000000000L;
			} else {
				key = 'P';
				div = 1000000000000000000L;
			}

			double result = new BigDecimal(value).divide(new BigDecimal(div)).doubleValue();

			DecimalFormat format = new DecimalFormat("#0.##");

			StringBuilder sb = new StringBuilder(format.format(result));
			sb.append(key);

			return sb.toString();
		}
	}

	public String hardReplace(String original, String replace, String value) {
		int size1 = original.length();
		int size2 = replace.length();

		if (size1 < size2)
			return original;
		if (size1 == size2)
			if (original.equals(replace))
				return value;
			else
				return original;

		StringBuilder result = new StringBuilder();

		for (int start = 0, end = size2; end <= size1; start++, end++)
			if (original.substring(start, end).equals(replace)) {
				result.append(value);
				start += size2 - 1;
				end += size2 - 1;
			} else
				result.append(original.charAt(start));

		return result.toString();
	}

	public boolean isInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}

package me.juanco.lc.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.juanco.lc.chat.Message;

public class ItemUtils {

	ItemStack separator = get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), "&a");
	ItemStack next = get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5), "&2&lNext Page");
	ItemStack previous = get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), "&4&lPrevious Page");

	public ItemStack getSeparator() {
		return separator;
	}

	public ItemStack getNext() {
		return next;
	}

	public ItemStack getPrevious() {
		return previous;
	}

	public ItemStack get(Material mat, String display) {
		return create(new ItemStack(mat), display, null);
	}

	public ItemStack get(Material mat, String display, String... lore) {
		return create(new ItemStack(mat), display, Arrays.asList(lore));
	}

	public ItemStack get(Material mat, String display, List<String> lore) {
		return create(new ItemStack(mat), display, lore);
	}

	public ItemStack get(ItemStack item, String display) {
		return create(item, display, null);
	}

	public ItemStack get(ItemStack item, List<String> lore) {
		return create(item, null, lore);
	}

	public ItemStack get(ItemStack item, String display, String... lore) {
		return create(item, display, Arrays.asList(lore));
	}

	public ItemStack get(ItemStack item, String display, List<String> lore) {
		return create(item, display, lore);
	}

	public void setLore(ItemStack item, String... lore) {
		ItemMeta meta = item.getItemMeta();
		List<String> loreList = meta.getLore();
		if (loreList == null)
			loreList = new ArrayList<String>();

		for (String l : lore)
			loreList.add(Message.t(l));

		meta.setLore(loreList);

		item.setItemMeta(meta);
	}

	ItemStack create(ItemStack item, String display, List<String> lore) {
		ItemMeta im = item.getItemMeta();

		im.setDisplayName(Message.t(display));
		if (lore != null && !lore.isEmpty()) {
			List<String> translated = new ArrayList<String>();
			for (String l : lore)
				translated.add(Message.t(l));
			im.setLore(translated);
		}

		item.setItemMeta(im);

		return item;
	}

	public int giveItems(Player target, ItemStack... items) {
		if (items == null)
			return -1;

		Map<Integer, ItemStack> left = target.getInventory().addItem(items);
		if (left.isEmpty())
			return -1;
		else {
			Location loc = target.getLocation();
			int amount = 0;

			for (ItemStack item : left.values()) {
				amount += item.getAmount();
				loc.getWorld().dropItemNaturally(loc, item);
			}

			return amount;
		}
	}

	public boolean validate(ItemStack item) {
		return item != null && !item.getType().equals(Material.AIR) && item.getAmount() > 0;
	}
}

package me.juanco.lc.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import me.juanco.lc.core.Main;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityPlayer;

public class Utils {

	public boolean isRightClick(Action action) {
		return action.equals(Action.RIGHT_CLICK_BLOCK) || action.equals(Action.RIGHT_CLICK_AIR);
	}

	public boolean isLeftClick(Action action) {
		return action.equals(Action.LEFT_CLICK_BLOCK) || action.equals(Action.LEFT_CLICK_AIR);
	}

	public boolean isBlockClick(Action action) {
		return action.equals(Action.RIGHT_CLICK_BLOCK) || action.equals(Action.LEFT_CLICK_BLOCK);
	}

	public boolean isAirClick(Action action) {
		return action.equals(Action.LEFT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_AIR);
	}

	public boolean checkNull(ItemStack item) {
		return !(item == null || item.getType().equals(Material.AIR));
	}

	public void saveLoc(Location loc, ConfigurationSection section, String path) {
		section.set(path, Main.ioUtils.serializeMap(loc.serialize()));
	}

	public void saveLoc(Location loc, FileConfiguration config, String path) {
		config.set(path, Main.ioUtils.serializeMap(loc.serialize()));
	}

	public Location loadLoc(String serial) {
		return Location.deserialize(Main.ioUtils.deserializeMap(serial));
	}

	@SuppressWarnings("unchecked")
	public <T> T getEntityOnSight(Player player, double range, String className, double height, boolean precise) {
		EntityPlayer ep = ((CraftPlayer) player).getHandle();

		List<Entity> possibilities = ep.world.getEntities(ep, ep.getBoundingBox().grow(range, range, range));
		List<Entity> toRemove = new ArrayList<Entity>();

		for (Entity entity : possibilities)
			if (!entity.getClass().getSimpleName().equals(className))
				toRemove.add(entity);

		if (!toRemove.isEmpty())
			for (Entity entity : toRemove)
				possibilities.remove(entity);

		toRemove.clear();

		if (possibilities.isEmpty())
			return null;

		Location loc = player.getEyeLocation().clone();
		Vector direction = loc.getDirection();

		double xDif = direction.getX() / 4;
		double yDif = direction.getY() / 4;
		double zDif = direction.getZ() / 4;

		for (double amount = 0; amount <= range; amount += 0.25) {
			loc.add(xDif, yDif, zDif);

			for (Entity entity : possibilities)
				if (equals(entity.locX, entity.locY + height, entity.locZ, loc, precise))
					return (T) entity;
				else if (!loc.getBlock().getType().equals(Material.AIR))
					return null;
		}
		return null;
	}

	public boolean equals(double x, double y, double z, Location loc, boolean precise) {
		Location loc1 = new Location(loc.getWorld(), x, y, z);

		if (!precise) {
			boolean xBool = Math.abs(loc.getX() - loc1.getX()) < 1.2;
			boolean yBool = Math.abs(loc.getY() - loc1.getY()) < 0.3;
			boolean zBool = Math.abs(loc.getZ() - loc1.getZ()) < 1.2;

			return xBool && yBool && zBool;
		} else
			return loc.getBlockX() == loc1.getBlockX() && loc.getBlockY() == loc1.getBlockY()
					&& loc.getBlockZ() == loc1.getBlockZ() && loc.distance(loc1) < 0.5;
	}
}

package me.juanco.lc.packets;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import io.netty.channel.Channel;

public class PacketInjector {

	public void addPlayer(Player player) {
		try {
			Channel ch = getChannel(player);
			if (ch.pipeline().get("PacketInjector") != null)
				ch.pipeline().remove("PacketInjector");
			ch.pipeline().addBefore("packet_handler", "PacketInjector", new PacketHandler(player));
		} catch (Exception ex) { ex.printStackTrace(); }
	}

	public void removePlayer(Player player) {
		try {
			Channel ch = getChannel(player);
			if (ch.pipeline().get("PacketInjector") != null)
				ch.pipeline().remove("PacketInjector");
		} catch (Exception ex) { ex.printStackTrace(); }
	}

	private Channel getChannel(Player player) {
		return ( (CraftPlayer) player).getHandle().playerConnection.networkManager.channel;
	}
}

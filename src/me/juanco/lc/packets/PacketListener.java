package me.juanco.lc.packets;

import org.bukkit.entity.Player;

public abstract class PacketListener {

	protected final String packet;
	
	public PacketListener(String packetName) {
		packet = packetName;
	}

	public abstract boolean listen(Object message, Player target);

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return packet.equals(obj);
		return super.equals(obj);
	}
}

package me.juanco.lc.packets;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

public class PacketHandler extends ChannelDuplexHandler {
	
	private Player player;
	private static List<PacketListener> in = new ArrayList<PacketListener>();
	private static List<PacketListener> out = new ArrayList<PacketListener>();
	
	public PacketHandler(final Player player) {
		this.player = player;
	}
	
	@Override
	public void write(ChannelHandlerContext context, Object message, ChannelPromise promise) throws Exception {
		String className = message.getClass().getSimpleName();
		
		boolean cancelled = false;
		
		for (PacketListener listener : out)
			if (listener.equals(className)) {
				boolean cancel = listener.listen(message, player);
				if (!cancelled)
					cancelled = cancel;
			}
		
		if (cancelled)
			return;
		
		super.write(context, message, promise);
	}
	
	// private static final List<String> blackList =
	// Lists.newArrayList("PacketPlayInFlying", "PacketPlayInKeepAlive",
	// "PacketPlayInLook", "PacketPlayInPositionLook", "PacketPlayInPosition",
	// "PacketPlayOutScoreboardTeam",
	// "PacketPlayOutMapChunk", "PacketPlayOutRelEntityMove",
	// "PacketPlayOutPlayerListHeaderFooter",
	// "PacketPlayOutMapChunkBulk", " PacketPlayOutEntityTeleport",
	// "PacketPlayOutUpdateTime",
	// "PacketPlayOutKeepAlive", "PacketPlayOutEntityHeadRotation",
	// "PacketPlayOutRelEntityMoveLook",
	// "PacketPlayOutEntityEquipment", "PacketPlayOutChat",
	// "PacketPlayOutTitle", "PacketPlayOutUpdateSign",
	// "PacketPlayOutScoreboardObjective", "PacketPlayOutUpdateHealth",
	// "PacketPlayOutExperience",
	// "PacketPlayOutPosition", "PacketPlayOutEntityMetadata",
	// "PacketPlayOutEntityTeleport",
	// "PacketPlayOutNamedSoundEffect", "PacketPlayOutUpdateAttributes",
	// "PacketPlayOutPlayerInfo",
	// "PacketPlayOutWorldBorder", "PacketPlayOutScoreboardScore",
	// "PacketPlayOutTileEntityData",
	// "PacketPlayOutNamedEntitySpawn", "PacketPlayOutSpawnEntityLiving",
	// "PacketPlayOutScoreboardDisplayObjective", "PacketPlayInSettings",
	// "PacketPlayOutEntityVelocity",
	// "PacketPlayOutEntityLook");

	@Override
	public void channelRead(ChannelHandlerContext context, Object message) throws Exception {
		String className = message.getClass().getSimpleName();

		// if (!blackList.contains(className))
		// System.out.println(className);

		boolean cancelled = false;
		
		for (PacketListener listener : in)
			if (listener.equals(className)) {
				boolean cancel = listener.listen(message, player);
				if (!cancelled)
					cancelled = cancel;
			}
		
		if (cancelled)
			return;

		super.channelRead(context, message);
	}
	
	
	public static void register(PacketListener listener) {
		if (listener.packet.startsWith("PacketPlayIn"))
			in.add(listener);
		else out.add(listener);
	}
}

package me.juanco.lc.crates;

import org.bukkit.inventory.ItemStack;

import me.juanco.lc.core.Main;
import me.juanco.lc.crates.objects.IStack;

public class CrateUtils {

	ItemStack[] crates;
	ItemStack[] panes;

	public CrateUtils() {
		LootType[] types = LootType.values();
		crates = new ItemStack[types.length - 1];
		panes = new ItemStack[types.length];

		for (int i = 0; i < types.length; i++) {
			if (i != types.length - 1)
				crates[i] = types[i].crate;
			panes[i] = types[i].pane;
		}
	}

	public boolean isCrate(ItemStack item) {
		return Main.ioUtils.defaultIndexOf(crates, new IStack(item)) > -1;
	}

	public boolean isPane(ItemStack item) {
		return Main.ioUtils.defaultIndexOf(panes, new IStack(item)) > -1;
	}

	public CrateType fromItem(ItemStack item) {
		return CrateType.values()[Main.ioUtils.defaultIndexOf(crates, new IStack(item))];
	}

	public LootType getLoot(ItemStack item) {
		return LootType.values()[Main.ioUtils.defaultIndexOf(panes, new IStack(item))];
	}
}

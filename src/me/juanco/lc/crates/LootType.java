package me.juanco.lc.crates;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.core.Main;
import me.juanco.lc.crates.managers.GuiManager;
import me.juanco.lc.files.Files;

public enum LootType {

	common("&7&lCommon ", (short) 7), uncommon("&2&lUncommon ", (short) 5), rare("&1&lRare ",
			(short) 11), epic("&5&lEpic ", (short) 10), legendary("&6&lLegendary ", (short) 1), ready(null, (short) 0);
	
	public final ItemStack crate;
	public final ItemStack pane;
	public final List<String> rewards;
	public final List<String> specialRewards;
	public final double specialChance;
	public final String prefix;
	public LootType upper = null;

	private LootType(String prefix, short color) {
		this.prefix = prefix;
		if (ordinal() == 5) {
			crate = null;
			pane = null;
			rewards = null;
			specialRewards = null;
			specialChance = 0;
		} else if (GuiManager.loots[ordinal()] == null) {
			crate = Main.itemUtils.get(Material.CHEST, new StringBuilder(prefix).append("Crate").toString());
			pane = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, color),
					new StringBuilder(prefix).append("Loot").toString(), "&8&lRight&7-&8&lClick &7&lTo Claim");
			rewards = Files.crate.file.getStringList("Rewards." + toString() + ".normal");
			specialRewards = Files.crate.file.getStringList("Rewards." + toString() + ".special");
			specialChance = Files.crate.file.getDouble("Rewards." + toString() + ".specialChance");
			GuiManager.loots[ordinal()] = this;
		} else {
			LootType loot = GuiManager.loots[ordinal()];
			crate = loot.crate;
			pane = loot.pane;
			rewards = loot.rewards;
			specialRewards = loot.specialRewards;
			specialChance = loot.specialChance;
		}
	}

	public CrateType getCrate() {
		return CrateType.valueOf(name());
	}
}

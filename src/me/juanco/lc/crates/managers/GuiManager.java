package me.juanco.lc.crates.managers;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;
import me.juanco.lc.crates.CrateType;
import me.juanco.lc.crates.LootType;
import me.juanco.lc.files.Files;

public class GuiManager {

	public static CrateType[] crates = new CrateType[5];
	public static LootType[] loots = new LootType[5];

	public static final ItemStack Unknown = Main.itemUtils.get(new ItemStack(Material.CHEST),
			"&4-&a?&b?&c?&d?&e?&a?&b?&c?&d?&e&4-", "&3&lMystery Box");

	public static final String PreC_Title = Message.t("&8&lCommon Crate");
	public static final ItemStack PreC_Default = Main.itemUtils
			.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), "&e&lChoose Currency");
	public static final ItemStack PreC_Or = Main.itemUtils.get(Material.STORAGE_MINECART, "&f&lOr");
	public static ItemStack PreC_Normal;
	public static ItemStack PreC_Token;

	public GuiManager() {
		PreC_Normal = Main.itemUtils.get(new ItemStack(Material.EXPLOSIVE_MINECART),
				new StringBuilder("&a&l$").append((int) Files.crate.money).toString(), "&8&lCommon Crate");
		PreC_Token = Main.itemUtils.get(new ItemStack(Material.POWERED_MINECART),
				new StringBuilder("&c&l").append((int) Files.crate.tokens).append(" PvP tokens").toString(),
				"&8&lCommon Crate");
	}

	private List<int[]> chances = null;

	// public void reload() { chances = null; }

	public LootType randomCrate(CrateType type) {

		int[] chances;
		if (this.chances == null || this.chances.size() <= type.ordinal() || this.chances.get(type.ordinal()) == null) {
			double[] possibilities = loadPossibilities(type);
			chances = new int[possibilities.length];

			for (int index = 0; index < chances.length; index++)
				chances[index] = (int) (possibilities[index] * 5000);

			int total = Main.ioUtils.total(chances);
			if (total < 500000)
				chances[chances.length - 1] += 500000 - total;

			if (this.chances == null) {
				this.chances = Lists.newArrayList();
				for (int i = 0; i < CrateType.values().length; i++)
					this.chances.add(null);
			}
			this.chances.add(type.ordinal(), chances);
		} else
			chances = this.chances.get(type.ordinal());

		int value = Main.ioUtils.ultraRandom(500000, (int) (System.currentTimeMillis() % 450 + 50));
		int current = 0, index = 0;
		LootType[] loots = LootType.values();
		int cap = 5 - type.ordinal();
		while (index < cap) {
			current += chances[index];
			if (index == cap - 1 || current >= value)
				return loots[loots.length - 2 - index];
			index++;
		}

		return LootType.valueOf(type.toString());
	}

	private double[] loadPossibilities(CrateType type) {
		double[] possibilities = new double[5 - type.ordinal()];
		CrateType[] values = CrateType.values();
		for (int index = 0; index < possibilities.length; index++)
			possibilities[index] = Files.crate.file
					.getDouble("Crates." + type.name() + "." + values[values.length - index - 1]);
		return possibilities;
	}
}

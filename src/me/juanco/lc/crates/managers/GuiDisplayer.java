package me.juanco.lc.crates.managers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.crates.CrateType;
import me.juanco.lc.crates.Crates;
import me.juanco.lc.crates.LootType;
import me.juanco.lc.crates.objects.Displayed;

public class GuiDisplayer {

	Crates crates;

	public GuiDisplayer(Crates crates) {
		this.crates = crates;
	}

	public static List<Displayed> displayed = new ArrayList<Displayed>();

	public void display(Player p, ItemStack item) {
		display(p, crates.getUtils().fromItem(item));
	}

	public void display(Player p, CrateType type) {
		Inventory gui = Bukkit.createInventory(null, 54, type.title);
		List<LootType> loots = new ArrayList<LootType>();

		for (int index = 0; index < gui.getSize(); index++) {
			LootType loot = crates.getManager().randomCrate(type);
			loots.add(loot);
			gui.setItem(index, GuiManager.Unknown);
		}
		p.openInventory(gui);
		displayed.add(new Displayed(p, loots, type));
	}

	public void preCommon(Player p) {
		Inventory gui = Bukkit.createInventory(null, 27, GuiManager.PreC_Title);

		for (int index = 0; index < gui.getSize(); index++)
			gui.setItem(index, GuiManager.PreC_Default);

		gui.setItem(12, GuiManager.PreC_Normal);
		gui.setItem(13, GuiManager.PreC_Or);
		gui.setItem(14, GuiManager.PreC_Token);

		p.openInventory(gui);
	}
}

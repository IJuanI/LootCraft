package me.juanco.lc.crates;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;
import me.juanco.lc.crates.managers.GuiManager;

public enum CrateType {

	common("&7&lCommon "), uncommon("&2&lUncommon "), rare("&1&lRare "), epic("&5&lEpic "), legendary("&6&lLegendary ");
	
	public final String title;
	public final ItemStack crate;
	public final String prefix;
	
	private CrateType(String prefix) {
		this.prefix = prefix;
		if (GuiManager.crates[ordinal()] == null) {
			title = Message.t(prefix + "Loot");
			crate = Main.itemUtils.get(Material.CHEST, new StringBuilder(prefix).append("Crate").toString());
			GuiManager.crates[ordinal()] = this;
		} else {
			crate = GuiManager.crates[ordinal()].crate;
			title = GuiManager.crates[ordinal()].title;
		}
	}

	public LootType getLoot() {
		return LootType.valueOf(name());
	}
}

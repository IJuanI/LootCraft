package me.juanco.lc.crates.listeners;

import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.block.Chest;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.crates.LootType;
import me.juanco.lc.crates.objects.ClickableCrate;
import me.juanco.lc.files.Files;

public class CratePlayerInteractListener extends LootListener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (getMain().getUtils().isRightClick(e.getAction()) && getMain().getUtils().checkNull(e.getItem())
				&& e.getMaterial().isSolid()) {
			if (getMain().getCrates().getUtils().isCrate(e.getItem()))
				crateInteract(e);
			else if (getMain().getCrates().getUtils().isPane(e.getItem()))
				paneInteract(e);
		} else if (getMain().getCrates().isAdmin(e.getPlayer())) {
			if (e.getClickedBlock() == null || !(e.getClickedBlock().getState() instanceof Chest)) {
				getMain().getCrates().leave(e.getPlayer());
				Message.send(Messages.ALERTS.CCError, e.getPlayer());
			} else if (!getMain().getCrates().isClickable(e.getClickedBlock())) {
				getMain().getCrates().leave(e.getPlayer());
				getMain().getCrates().addClickable(new ClickableCrate(e.getClickedBlock()));

				ConfigurationSection sec = Files.crate.file.getConfigurationSection("Chests");

				if (sec == null)
					sec = Files.crate.file.createSection("Chests");

				Set<String> keys = sec.getKeys(false);

				getMain().getUtils().saveLoc(e.getClickedBlock().getLocation(), sec,
						String.valueOf(keys == null ? 0 : keys.size()));
				Files.crate.save();

				Message.send(Messages.ALERTS.CCSuccess, e.getPlayer());
				e.setCancelled(true);
			}
		} else if (e.getClickedBlock() != null && getMain().getCrates().isClickable(e.getClickedBlock())) {
			getMain().getCommand("crate").execute(e.getPlayer(), "crate", new String[]{"common"});
			e.setCancelled(true);
		}
	}

	void crateInteract(PlayerInteractEvent e) {
		e.setCancelled(true);
		
		ItemStack item = e.getItem();
		if (item.getAmount() > 1) {
			item = item.clone();
			e.getItem().setAmount(item.getAmount() -1);
		} else
			e.getPlayer().getInventory().remove(item);
		
		getMain().getCrates().getDisplayer().display(e.getPlayer(), item);
	}

	void paneInteract(PlayerInteractEvent e) {
		e.setCancelled(true);
		Player p = e.getPlayer();

		if (e.getItem().getAmount() > 1)
			e.getItem().setAmount(e.getItem().getAmount() - 1);
		else
			p.getInventory().remove(e.getItem());

		boolean op = p.isOp();

		LootType loot = getMain().getCrates().getUtils().getLoot(e.getItem());
		List<String> cmds;

		if (Main.ioUtils.randomList((int) (loot.specialChance * 500)).contains(System.currentTimeMillis() % 50000))
			cmds = loot.specialRewards;
		else
			cmds = loot.rewards;

		try {
			if (!op)
				p.setOp(true);
			Bukkit.dispatchCommand(p, cmds.get(Main.ioUtils.ultraRandom(cmds.size())));
		} finally {
			if (!op)
				p.setOp(false);
		}

	}
}

package me.juanco.lc.crates.listeners;

import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;
import me.juanco.lc.core.Main;
import me.juanco.lc.crates.CrateType;
import me.juanco.lc.crates.LootType;
import me.juanco.lc.crates.managers.GuiDisplayer;
import me.juanco.lc.crates.managers.GuiManager;
import me.juanco.lc.crates.tasks.CrateAnimationTask;
import me.juanco.lc.files.Files;

public class CrateInventoryListener extends LootListener {

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		int index = -1;
		if (e.getClickedInventory() != null
				&& e.getClickedInventory().equals(e.getWhoClicked().getOpenInventory().getTopInventory()))
			if ((index = Main.ioUtils.indexOf(GuiDisplayer.displayed, e.getWhoClicked())) > -1) {
				Player p = (Player) e.getWhoClicked();
				List<LootType> loots = GuiDisplayer.displayed.get(index).loots;
				if (loots.get(loots.size() - 1).equals(LootType.ready))
					p.closeInventory();
				else {
					LootType loot = loots.get(e.getSlot());
					LootType ready = LootType.ready;
					ready.upper = loot;
					loots.add(ready);
					e.getClickedInventory().setItem(e.getSlot(), loot.crate);
					CrateAnimationTask.start(p, e.getSlot(), e.getClickedInventory().getSize());

					broadcast(loot, p, GuiDisplayer.displayed.get(index).type);
				}

				p.setItemOnCursor(null);
				e.setCancelled(true);
			} else if (e.getClickedInventory().getTitle().equals(GuiManager.PreC_Title)) {
				e.setCancelled(true);
				int slot = e.getSlot();
				if (slot == 12 || slot == 14) {
					Player p = (Player) e.getWhoClicked();
					p.closeInventory();
					boolean paid = true;
					long cost = slot == 12 ? Files.crate.money : Files.crate.tokens;

					if (slot == 12)
						if (Main.money.hasEnough(p, cost))
							paid = Main.money.withdraw(p, cost);
						else paid = false;
					else if (Main.tokens.hasEnough(p, (int) cost))
						paid = Main.tokens.withdraw(p, (int) cost);
					else paid = false;

					if (paid) {
						HashMap<Integer, ItemStack> left = p.getInventory().addItem(CrateType.common.crate);
						if (!left.isEmpty())
							for (ItemStack item : left.values()) 
								p.getWorld().dropItemNaturally(p.getLocation(), item);
					} else
						new Message(e.getWhoClicked()).msg(Messages.ERRORS.afford);
				}
			}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		int index = Main.ioUtils.indexOf(GuiDisplayer.displayed, e.getPlayer());
		if (index > -1) {
			Player p = (Player) e.getPlayer();
			List<LootType> loots = GuiDisplayer.displayed.get(index).loots;

			CrateAnimationTask.end(p);
			LootType type;

			HashMap<Integer, ItemStack> left;
			if (loots.get(loots.size() - 1).equals(LootType.ready))
				left = p.getInventory().addItem((type = loots.get(loots.size() - 1)).upper.pane);
			else
				left = p.getInventory()
						.addItem((type = loots.get((int) System.currentTimeMillis() % loots.size())).pane);

			if (!left.isEmpty())
				for (ItemStack item : left.values())
					p.getWorld().dropItemNaturally(p.getLocation(), item);

			if (!type.equals(LootType.ready))
				broadcast(type, p, GuiDisplayer.displayed.get(index).type);

			GuiDisplayer.displayed.remove(index);
		}
	}

	private void broadcast(LootType lType, Player player, CrateType cType) {
		StringBuilder builder = new StringBuilder("%alert ").append(player.getName()).append(" received a");
		if (lType.ordinal() == 3)
			builder.append('n');
		builder.append(' ').append(lType.prefix).append("&b&lfrom a");
		if (cType.ordinal() == 3 || cType.ordinal() == 1)
			builder.append('n');
		builder.append(' ').append(cType.prefix).append("&b&lcrate");

		Message.broadcast(builder.toString());
	}
}

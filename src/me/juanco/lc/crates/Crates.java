package me.juanco.lc.crates;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import me.juanco.lc.core.Main;
import me.juanco.lc.crates.flying.FlyingCrate;
import me.juanco.lc.crates.managers.GuiDisplayer;
import me.juanco.lc.crates.managers.GuiManager;
import me.juanco.lc.crates.objects.ClickableCrate;
import me.juanco.lc.crates.objects.CrateAdmin;
import me.juanco.lc.files.Files;

public class Crates {

	private final CrateUtils utils = new CrateUtils();
	private final GuiDisplayer displayer;
	private final GuiManager manager = new GuiManager();

	private final List<CrateAdmin> admins = new ArrayList<CrateAdmin>();
	private final List<ClickableCrate> crates = new ArrayList<ClickableCrate>();

	public Crates(Main main) {
		displayer = new GuiDisplayer(this);

		new FlyingCrate(main);

		load(main);
	}

	public void leave(Player player) {
		admins.remove(Main.ioUtils.get(admins, player));
	}

	public boolean isClickable(Block block) {
		return Main.ioUtils.indexOf(crates, block) > -1;
	}

	public void addClickable(ClickableCrate crate) {
		crates.add(crate);
	}

	public void addAdmin(CrateAdmin admin) {
		admins.add(admin);
	}

	public boolean isAdmin(Player player) {
		return Main.ioUtils.indexOf(admins, player) > -1;
	}

	private void load(Main main) {
		ConfigurationSection sec = Files.crate.file.getConfigurationSection("Chests");
		if (sec != null)
		for (String id : sec.getKeys(false)) {
			Location loc = main.getUtils().loadLoc(sec.getString(id));
			crates.add(new ClickableCrate(loc.getBlock()));
		}
	}

	public CrateUtils getUtils() {
		return utils;
	}

	public GuiDisplayer getDisplayer() {
		return displayer;
	}

	public GuiManager getManager() {
		return manager;
	}

}

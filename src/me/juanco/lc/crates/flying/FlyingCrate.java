package me.juanco.lc.crates.flying;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.files.LootFile;

public class FlyingCrate {

	private final static Vector pos1 = new Vector(317, 47, -1271);
	private final static Vector pos2 = new Vector(329, 54, -1251);

	private final FlyingCrateLoader loader;
	private final FlyingCrateSaver saver;

	private final LootFile target;

	private List<FlyingBlock> blocks = new ArrayList<FlyingBlock>();

	public FlyingCrate(Main core) {
		loader = new FlyingCrateLoader();
		saver = new FlyingCrateSaver();

		target = Files.storage;

		loadBlocks();

		FlyingAppearanceTask.checkDates(this);
	}

	private void loadBlocks() {
		if (!target.file.isList("LootDrop"))
			saveRegion();
		else
			blocks = loader.load(target.file.getByteList("LootDrop").toArray(new Byte[0]));
		hide();
	}

	@SuppressWarnings("deprecation")
	private void saveRegion() {
		World world = Bukkit.getWorld("world");
		Location loc = new Location(world, pos1.getX(), pos1.getY(), pos1.getZ());

		while (loc.getY() <= pos2.getY()) {
			while (loc.getX() <= pos2.getX()) {
				while (loc.getZ() <= pos2.getZ()) {
					Block block = loc.getBlock();
					blocks.add(new FlyingBlock(block.getType(), block.getData()));
					loc.setZ(loc.getZ() + 1);
				}
				loc.setZ(pos1.getZ());
				loc.setX(loc.getX() + 1);
			}
			loc.setX(pos1.getX());
			loc.setY(loc.getY() + 1);
		}

		byte[] bit = saver.save(blocks);
		List<Byte> bits = new ArrayList<Byte>();

		for (byte b : bit)
			bits.add(b);

		target.file.set("LootDrop", bits);
		target.save();
	}

	public void hide() {
		World world = Bukkit.getWorld("world");
		Location loc = new Location(world, pos1.getX(), pos1.getY(), pos1.getZ());

		while (loc.getY() <= pos2.getY()) {
			while (loc.getX() <= pos2.getX()) {
				while (loc.getZ() <= pos2.getZ()) {
					loc.getBlock().setType(Material.AIR);
					loc.setZ(loc.getZ() + 1);
				}
				loc.setZ(pos1.getZ());
				loc.setX(loc.getX() + 1);
			}
			loc.setX(pos1.getX());
			loc.setY(loc.getY() + 1);
		}
	}

	@SuppressWarnings("deprecation")
	public void show() {
		World world = Bukkit.getWorld("world");
		Location loc = new Location(world, pos1.getX(), pos1.getY(), pos1.getZ());
		int index = 0;

		while (loc.getY() <= pos2.getY()) {
			while (loc.getX() <= pos2.getX()) {
				while (loc.getZ() <= pos2.getZ()) {
					FlyingBlock block = blocks.get(index);

					loc.getBlock().setType(block.getMaterial());
					loc.getBlock().setData(block.getData());

					index++;

					loc.setZ(loc.getZ() + 1);
				}
				loc.setZ(pos1.getZ());
				loc.setX(loc.getX() + 1);
			}
			loc.setX(pos1.getX());
			loc.setY(loc.getY() + 1);
		}
	}
}

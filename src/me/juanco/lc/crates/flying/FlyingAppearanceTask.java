package me.juanco.lc.crates.flying;

import java.util.Date;
import java.util.Random;

import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.clock.LootTask;

public class FlyingAppearanceTask extends LootTask {

	private static final Random rand = new Random();

	private static final int[] possibilities1 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
			17, 18,
			19, 20, 20, 20, 20, 21, 21, 21, 21, 22, 22, 22, 22, 23, 23, 23, 23, 24, 24, 24, 25, 25, 25, 26, 26, 26, 27,
			27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33, 33, 34, 34, 34, 35, 35, 35, 36, 36, 36, 37, 37, 37, 38,
			38, 38, 39, 39, 39, 39, 40, 40, 40, 40, 41, 41, 41, 41, 42, 42, 42, 42, 43, 43, 43, 43, 44, 44, 44, 44, 45,
			45, 45, 45, 46, 46, 46, 46, 47, 47, 47, 47 };
	private static final int[] possibilities2 = new int[] { 50, 50, 50, 51, 51, 51, 52, 52, 52, 53, 53, 53, 54, 54, 54,
			55, 55,
			55, 56, 56, 56, 57, 57, 57, 58, 58, 58, 59, 59, 59, 60, 60, 60, 61, 61, 61, 62, 62, 62, 63, 63, 63, 64, 64,
			64, 65, 65, 65, 66, 66, 66, 66, 67, 67, 67, 67, 68, 68, 68, 68, 69, 69, 69, 69, 70, 70, 70, 70, 71, 71, 71,
			71, 72, 72, 72, 72, 73, 73, 73, 73, 74, 74, 74, 74, 75, 75, 75, 75, 76, 76, 76, 76, 77, 77, 77, 77, 78, 78,
			78, 79, 79, 79, 80, 80, 81, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97 };

	private static Date firstUpdate = null;
	private static Date lastUpdate = null;

	private final FlyingCrate flying;
	private boolean show = true;

	public FlyingAppearanceTask(long delay, FlyingCrate flying) {
		super(delay, false);
		this.flying = flying;
	}

	private FlyingAppearanceTask(FlyingCrate flying) {
		super(900, false);
		this.flying = flying;
		show = false;
	}

	@Override
	public void run() {
		if (show) {
			flying.show();
			LootClock.addTask(new FlyingAppearanceTask(flying));
		} else {
			flying.hide();
			checkDates(flying);
		}
	}

	public static Date getFirst() {
		return firstUpdate;
	}

	public static Date getLast() {
		return lastUpdate;
	}

	public static void checkDates(FlyingCrate flying) {
		Date current = new Date();

		if (firstUpdate == null || !firstUpdate.after(current)) {
			firstUpdate = calcTime(current, possibilities1);
		}

		if (lastUpdate == null || !lastUpdate.after(current)) {
			lastUpdate = calcTime(current, possibilities2);
		}
	}

	@SuppressWarnings("deprecation")
	private static Date calcTime(Date current, int[] possibilities) {
		
		long relativeTime = possibilities[rand.nextInt(possibilities.length)] * 900;
		if (current.getHours() > 8 + current.getTimezoneOffset())
			relativeTime += 86400;

		Date date = (Date) current.clone();
		int offset = current.getTimezoneOffset();

		date.setHours(9 + offset);
		date.setMinutes(0);
		date.setSeconds(0);
		date.setTime(date.getTime() + relativeTime * 1000);
		
		return date;
	}

}

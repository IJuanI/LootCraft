package me.juanco.lc.crates.flying;

import org.bukkit.Material;

public class FlyingBlock {

	private final Material mat;
	private final byte data;

	public FlyingBlock(Material mat, int data) {
		this.mat = mat;
		this.data = (byte) data;
	}

	public Material getMaterial() {
		return mat;
	}

	public byte getData() {
		return data;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Material)
			return mat.equals(obj);
		return super.equals(obj);
	}
}

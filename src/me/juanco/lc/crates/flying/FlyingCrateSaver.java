package me.juanco.lc.crates.flying;

import java.util.List;

import org.bukkit.Material;

public class FlyingCrateSaver {

	private final String air = "1100";
	private final String smoothStairs = "1011";
	private final String brickStairs = "1010";
	private final String chest = "1001";
	private final String step = "1000";
	private final String acaciaFence = "0111";
	private final String darkOakFence = "0110";
	private final String redstoneBlock = "0101";
	private final String cobbleWall = "0100";
	private final String redstoneLamp = "0011";
	private final String smoothBrick = "0010";
	private final String brick = "0001";
	private final String bedrock = "0000";
	private final String nullData = "0000";

	protected FlyingCrateSaver() {
	}

	public byte[] save(List<FlyingBlock> blocks) {
		byte[] bits = new byte[blocks.size()];

		int i = 0;

		for (FlyingBlock block : blocks) {
			if (block.equals(Material.AIR))
				bits[i] = getByte(new StringBuilder(air).append(nullData));
			else {
				String data = nullData;
				String id;

				switch (block.getMaterial()) {
				case SMOOTH_STAIRS:
					id = smoothStairs;
					data = getByte(block.getData());
					break;
				case BRICK_STAIRS:
					id = brickStairs;
					data = getByte(block.getData());
					break;
				case CHEST:
					id = chest;
					data = getByte(block.getData());
					break;
				case STEP:
					id = step;
					data = getByte(block.getData());
					break;
				case ACACIA_FENCE:
					id = acaciaFence;
					break;
				case DARK_OAK_FENCE:
					id = darkOakFence;
					break;
				case REDSTONE_BLOCK:
					id = redstoneBlock;
					break;
				case COBBLE_WALL:
					id = cobbleWall;
					break;
				case REDSTONE_LAMP_ON:
					id = redstoneLamp;
					break;
				case SMOOTH_BRICK:
					id = smoothBrick;
					break;
				case BRICK:
					id = brick;
					break;
				case BEDROCK:
					id = bedrock;
					break;
				default:
					id = air;
					break;
				}

				bits[i] = (byte) Integer.parseInt(new StringBuilder(id).append(data).toString(), 2);
			}

			i++;
		}

		return bits;
	}

	private String getByte(short data) {
		byte bit = (byte) data;
		char[] bits = new char[4];

		if ((bit & 0x08) >> 3 == 1)
			bits[0] = '1';
		else
			bits[0] = '0';

		if ((bit & 0x04) >> 2 == 1)
			bits[1] = '1';
		else
			bits[1] = '0';

		if ((bit & 0x02) >> 1 == 1)
			bits[2] = '1';
		else
			bits[2] = '0';

		if ((bit & 0x01) == 1)
			bits[3] = '1';
		else
			bits[3] = '0';

		return new String(bits);
	}

	private byte getByte(StringBuilder builder) {
		return (byte) Integer.parseInt(builder.toString(), 2);
	}
}

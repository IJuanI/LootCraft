package me.juanco.lc.crates.flying;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

public class FlyingCrateLoader {

	protected FlyingCrateLoader() {
	}

	public List<FlyingBlock> load(Byte[] bits) {
		Material mat = null;
		boolean special = false;

		List<FlyingBlock> blocks = new ArrayList<FlyingBlock>();
		int i = 0;

		while (i < bits.length) {
			byte bit = bits[i];

			if ((bit & 0x80) >> 7 == 1)
				if ((bit & 0x40) >> 6 == 1)
					blocks.add(new FlyingBlock(Material.AIR, 0));
				else {
					if ((bit & 0x20) >> 5 == 1)
						if ((bit & 0x10) >> 4 == 1)
							mat = Material.SMOOTH_STAIRS;
						else mat = Material.BRICK_STAIRS;
					else if ((bit & 0x10) >> 5 == 1)
						mat = Material.CHEST;
						else mat = Material.STEP;
						
					special = true;
				}
			else if ((bit & 0x40) >> 6 == 1)
				if ((bit & 0x20) >> 5 == 1)
					if ((bit & 0x10) >> 4 == 1)
						blocks.add(new FlyingBlock(Material.ACACIA_FENCE, 0));
						else blocks.add(new FlyingBlock(Material.DARK_OAK_FENCE, 0));
				else if ((bit & 0x10) >> 4 == 1)
					blocks.add(new FlyingBlock(Material.REDSTONE_BLOCK, 0));
						else blocks.add(new FlyingBlock(Material.COBBLE_WALL, 0));
			else if ((bit & 0x20) >> 5 == 1)
				if ((bit & 0x10) >> 4 == 1)
					blocks.add(new FlyingBlock(Material.REDSTONE_LAMP_ON, 0));
						else blocks.add(new FlyingBlock(Material.SMOOTH_BRICK, 0));
			else if ((bit & 0x10) >> 4 == 1)
				blocks.add(new FlyingBlock(Material.BRICK, 0));
						else blocks.add(new FlyingBlock(Material.BEDROCK, 0));
			
			if (special) {
				StringBuilder parsedBit = new StringBuilder();
				parsedBit.append((bit & 0x08) >> 3);
				parsedBit.append((bit & 0x04) >> 2);
				parsedBit.append((bit & 0x02) >> 1);
				parsedBit.append(bit & 0x01);
				blocks.add(new FlyingBlock(mat, Integer.parseInt(parsedBit.toString(), 2)));

				mat = null;
				special = false;
			}

			i++;
		}

		return blocks;
	}
}

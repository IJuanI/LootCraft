package me.juanco.lc.crates.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.core.Main;
import me.juanco.lc.crates.objects.CrateAnimator;

public class CrateAnimationTask extends BukkitRunnable {

	private static Set<CrateAnimator> animations = new ConcurrentSet<CrateAnimator>();

	public CrateAnimationTask(Main core) {
		runTaskTimer(core, 20, 1);
	}

	@Override
	public void run() {
		for (CrateAnimator animator : animations)
			if (animator.flush())
				animations.remove(animator);
	}

	public static void start(Player p, int slot, int size) {
		List<Integer> left = new ArrayList<Integer>();

		for (int index = 0; index < size; index++)
			if (index != slot)
				left.add(index);

		animations.add(new CrateAnimator(p, left));
	}

	public static void end(Player p) {
		CrateAnimator anim = Main.ioUtils.get(animations, p);

		if (anim != null)
			animations.remove(anim);
	}

}

package me.juanco.lc.crates.objects;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import me.juanco.lc.core.Main;
import me.juanco.lc.crates.LootType;
import me.juanco.lc.crates.managers.GuiDisplayer;
import me.juanco.lc.utils.IOUtils;

public class CrateAnimator {

	private final Player player;
	private final List<Integer> items;

	public CrateAnimator(Player player, List<Integer> items) {
		this.player = player;
		this.items = items;
	}

	public boolean flush() {
		int index = Main.ioUtils.indexOf(GuiDisplayer.displayed, player);

		if (index > -1) {
			Inventory inv = player.getOpenInventory().getTopInventory();
			List<LootType> loots = GuiDisplayer.displayed.get(index).loots;

			IOUtils utils = Main.ioUtils;

			for (int i = 0; i < 3; i++)
				if (items.isEmpty())
					break;
				else {
					int rand = items.remove(utils.ultraRandom(items.size()));
					inv.setItem(rand, loots.get(rand).pane);
				}
		}

		if (items.isEmpty())
			return true;
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		return super.equals(obj);
	}
}

package me.juanco.lc.crates.objects;

import org.bukkit.entity.Player;

public class CrateAdmin {

	private final Player player;

	public CrateAdmin(Player player) {
		this.player = player;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		return super.equals(obj);
	}
}

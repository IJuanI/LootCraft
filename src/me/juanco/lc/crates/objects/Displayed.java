package me.juanco.lc.crates.objects;

import java.util.List;

import org.bukkit.entity.Player;

import me.juanco.lc.crates.CrateType;
import me.juanco.lc.crates.LootType;

public class Displayed {

	public final Player player;
	public List<LootType> loots;
	public final CrateType type;

	public Displayed(Player player, List<LootType> loots, CrateType type) {
		this.player = player;
		this.loots = loots;
		this.type = type;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		return super.equals(obj);
	}
}

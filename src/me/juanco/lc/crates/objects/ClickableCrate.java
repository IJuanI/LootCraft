package me.juanco.lc.crates.objects;

import org.bukkit.block.Block;

public class ClickableCrate {

	private final Block block;

	public ClickableCrate(Block block) {
		this.block = block;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Block)
			return block.equals(obj);
		return super.equals(obj);
	}
}

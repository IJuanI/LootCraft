package me.juanco.lc.crates.objects;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class IStack extends ItemStack {

	public IStack(Material type) {
		super(type);
	}

	public IStack(Material type, int amount) {
		super(type, amount);
	}

	public IStack(Material type, int amount, short damage) {
		super(type, amount, damage);
	}

	public IStack(ItemStack stack) throws IllegalArgumentException {
		super(stack.clone());
	}

	@Override
	public boolean equals(Object object) {
		if (object == null)
			return false;
		else if (!(object instanceof ItemStack))
			return false;
		else if (this == object || super.equals(object))
			return true;
		else
			return isSimilar((ItemStack) object);
	}
}

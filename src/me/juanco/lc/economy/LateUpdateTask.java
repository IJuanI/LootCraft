package me.juanco.lc.economy;

import org.bukkit.entity.Player;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class LateUpdateTask extends LootTask {

	private final static ConcurrentSet<Entry> entries = new ConcurrentSet<Entry>();

	public LateUpdateTask() {
		super(1, true);
		LootClock.addTask(this);
	}

	@Override
	public void run() {
		for (Entry entry : entries)
			if (entry.flush())
				entries.remove(entry);
	}

	public static void queue(String placeholderName, Player player, PlaceholderManager placeholders) {
		entries.add(new Entry(placeholders, placeholderName, player));
	}

	private static class Entry {

		private final PlaceholderManager placeholders;
		private final String name;
		private final Player player;

		private int time = 3;

		private Entry(PlaceholderManager placeholders, String placeholderName, Player player) {
			this.placeholders = placeholders;
			name = placeholderName;
			this.player = player;
		}

		private boolean flush() {
			time--;
			if (time < 1)
				placeholders.callPlaceholder(name, player);
			else
				return false;
			return true;
		}
	}
}

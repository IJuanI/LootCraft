package me.juanco.lc.economy;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.core.Main;

public class Economy {

	Currency currency;
	private final Main core;

	public Economy(Currency currency, Main core) {
		this.currency = currency;
		this.core = core;
	}

	public boolean hasEnough(OfflinePlayer player, long amount) {
		return getAmount(player) >= amount;
	}

	public long[] getAmounts(UUID uuid) {
		try {
			ResultSet rs = get(uuid);
			if (validate(rs) || validate(rs = insert(uuid)))
				try {
					return new long[] { rs.getLong("Money"), rs.getLong("Tokens") };
				} catch (Exception ex) {
				}
		} catch (SQLException ex) {
		}

		return new long[] { -1L, -1L };
	}

	public long getAmount(OfflinePlayer player) {
		try {
			ResultSet rs = get(player);
			if (validate(rs) || validate(rs = insert(player)))
				try {
					return rs.getLong(currency.name());
				} catch (Exception ex) {
					ex.printStackTrace();
					System.out.println(currency.name());

				}
		} catch(SQLException ex) {
		}
		
		return -1L;
	}

	public boolean withdraw(OfflinePlayer player, long amount) {
		long money = getAmount(player);
		if (money >= amount)
			return set(player, money - amount);
		return false;
	}

	public boolean add(OfflinePlayer player, long amount) {
		long money = getAmount(player);
		return set(player, money + amount);

	}

	public boolean set(OfflinePlayer player, long amount) {
		if (amount < 0)
			return false;

		if (player.isOnline()) {
			String currencyName;

			if (currency == Currency.Money)
				currencyName = "money";
			else
				currencyName = "tokens";

			UserFile.getFile(player.getUniqueId()).props.set(currencyName, Main.ioUtils.format(amount));
			core.getPlaceholders().callPlaceholder(currencyName, player.getPlayer());
		}
		return core.getDB().update("Economy", currency.name(), amount, "uuid", player.getUniqueId().toString());
	}

	ResultSet get(OfflinePlayer player) {
		if (player == null)
			return null;
		return get(player.getUniqueId());
	}

	ResultSet get(UUID uuid) {
		if (core.getDB() == null || uuid == null)
			return null;
		return core.getDB().selectEcon(uuid.toString());
	}

	boolean contains(OfflinePlayer player) throws SQLException {
		ResultSet rs = get(player);
		return rs != null && !rs.isClosed() && rs.next();
	}

	boolean validate(ResultSet rs) throws SQLException {
		return rs != null && !rs.wasNull() && !rs.isClosed() && rs.getMetaData() != null && rs.next();
	}

	ResultSet insert(OfflinePlayer player) throws SQLException {
		if (player == null)
			return null;

		return insert(player.getUniqueId());
	}

	ResultSet insert(UUID uuid) throws SQLException {
		if (core.getDB() == null || uuid == null)
			return null;

		core.getDB().insertEcon(uuid.toString());
		return get(uuid);
	}

}

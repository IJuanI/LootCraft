package me.juanco.lc.kits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.kits.listeners.KitDamageListener;
import me.juanco.lc.kits.listeners.KitHungerListener;
import me.juanco.lc.kits.listeners.KitInteractListener;
import me.juanco.lc.kits.listeners.packets.KitInBlockPlaceListener;
import me.juanco.lc.kits.listeners.packets.KitInWindowClickListener;
import me.juanco.lc.kits.listeners.packets.KitOutSetSlotListener;
import me.juanco.lc.packets.PacketHandler;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class Kits {

	private static List<LootKit> kits = new ArrayList<LootKit>();
	private static List<Material> materials = new ArrayList<Material>();
	private static List<KitPlayer> players = new ArrayList<KitPlayer>();

	public Kits(Main main) {
		loadKits();
		registerEventListeners();
		registerPacketListeners(main.getPlaceholders());
	}

	private void loadKits() {
		for (String id : Files.kits.file.getKeys(false)) {
			LootKit kit = new LootKit(id);
			kits.add(kit);
			
			if (kit.getItemLevel() > 0 && !materials.contains(kit.getMaterial()))
				materials.add(kit.getMaterial());
		}
	}

	public void playerJoin(Player player) {
		int index = 0;

		KitPlayer user = Kits.getUser(player);

		for (ItemStack item : player.getInventory().getArmorContents()) {
			int level = 0;

			if (item != null) {
				LootKit kit = Kits.get(item);

				if (kit != null)
					level = kit.getItemLevel();
			}

			user.setEquipment(3 - index, level);
			level = 0;

			index++;
		}

		LootKit top = null, current;

		for (ItemStack item : player.getInventory().getContents())
			if (item != null && (current = Kits.get(item)) != null)
				if (top == null || current.getItemLevel() > top.getItemLevel())
					top = current;

		if (top != null)
			user.setBestItem(top.getItem(), top.getItemLevel());
	}

	public void playerQuit(Player player) {
		remove(player);
	}

	private void registerEventListeners() {
		new KitDamageListener();
		new KitInteractListener();
		new KitHungerListener();
	}

	private void registerPacketListeners(PlaceholderManager placeholders) {
		PacketHandler.register(new KitInBlockPlaceListener(placeholders));
		PacketHandler.register(new KitInWindowClickListener(placeholders));
		PacketHandler.register(new KitOutSetSlotListener(placeholders));
	}

	public LootKit getKit(String id) {
		return Main.ioUtils.get(kits, id);
	}

	public LootKit getKit(ItemStack item) {
		return Main.ioUtils.get(kits, item);
	}

	public KitPlayer getPlayer(Player player) {
		if (player == null)
			return null;

		KitPlayer kPlayer = Main.ioUtils.get(players, player);
		if (kPlayer == null) {
			kPlayer = new KitPlayer(player);
			players.add(kPlayer);
		}

		return kPlayer;
	}

	public void remove(Player player) {
		int index = Main.ioUtils.indexOf(players, player);
		if (index > -1)
			players.remove(index);
	}

	public static LootKit get(String id) {
		return Main.ioUtils.get(kits, id);
	}

	public static LootKit get(ItemStack item) {
		return Main.ioUtils.get(kits, item);
	}

	public static boolean exists(Material mat) {
		return materials.contains(mat);
	}

	public static KitPlayer getUser(Player player) {
		if (player == null)
			return null;

		KitPlayer kPlayer = Main.ioUtils.get(players, player);
		if (kPlayer == null) {
			kPlayer = new KitPlayer(player);
			players.add(kPlayer);
		}

		return kPlayer;
	}
}

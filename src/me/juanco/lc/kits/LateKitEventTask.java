package me.juanco.lc.kits;

import org.bukkit.Bukkit;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.kits.events.KitItemUpdateEvent;

public class LateKitEventTask extends LootTask {

	private static ConcurrentSet<KitItemUpdateEvent> queue = new ConcurrentSet<KitItemUpdateEvent>();

	public LateKitEventTask() {
		super(1, true);
		LootClock.addTask(this);
	}

	@Override
	public void run() {
		for (KitItemUpdateEvent event : queue) {
			Bukkit.getPluginManager().callEvent(event.preCall());
			queue.remove(event);
		}
	}

	public static void add(KitItemUpdateEvent event) {
		queue.add(event);
	}

}

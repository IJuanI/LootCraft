package me.juanco.lc.kits.leveltop;

import me.juanco.lc.api.clock.LootTask;

public class LevelTopUpdateTask extends LootTask {

	private final LevelTopManager top;

	public LevelTopUpdateTask(LevelTopManager top) {
		super(2, true);
		this.top = top;
	}

	@Override
	public void run() {
		top.tick();
	}

}

package me.juanco.lc.kits.leveltop;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.juanco.lc.core.Main;
import me.juanco.lc.files.LootFile;

public class LevelTopFile extends LootFile {

	private List<LevelTopEntry> entries = new ArrayList<LevelTopEntry>();
	private List<LevelTopEntry> modified = new ArrayList<LevelTopEntry>();
	private int size = -1;

	private final DecimalFormat intFormat = new DecimalFormat("0000000000");
	private final DecimalFormat longFormat = new DecimalFormat("0000000000000000000");

	private int last = -1;

	public LevelTopFile(Plugin pl) {
		super(pl, "level.top");

		loadEntries();
	}

	private void loadEntries() {
		List<String> strings = file.getStringList("Entries");

		if (strings != null && !strings.isEmpty()) {
			for (String string : strings)
				entries.add(new LevelTopEntry(string.substring(0, 36), Integer.parseInt(string.substring(36, 46)),
						Long.parseLong(string.substring(46, 65))));

			size = entries.size();
		}
	}

	@Override
	protected void defaults() {
		file.set("Entries", new ArrayList<String>());

		save();
	}

	public void addEntry(LevelTopEntry entry, int pos) {
		entries.add(pos - 1, entry);
		modified.add(entry);

		if (size < 1)
			size = 1;
		else
			size++;
	}

	public void removeEntry(LevelTopEntry entry) {
		entries.remove(entry);

		if (size > 1)
			size--;
		else
			size = -1;
	}

	protected LevelTopEntry getEntry(Player player) {
		return Main.ioUtils.get(entries, player);
	}

	public int getPosition(LevelTopEntry entry) {
		return entries.indexOf(entry);
	}

	public boolean contains(LevelTopEntry entry) {
		return entries.indexOf(entry) > -1;
	}

	public int getPosition(String uuid) {
		return Main.ioUtils.indexOf(entries, uuid);
	}

	public LevelTopEntry getEntry(int position) {
		return entries.get(position);
	}

	public void update(LevelTopEntry entry, int newPosition) {
		entries.remove(entry);
		entries.add(newPosition, entry);
	}

	public LevelTopEntry next() {
		if (last < 0)
			return null;

		LevelTopEntry entry = last >= size - 1 ? null : entries.get(last);

		while (modified.contains(entry) && last < size - 1)
			entry = entries.get(last++);

		last++;
		if (last >= size)
			if (modified.isEmpty())
				last = 0;
			else
				entry = modified.remove(0);

		return entry;
	}

	public int getSize() {
		return size;
	}

	@Override
	public void save() {
		List<String> strings = new ArrayList<String>();

		for (LevelTopEntry entry : entries)
			strings.add(new StringBuilder(entry.getUUID()).append(intFormat.format(entry.getLevel()))
					.append(longFormat.format(entry.getLastUpdate())).toString());

		file.set("Entries", strings);

		super.save();
	}
}

package me.juanco.lc.kits.leveltop;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.mojang.authlib.GameProfile;

import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;
import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.LootKit;
import me.juanco.lc.virtual.VirtualUtils;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PlayerInteractManager;
import net.minecraft.server.v1_8_R3.WorldServer;

public class LevelTopManager {

	private final LevelTopFile top = Files.levelTop;

	private final VirtualUtils virtual;

	public LevelTopManager(Main core) {
		virtual = core.getVirtual().getUtils();

		LootClock.addTask(new LevelTopUpdateTask(this));
	}

	public int getTotalLevel(Player player) {
		LevelTopEntry entry = top.getEntry(player);
		if (entry != null)
			return entry.getLevel();
		return 0;
	}

	public void add(Player player) {
		String uuid = player.getUniqueId().toString();
		int level = getLevel(player);

		LevelTopEntry entry = new LevelTopEntry(uuid, level, System.currentTimeMillis());

		top.addEntry(entry, getPosition(level, top.getSize()));
	}

	public void tick() {
		for (int max = 0, i = 0; i < 4 && max < 40; i++, max++) {
			LevelTopEntry entry = top.next();

			if (entry == null)
				break;

			Player player = getPlayer(entry.getUUID());

			if (entry.getLastUpdate() > player.getLastPlayed())
				i--;
			else {
				int level = getLevel(player);
				int pos = getPosition(level, entry.getPosition());

				if (entry.getPosition() != pos)
					entry.setPosition(pos);
				else
					i--;
			}

		}
	}

	public Player getPlayer(String uuid) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(uuid));

		if (player.isOnline())
			return player.getPlayer();

		else {
			WorldServer ws = ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle();
			EntityPlayer ep = new EntityPlayer(ws.getMinecraftServer(), ws, new GameProfile(player.getUniqueId(), null),
					new PlayerInteractManager(ws));

			return new CraftPlayer(ws.getServer(), ep);
		}

	}

	private int getPosition(int level, int currentPos) {
		int tempLevel = top.getEntry(currentPos - 1).getLevel();
		int lastPos = currentPos;

		if (tempLevel >= level)
			return currentPos;

		lastPos = getRelativePos(level, lastPos, 1000);
		lastPos = getRelativePos(level, lastPos, 100);
		lastPos = getRelativePos(level, lastPos, 10);
		lastPos = getRelativePos(level, lastPos, 1);

		return lastPos;
	}

	private int getRelativePos(int level, int current, int multiplier) {
		int last = current - multiplier;
		int temp = top.getEntry(last).getLevel();

		if (temp >= level)
			return current;

		while (temp < level && last > multiplier)
			temp = top.getEntry(last = last - multiplier).getLevel();

		if (last <= multiplier)
			return last;

		return last + multiplier;
	}

	private int getLevel(Player player) {
		UserFile file = UserFile.getFile(player.getUniqueId());
		List<ItemStack> items = new ArrayList<ItemStack>();

		int level = 0;

		for (int i = 0; i < 4; i++)
			if (virtual.hasChest(i, file))
				for (ItemStack item : virtual.getVirtualContents(player, i).getContents())
					if (item != null)
						items.add(item);

		for (ItemStack item : player.getInventory().getContents())
			if (item != null)
				items.add(item);

		for (ItemStack item : player.getInventory().getArmorContents())
			if (item != null)
				items.add(item);

		for (ItemStack item : items)
			if (Kits.exists(item.getType())) {
				LootKit kit = Kits.get(item);
				if (kit != null)
					level += kit.getItemLevel();
			}

		items.clear();

		return level;
	}

}

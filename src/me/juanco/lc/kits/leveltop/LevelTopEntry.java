package me.juanco.lc.kits.leveltop;

import java.util.UUID;

import org.bukkit.entity.Player;

import me.juanco.lc.files.Files;

public class LevelTopEntry {

	private final String uuid;
	private int level;
	private long lastUpdate = 0;

	public LevelTopEntry(String uuid, int level, long lastUpdate) {
		this.uuid = uuid;
		this.level = level;
		this.lastUpdate = lastUpdate;
	}

	public LevelTopEntry(String uuid, int level) {
		this.uuid = uuid;
		this.level = level;
	}

	public void setLevel(int level) {
		this.level = level;
		lastUpdate = System.currentTimeMillis();
	}

	public void setPosition(int position) {
		Files.levelTop.update(this, position);
	}

	public int getLevel() {
		return level;
	}

	public int getPosition() {
		return Files.levelTop.getPosition(this);
	}

	public boolean shouldUpdate(long lastLogin) {
		return lastUpdate < lastLogin;
	}

	protected String getUUID() {
		return uuid;
	}

	protected long getLastUpdate() {
		return lastUpdate;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return ((Player) obj).getUniqueId().equals(uuid);
		if (obj instanceof UUID)
			return uuid.equals(obj);
		return super.equals(obj);
	}
}

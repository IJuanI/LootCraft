package me.juanco.lc.kits.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.kits.KitPlayer;
import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.LootKit;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class KitItemUpdateEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	private final ItemStack item;
	private final Player target;
	private final boolean check;
	private final PlaceholderManager placeholders;

	public KitItemUpdateEvent(PlaceholderManager placeholders, Player target, boolean check) {
		this.target = target;
		this.placeholders = placeholders;
		this.check = check;
		item = null;
	}

	public KitItemUpdateEvent preCall() {
		KitPlayer user = Kits.getUser(target);

		int level = user.getLevel();
		
		if (check)
			check(user);
		else
			asNull(user);

		if (user.getLevel() != level)
			placeholders.callPlaceholder("item_level", target);

		return this;

	}

	public KitItemUpdateEvent(PlaceholderManager placeholders, ItemStack item, Player target) {
		this.placeholders = null;
		check = false;

		LootKit kit = Kits.get(item);
		KitPlayer user = Kits.getUser(target);

		if (!target.getInventory().contains(item)) {
			this.item = item;
			this.target = null;
			return;
		}

		int level = user.getLevel();

		if (kit != null && kit.getItemLevel() > 0) {
			if (kit.getItemLevel() > user.getBestItemLevel())
				user.setBestItem(item, kit.getItemLevel());

			this.item = item;
			this.target = target;

		} else {
			this.target = target;
			asNull(user);
			this.item = null;
		}

		if (user.getLevel() != level)
			placeholders.callPlaceholder("item_level", target);
	}

	private void asNull(KitPlayer user) {
		if (user.getBestItemLevel() > 0)
			if (empty() || !matches() || !hasBest())
				user.setBestItem(null, 0);
	}

	private void check(KitPlayer user) {
		if (!empty() && matches()) {
			LootKit best = null;

			for (ItemStack item : target.getInventory().getContents())
				if (item != null) {
					LootKit kit = Kits.get(item);
					if (kit != null)
						if (best == null || best.getItemLevel() < kit.getItemLevel())
							best = kit;
				}

			if (best != null)
				user.setBestItem(best.getItem(), best.getItemLevel());
		}
	}

	private boolean empty() {
		for (ItemStack item : target.getInventory().getContents())
			if (item != null)
				return false;
		return true;
	}

	private boolean matches() {
		for (ItemStack item : target.getInventory().getContents())
			if (item != null && Kits.exists(item.getType()))
				return true;
		return false;
	}

	private boolean hasBest() {
		for (ItemStack item : target.getInventory().getContents())
			if (item != null && Kits.get(item) != null)
				return true;

		return false;
	}

	public ItemStack getItem() {
		return item;
	}

	public Player getTarget() {
		return target;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}

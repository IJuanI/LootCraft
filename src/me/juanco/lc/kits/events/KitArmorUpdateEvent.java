package me.juanco.lc.kits.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.LootKit;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class KitArmorUpdateEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	private final ItemStack item;
	private final int slot;
	private final Player target;

	public KitArmorUpdateEvent(PlaceholderManager placeholders, ItemStack item, int slot, Player target) {
		int level = 0;

		if (item != null) {
			LootKit kit = Kits.get(item);

			if (kit != null)
				level = kit.getItemLevel();
		}

		Kits.getUser(target).setEquipment(slot - 5, level);

		placeholders.callPlaceholder("item_level", target);

		this.item = item;
		this.slot = slot;
		this.target = target;
	}

	public ItemStack getItem() {
		return item;
	}

	public int getSlot() {
		return slot;
	}

	public Player getTarget() {
		return target;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}

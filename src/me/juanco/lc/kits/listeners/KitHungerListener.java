package me.juanco.lc.kits.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import me.juanco.lc.api.LootListener;

public class KitHungerListener extends LootListener {

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		if (e.getEntity() instanceof Player && ((Player) e.getEntity()).getFoodLevel() < 20)
			e.setFoodLevel(20);
		else
			e.setCancelled(true);
	}
}

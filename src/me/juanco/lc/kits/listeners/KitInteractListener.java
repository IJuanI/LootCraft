package me.juanco.lc.kits.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

import me.juanco.lc.api.LootListener;
import me.juanco.lc.core.Main;
import me.juanco.lc.kits.LootKit;

public class KitInteractListener extends LootListener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (getMain().getUtils().isRightClick(e.getAction()) && Main.itemUtils.validate(e.getItem())) {
			LootKit kit = getMain().getKits().getKit(e.getItem());
			if (kit != null && kit.getCommand() != null)
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), kit.getCommand());
		}
	}
}

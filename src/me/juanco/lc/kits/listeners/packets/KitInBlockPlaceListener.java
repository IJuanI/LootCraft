package me.juanco.lc.kits.listeners.packets;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.events.KitArmorUpdateEvent;
import me.juanco.lc.packets.PacketListener;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayInBlockPlace;

public class KitInBlockPlaceListener extends PacketListener {

	private final PlaceholderManager placeholders;

	public KitInBlockPlaceListener(PlaceholderManager placeholders) {
		super("PacketPlayInBlockPlace");

		this.placeholders = placeholders;
	}

	@Override
	public boolean listen(Object message, Player target) {
		try {
			PacketPlayInBlockPlace packet = (PacketPlayInBlockPlace) message;

			if (packet.getItemStack() != null && packet.getFace() == 255
					&& packet.a().equals(new BlockPosition(-1, -1, -1)) && packet.d() + packet.e() + packet.f() == 0) {

				ItemStack item = CraftItemStack.asCraftMirror(packet.getItemStack());

				if (!Kits.exists(item.getType()))
					return false;

				Material mat = item.getType();

				KitInListenerArmorTypes type = null;

				for (KitInListenerArmorTypes types : KitInListenerArmorTypes.values())
					if (mat.toString().contains(types.name())) {
						type = types;
						break;
					}

				if (type != null
						&& target.getInventory().getArmorContents()[3 - type.ordinal()].getType().equals(Material.AIR))
					Bukkit.getPluginManager()
							.callEvent(new KitArmorUpdateEvent(placeholders, item, type.ordinal() + 5, target));
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

	private enum KitInListenerArmorTypes {
		HELMET, CHESTPLATE, LEGGINGS, BOOTS;
	}
}

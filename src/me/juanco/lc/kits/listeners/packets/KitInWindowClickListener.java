package me.juanco.lc.kits.listeners.packets;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.LateKitEventTask;
import me.juanco.lc.kits.events.KitArmorUpdateEvent;
import me.juanco.lc.kits.events.KitItemUpdateEvent;
import me.juanco.lc.packets.PacketListener;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;
import net.minecraft.server.v1_8_R3.PacketPlayInWindowClick;

public class KitInWindowClickListener extends PacketListener {

	private final PlaceholderManager placeholders;

	public KitInWindowClickListener(PlaceholderManager placeholders) {
		super("PacketPlayInWindowClick");

		this.placeholders = placeholders;
	}

	@Override
	public boolean listen(Object message, Player target) {
		PacketPlayInWindowClick packet = (PacketPlayInWindowClick) message;

		if (target == null)
			return false;

		if (packet.a() == 0) {
			if (packet.e() != null) {
				if (packet.b() < 9 && packet.f() == 1) {
					ItemStack item = CraftItemStack.asCraftMirror(packet.e());

					if (target.getInventory().firstEmpty() > -1 && Kits.exists(item.getType())) {
						armor(null, packet.b(), target);
						item(item, target, false);
					}
				} else if (packet.f() == 1) {
					ItemStack item = CraftItemStack.asCraftMirror(packet.e());
					Material mat = item.getType();

					KitInListenerArmorTypes type = null;

					for (KitInListenerArmorTypes types : KitInListenerArmorTypes.values())
						if (mat.toString().contains(types.name())) {
							type = types;
							break;
						}

					if (type != null && Kits.exists(item.getType()))
						if (target.getInventory().getArmorContents()[3 - type.ordinal()].getType()
								.equals(Material.AIR)) {
							armor(item, type.ordinal() + 5, target);
							item(null, target, false);
						}
				} else if (packet.b() > 8 && Kits.exists(CraftItemStack.asCraftMirror(packet.e()).getType()))
					item(null, target, false);
			} else if (packet.b() > 8)
				if (packet.e() == null || Kits.exists(CraftItemStack.asCraftMirror(packet.e()).getType()))
					item(null, target, packet.e() == null);
		} else {
			ItemStack item = packet.e() == null ? null : CraftItemStack.asCraftMirror(packet.e());
			if (packet.f() == 1 && item != null) {
				if (Kits.exists(item.getType()))
					if (packet.b() - target.getOpenInventory().getTopInventory().getSize() > -1) {
						Inventory inv = target.getOpenInventory().getTopInventory();
						if (inv.firstEmpty() > -1 || firstPartial(item, inv.getContents()) > -1)
							item(null, target, false);
					} else if (target.getInventory().firstEmpty() > -1
							|| firstPartial(item, target.getInventory().getContents()) > -1)
						item(item, target, false);
			} else if (packet.b() - target.getOpenInventory().getTopInventory().getSize() > -1)
				if (packet.e() == null || Kits.exists(CraftItemStack.asCraftMirror(packet.e()).getType()))
					item(null, target, packet.e() == null);
		}

		return false;
	}

	private void armor(ItemStack item, int slot, Player target) {
		Bukkit.getPluginManager().callEvent(new KitArmorUpdateEvent(placeholders, item, slot, target));
	}

	private void item(ItemStack item, Player target, boolean check) {
		KitItemUpdateEvent event = null;

		if (item == null)
			LateKitEventTask.add(new KitItemUpdateEvent(placeholders, target, check));
		else
			event = new KitItemUpdateEvent(placeholders, item, target);

		if (event != null)
			Bukkit.getPluginManager().callEvent(event);
	}

	private int firstPartial(ItemStack item, ItemStack[] inventory) {
		if (item == null)
			return -1;

		ItemStack filteredItem = CraftItemStack.asCraftCopy(item);

		for (int i = 0; i < inventory.length; i++) {
			ItemStack cItem = inventory[i];
			if (cItem != null && cItem.getAmount() < cItem.getMaxStackSize() && cItem.isSimilar(filteredItem))
				return i;
		}

		return -1;
	}

	private enum KitInListenerArmorTypes {
		HELMET, CHESTPLATE, LEGGINGS, BOOTS;
	}
}

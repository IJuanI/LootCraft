package me.juanco.lc.kits.listeners.packets;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.events.KitArmorUpdateEvent;
import me.juanco.lc.kits.events.KitItemUpdateEvent;
import me.juanco.lc.packets.PacketListener;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;
import net.minecraft.server.v1_8_R3.PacketPlayOutSetSlot;

public class KitOutSetSlotListener extends PacketListener {

	private final PlaceholderManager placeholders;

	private final Field a;
	private final Field b;
	private final Field c;

	public KitOutSetSlotListener(PlaceholderManager placeholders) {
		super("PacketPlayOutSetSlot");

		this.placeholders = placeholders;

		try {
			a = PacketPlayOutSetSlot.class.getDeclaredField("a");
			b = PacketPlayOutSetSlot.class.getDeclaredField("b");
			c = PacketPlayOutSetSlot.class.getDeclaredField("c");

			a.setAccessible(true);
			b.setAccessible(true);
			c.setAccessible(true);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean listen(Object message, Player target) {
		PacketPlayOutSetSlot packet = (PacketPlayOutSetSlot) message;

		if (target == null)
			return false;

		try {
			int slot = (int) b.get(packet);

			if (target != null)
				if ((int) a.get(packet) == 0 && slot > 4 && slot < 9)
					Bukkit.getPluginManager()
					.callEvent(new KitArmorUpdateEvent(placeholders,
							c.get(packet) == null ? null
									: CraftItemStack.asCraftMirror(
											(net.minecraft.server.v1_8_R3.ItemStack) c.get(packet)),
									slot, target));
				else if (c.get(packet) != null) {
					ItemStack item = CraftItemStack
							.asCraftMirror((net.minecraft.server.v1_8_R3.ItemStack) c.get(packet));

					if (Kits.exists(item.getType()))
						Bukkit.getPluginManager().callEvent(new KitItemUpdateEvent(placeholders, item, target));
				} else
					Bukkit.getPluginManager().callEvent(new KitItemUpdateEvent(placeholders, target, false).preCall());

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

}

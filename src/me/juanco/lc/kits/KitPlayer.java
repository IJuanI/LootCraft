package me.juanco.lc.kits;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class KitPlayer {

	private final Player player;
	private int level = 0;

	private ItemStack bestItem;
	private int helmet = 0;
	private int chestplate = 0;
	private int leggings = 0;
	private int boots = 0;

	public KitPlayer(Player player) {
		this.player = player;
	}

	public int getLevel() {
		return level + helmet + chestplate + leggings + boots;
	}

	public ItemStack getBestItem() {
		return bestItem;
	}

	public int getBestItemLevel() {
		return level;
	}

	public Player getPlayer() {
		return player;
	}

	public void setBestItem(ItemStack bestItem, int level) {
		this.level = level;
		this.bestItem = bestItem;
	}

	public void setEquipment(int index, int level) {
		switch (index) {
		case 0:
			helmet = level;
			break;
		case 1:
			chestplate = level;
			break;
		case 2:
			leggings = level;
			break;
		default:
			boots = level;
			break;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		return super.equals(obj);
	}
}

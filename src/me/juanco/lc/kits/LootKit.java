package me.juanco.lc.kits;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import me.juanco.lc.api.LootAttribute;
import me.juanco.lc.api.LootAttribute.AttributeType;
import me.juanco.lc.api.LootProperties;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.Files;

public class LootKit {

	private static final String levelLore = "&f&lItem Level &7&l- [&b&l%s&7&l]";
	private final LootProperties props = new LootProperties();

	protected LootKit(String id) {
		ConfigurationSection sec = Files.kits.file.getConfigurationSection(id);

		int level = getInt(sec, "item level", 0);
		props.set("level", level);
		props.set("command", getString(sec, "command", null));

		Material mat;
		short data = (short) 0;

		String mName = getString(sec, "item.id", "Stone");
		if (mName.contains(":")) {
			String[] values = mName.split(":");
			mat = Material.matchMaterial(values[0]);
			data = Short.parseShort(values[1]);
		} else
			mat = Material.matchMaterial(mName);

		props.set("material", mat);

		String name = getString(sec, "item.name", null);
		List<String> lore = getStringList(sec, "item.lore", null);
		int r = getInt(sec, "item.color.r", -1);
		int g = getInt(sec, "item.color.g", -1);
		int b = getInt(sec, "item.color.b", -1);

		if (level > 0)
			lore.add(0, String.format(levelLore, level));

		double damage = getDouble(sec, "damage", 1);
		double health = getDouble(sec, "max hearts", 0);

		LootAttribute attribute = new LootAttribute(Main.itemUtils.get(new ItemStack(mat, 1, data), name, lore));

		if (health < 1)
			attribute.add("Damage", damage, AttributeType.Damage);

		if (health > 0)
			attribute.add("Health", health, AttributeType.Health);

		ItemStack item = attribute.unbreakable().getStack();

		if (r > -1 && g > -1 && b > -1)
			try {
				LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
				meta.setColor(Color.fromRGB(r, g, b));

				item.setItemMeta(meta);

			} catch (Exception ex) {
				System.out.println(item);
				System.out.println(sec);
			}

		props.set("item", item);
		props.set("id", id);
	}

	public String getID() {
		return props.get("id");
	}

	public int getItemLevel() {
		return props.get("level");
	}

	public Material getMaterial() {
		return props.get("material");
	}

	public String getCommand() {
		return props.get("command");
	}

	public ItemStack getItem() {
		return props.get("item");
	}

	private int getInt(ConfigurationSection sec, String path, int def) {
		if (sec.contains(path))
			return sec.getInt(path);
		return def;
	}

	private double getDouble(ConfigurationSection sec, String path, double def) {
		if (sec.contains(path))
			return sec.getDouble(path);
		return def;
	}

	private String getString(ConfigurationSection sec, String path, String def) {
		if (sec.contains(path))
			return sec.getString(path);
		return def;
	}

	private List<String> getStringList(ConfigurationSection sec, String path, List<String> def) {
		if (sec.contains(path))
			return sec.getStringList(path);
		return def;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return ((String) props.get("id")).toLowerCase().equals(obj);
		else if (obj instanceof ItemStack) {
			ItemStack a = props.get("item");
			ItemStack b = (ItemStack) obj;

			return a == b || a == null == (b == null) && (a == null || a.getType().equals(b.getType()) && checkMeta(a, b));
		}
		return super.equals(obj);
	}
	
	private boolean checkMeta(ItemStack a, ItemStack b) {
		boolean c = a.hasItemMeta();
		boolean d = c == b.hasItemMeta();
		
		boolean e = d;
		
		if (c && d) {
			boolean f = a.getItemMeta().hasDisplayName();
			boolean g = f == b.getItemMeta().hasDisplayName();
			
			boolean h = a.getItemMeta().hasLore();
			boolean i = h == b.getItemMeta().hasLore();
			
			boolean j = a.getItemMeta().getItemFlags().equals(b.getItemMeta().getItemFlags());
			
			if (f && g)
				g = a.getItemMeta().getDisplayName().equals(b.getItemMeta().getDisplayName());
			
			if (h && i)
				i = a.getItemMeta().getLore().equals(b.getItemMeta().getLore());
			
			
			e = g && i && j;
		}
		
		return e;
	}
}

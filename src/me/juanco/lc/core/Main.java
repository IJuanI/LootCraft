package me.juanco.lc.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

import me.juanco.lc.api.BanManager;
import me.juanco.lc.api.LootListener;
import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.database.Credentials;
import me.juanco.lc.api.database.Database;
import me.juanco.lc.api.database.Table;
import me.juanco.lc.api.format.ChatFormatter;
import me.juanco.lc.api.format.LootFormatter;
import me.juanco.lc.api.inventory.LootInventoryListener;
import me.juanco.lc.api.leaderboard.LeaderboardUpdaterTask;
import me.juanco.lc.api.user.APIJoinListener;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.commands.ArenaCommand;
import me.juanco.lc.commands.CrateCommand;
import me.juanco.lc.commands.HologramsCommand;
import me.juanco.lc.commands.KitsCommand;
import me.juanco.lc.commands.LightCommand;
import me.juanco.lc.commands.MatchCommand;
import me.juanco.lc.commands.PayrollCommand;
import me.juanco.lc.commands.TradeCommand;
import me.juanco.lc.commands.VirtualCommand;
import me.juanco.lc.commands.economy.EconomyCommand;
import me.juanco.lc.commands.economy.MoneyCommand;
import me.juanco.lc.commands.economy.TokensCommand;
import me.juanco.lc.crates.Crates;
import me.juanco.lc.crates.listeners.CrateInventoryListener;
import me.juanco.lc.crates.listeners.CratePlayerInteractListener;
import me.juanco.lc.crates.tasks.CrateAnimationTask;
import me.juanco.lc.economy.Currency;
import me.juanco.lc.economy.Economy;
import me.juanco.lc.economy.LateUpdateTask;
import me.juanco.lc.files.Files;
import me.juanco.lc.holograms.Holograms;
import me.juanco.lc.holograms.listeners.HoloFrameListener;
import me.juanco.lc.impl.Featherboard;
import me.juanco.lc.impl.HideAttributes;
import me.juanco.lc.kits.Kits;
import me.juanco.lc.kits.LateKitEventTask;
import me.juanco.lc.kits.leveltop.LevelTopManager;
import me.juanco.lc.match.Matches;
import me.juanco.lc.packets.PacketHandler;
import me.juanco.lc.packets.PacketInjector;
import me.juanco.lc.payroll.Payrolls;
import me.juanco.lc.regen.Regen;
import me.juanco.lc.regen.tasks.RegenBlockTask;
import me.juanco.lc.regen.tasks.RegenSheepTask;
import me.juanco.lc.rsa.RSA;
import me.juanco.lc.scoreboard.ScoreboardManager;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;
import me.juanco.lc.scoreboard.sidebar.placeholders.impl.ItemLevelPlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.impl.LosePlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.impl.MoneyPlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.impl.PayrollAmountPlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.impl.PayrollTimePlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.impl.TokensPlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.impl.WinPlaceholder;
import me.juanco.lc.trades.Trades;
import me.juanco.lc.utils.IOUtils;
import me.juanco.lc.utils.ItemUtils;
import me.juanco.lc.utils.Utils;
import me.juanco.lc.virtual.Virtual;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.ChunkCoordIntPair;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.EnumSkyBlock;

public class Main extends JavaPlugin {

	public static final File folder = new File("plugins/Loot Craft");

	public static final IOUtils ioUtils = new IOUtils();
	public static final ItemUtils itemUtils = new ItemUtils();

	public static Economy money;
	public static Economy tokens;

	private final Utils utils = new Utils();
	private Crates crates;
	private Payrolls payrolls;
	private Virtual virtual;
	private Kits kits;
	private Matches matches;
	private Trades trades;
	private Regen regen;
	private Holograms holograms;
	private Database database;
	private LevelTopManager levelManager;
	private LootFormatter formatter;
	private PacketInjector packetInjector;
	private PlaceholderManager placeholders;
	private ScoreboardManager scoreboard;
	private BanManager bans;

	private RSA rsa;

	@Override
	public void onEnable() {
		rsa = new RSA('@');

		Holograms.register();
		new Files(this);

		loadCommands();
		loadEvents();
		initTasks();

		initOthers();
		loadPlaceholders();

		initSubMains();
		connectDatabase();

		for (Player player : Bukkit.getOnlinePlayers())
			onPlayerEnable(player);
	}

	@Override
	public void onDisable() {
		payrolls.onDisable();
		holograms.disable();
		trades.disable();

		Files.disable();
		UserFile.disable();

		RegenBlockTask.disable();
		RegenSheepTask.disable();

		try {
			if (database != null)
				database.exit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		for (Player player : Bukkit.getOnlinePlayers())
			onPlayerDisable(player);
	}

	public void onPlayerEnable(Player player) {
		packetInjector.addPlayer(player);
		kits.playerJoin(player);

		scoreboard.addPlayer(player, false);
	}

	public void onPlayerDisable(Player player) {
		packetInjector.removePlayer(player);
		crates.leave(player);

		scoreboard.removePlayer(player);
	}

	public void playerJoin(Player player) {
		player.setPlayerListName(formatter.tabFormat(player));
		player.setFoodLevel(20);

		scoreboard.addPlayer(player, true);

		packetInjector.addPlayer(player);
		kits.playerJoin(player);

		List<net.minecraft.server.v1_8_R3.Chunk> chunks = new ArrayList<net.minecraft.server.v1_8_R3.Chunk>();

		for (Location loc : LightCommand.locs)
			if (loc.getChunk().isLoaded()) {
				net.minecraft.server.v1_8_R3.Chunk chunk = ((CraftChunk)loc.getChunk()).getHandle();
				if (!chunks.contains(chunk))
					chunks.add(chunk);
			}

		for (net.minecraft.server.v1_8_R3.Chunk chunk : chunks) {
			EntityPlayer entity = ((CraftPlayer) player).getHandle();
			if (entity.world.equals(chunk.world))
				entity.chunkCoordIntPairQueue.add(new ChunkCoordIntPair(chunk.locX, chunk.locZ));
		}
	}

	public void playerQuit(Player player) {
		packetInjector.removePlayer(player);
		kits.playerQuit(player);
		HoloFrameListener.remove(player);
		crates.leave(player);

		scoreboard.removePlayer(player);

		Team team = player.getScoreboard().getEntryTeam(player.getName());
		if (team != null)
			team.unregister();
	}

	public void onChunkLoad(Chunk chunk) {
		net.minecraft.server.v1_8_R3.Chunk c = ((CraftChunk) chunk).getHandle();
		int X = c.locX;
		int Z = c.locZ;

		for (Location loc : LightCommand.locs) {
			int x = floor(loc.getX()) >> 4;
			int z = floor(loc.getY()) >> 4;

			if (x == X && z == Z)
				c.a(EnumSkyBlock.BLOCK, new BlockPosition(loc.getX(), loc.getY(), loc.getZ()), 15);
		}
	}

	public RSA getRSA(char key) {
		if (key == 0x40)
			return rsa;
		return null;
	}

	void loadCommands() {
		new EconomyCommand(this);
		new MoneyCommand(this);
		new TokensCommand(this);

		new CrateCommand(this);
		new PayrollCommand(this);
		new KitsCommand(this);
		new VirtualCommand(this);
		new MatchCommand(this);
		new ArenaCommand(this);

		new TradeCommand(this);
		new HologramsCommand(this);
		new LightCommand(this);
	}

	void loadEvents() {
		LootListener.setup(this);

		new LootInventoryListener();
		new APIJoinListener();
		new ChatFormatter();

		new CrateInventoryListener();
		new CratePlayerInteractListener();

		PacketHandler.register(new HideAttributes());
	}

	void initTasks() {
		new LootClock(this);
		new CrateAnimationTask(this);

		new LateUpdateTask();
		new LateKitEventTask();
		new LeaderboardUpdaterTask();
	}

	void initOthers() {
		money = new Economy(Currency.Money, this);
		tokens = new Economy(Currency.Tokens, this);

		if (Bukkit.getPluginManager().isPluginEnabled("FeatherBoard"))
			new Featherboard(this);

		packetInjector = new PacketInjector();
	}

	void initSubMains() {
		crates = new Crates(this);
		payrolls = new Payrolls(this);
		kits = new Kits(this);
		virtual = new Virtual(this);
		matches = new Matches(this);
		regen = new Regen(this);

		trades = new Trades(this);
		holograms = new Holograms(this);

		levelManager = new LevelTopManager(this);
		formatter = new LootFormatter(this);

		scoreboard = new ScoreboardManager(this, placeholders);
		bans = new BanManager(this);
	}

	void loadPlaceholders() {
		placeholders = new PlaceholderManager();

		placeholders.addPlaceholder(new PayrollAmountPlaceholder(this));
		placeholders.addPlaceholder(new PayrollTimePlaceholder(this));
		placeholders.addPlaceholder(new ItemLevelPlaceholder());
		placeholders.addPlaceholder(new TokensPlaceholder(placeholders));
		placeholders.addPlaceholder(new MoneyPlaceholder(placeholders));
		placeholders.addPlaceholder(new LosePlaceholder());
		placeholders.addPlaceholder(new WinPlaceholder());
	}

	void connectDatabase() {
		Credentials cred = new Credentials(get("Database.host"), get("Database.username"), get("Database.password"),
				get("Database.db"), Files.config.file.getInt("Database.port"));

		registerTables();

		try {
			database = new Database(cred);
		} catch (IllegalArgumentException ex) {
			database = null;
		}
	}

	void registerTables() {
		Table economy = new Table("Economy", "uuid");
		economy.addField("uuid", "String", 36, false, true);
		economy.addField("Money", "Long", false);
		economy.addField("Tokens", "Long", false);
		Database.addTable(economy);

		Table banned = new Table("Banned", "uuid");
		banned.addField("uuid", "String", 36, false, true);
		banned.addField("Reason", "String", 64, false);
		banned.addField("Time", "Date", false);
		Database.addTable(banned);
	}

	public Utils getUtils() {
		return utils;
	}

	public Crates getCrates() {
		return crates;
	}

	public Payrolls getPayrolls() {
		return payrolls;
	}

	public Virtual getVirtual() {
		return virtual;
	}

	public Kits getKits() {
		return kits;
	}

	public Matches getMatches() {
		return matches;
	}

	public Trades getTrades() {
		return trades;
	}

	public Regen getRegen() {
		return regen;
	}

	public Database getDB() {
		return database;
	}

	public Holograms getHolograms() {
		return holograms;
	}

	public LevelTopManager getLevelManager() {
		return levelManager;
	}

	public LootFormatter getFormatter() {
		return formatter;
	}

	public PlaceholderManager getPlaceholders() {
		return placeholders;
	}

	public ScoreboardManager getScoreboard() {
		return scoreboard;
	}

	public BanManager getBans() {
		return bans;
	}

	String get(String path) {
		return Files.config.file.getString(path);
	}

	private int floor(double num) {
		int floor = (int) num;
		return floor == num ? floor : floor - (int) (Double.doubleToRawLongBits(num) >>> 63);
	}
}

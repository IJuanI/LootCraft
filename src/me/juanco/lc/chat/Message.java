package me.juanco.lc.chat;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import me.juanco.lc.chat.json.JsonManager;
import me.juanco.lc.files.Files;

public class Message {

	static final MsgUtils utils = new MsgUtils();

	static String ePrefix = "&c&l[&4&lLootCraft&c&l]&d&l";
	static String sPrefix = "&b&l[&f&lLootCraft&b&l]&a&l";
	static String aPrefix = "&e&l[&d&lLootCraft&e&l]&b&l";

	private static JsonManager json = new JsonManager();


	public Player current;
	public final CommandSender receptor;

	public Message(CommandSender receptor) {
		this.receptor = receptor;
	}


	public void rawMsg(String... msgs) { rawMsg(Arrays.asList(msgs)); }
	public void rawMsg(List<String> msgs) {
		if (msgs != null)
			for (String msg : msgs)
				rawMsg(msg);
	}

	private void rawMsg(String msg) {
		boolean player = receptor instanceof Player;

		if (msg.indexOf('\n') > -1)
			for (String message : msg.split("\n"))
				if (player)
					receptor.sendMessage(translatePlayer(message));
				else
					receptor.sendMessage(translateConsole(message));
		else if (player)
			receptor.sendMessage(translatePlayer(msg));
		else
			receptor.sendMessage(translateConsole(msg));
	}

	public void msgWithReplaces(String message, String... replaces) {
		if (replaces == null || replaces.length < 2) msg(false, message);
		else {
			String[] array = getArray(message);
			if (array != null) {
				int totalReplaces = utils.divideExact(replaces.length, 2) - 1;
				String[][] replaceArray = new String[totalReplaces][2];
				for (int index = 0; index < totalReplaces; index++) {
					replaceArray[index][0] = replaces[index*2];
					replaceArray[index][1] = replaces[index*2+1];
				}
				for (String msg : array) {
					msg = replaces(msg, current);
					if (msg != null)
						for (int index = 0; index < totalReplaces; index++) {
							String repK = replaceArray[index][0];
							String repV = replaceArray[index][1];
							if (msg.indexOf(repK) > -1) msg = msg.replace(repK, repV);
						}
					if (msg.startsWith("J:")) {
						msg = msg.substring(2);
						if (receptor instanceof Player)
							if (json.contains(message))
								json.sendJson((CraftPlayer) receptor, json.get(message).end());
							else
								json.send((CraftPlayer) receptor, translatePlayer(msg), message);
						else if (json.contains(message))
							rawMsg(json.get(message).getRawText());
						else
							rawMsg(json.create(msg, message).getRawText());
					} else
						rawMsg(msg);
				}
			}
		}
	}

	public void msg(String... msgs) { msg(false, msgs); }
	public void msg(boolean prefix, String... msgs) {
		String[] parsed = new String[msgs.length];
		for (int index = 0; index < msgs.length; index++) {
			String[] array = getArray(msgs[index]);
			if (array != null)
				for (String message : array) {
					message = replaces(message, current);
					if (prefix) {
						message = aPrefix + message;
						prefix = false;
					}
					if (parsed[index] == null)
						parsed[index] = message;
					else parsed[index] += "\n" + message;
				}

			if (parsed[index] != null)
				if (parsed[index].startsWith("J:")) {
					parsed[index] = parsed[index].substring(2);
					if (receptor instanceof Player)
						if (json.contains(msgs[index]))
							json.sendJson((CraftPlayer) receptor, json.get(msgs[index]).end());
						else
							json.send((CraftPlayer) receptor, translatePlayer(parsed[index]), msgs[index]);
					else if (json.contains(msgs[index]))
						rawMsg(json.get(msgs[index]).getRawText());
					else
						rawMsg(json.create(parsed[index], msgs[index]).getRawText());
				} else
					rawMsg(parsed[index]);
		}
	}


	public static String getMsg(String msg) { return getMsg(false, msg, null); }
	public static String getMsg(boolean prefix, String msg, Player p) {
		String parsed = "";
		String[] array = Files.lang.get(msg);
		if (array != null && array.length > 0)
			for (String message : array) {
				message = replaces(message, p);
				if (prefix) {
					message = aPrefix + message;
					prefix = false;
				}
				if (parsed.isEmpty()) parsed += message;
				else parsed += "\n" + message;
			}
		else broken(msg);
		return parsed;
	}


	public void e(String msg) { rawMsg(ePrefix + msg); }
	public void s(String msg) { rawMsg(sPrefix + msg); }
	public void a(String msg) { rawMsg(aPrefix + msg); }

	public static String eS(String msg) { return t(ePrefix + msg); }
	public static String sS(String msg) { return t(sPrefix + msg); }
	public static String aS(String msg) { return t(aPrefix + msg); }

	String translateConsole(String original) {
		return ChatColor.translateAlternateColorCodes('&',
				original.startsWith("<center>") ? original.substring(8) : original);
	}

	String translatePlayer(String original) {
		return ChatColor.translateAlternateColorCodes('&',
				original.startsWith("<center>") ? utils.center(original.substring(8)) : original);
	}

	public static String t(String original) {
		return ChatColor.translateAlternateColorCodes('&', original);
	}

	public static void broadcast(String msg) {
		Bukkit.broadcastMessage(t(replaces(msg, null)));
	}
	public static void broadcast(String[] messages) {
		String msg = "";
		for (String str : messages) msg += "\n" + str;
		Bukkit.broadcastMessage(t(msg.substring(1)));
	}


	public static void broadcastWithReplaces(String message, String... replaces) {
		if (replaces == null || replaces.length < 2) broadcast(false, message);
		else {
			String[] array = getArray(message);
			if (array != null) {
				int totalReplaces = utils.divideExact(replaces.length, 2) - 1;
				String[][] replaceArray = new String[totalReplaces][2];
				for (int index = 0; index < totalReplaces; index++) {
					replaceArray[index][0] = replaces[index*2];
					replaceArray[index][1] = replaces[index*2+1];
				}
				for (String msg : array) {
					msg = replaces(msg, null);
					if (msg != null)
						for (int index = 0; index < totalReplaces; index++) {
							String repK = replaceArray[index][0];
							String repV = replaceArray[index][1];
							if (msg.indexOf(repK) > -1) msg = msg.replace(repK, repV);
						}
					broadcast(msg);
				}
			}
		}
	}

	public static void broadcast(boolean prefix, String... paths) {
		String[] parsed = new String[paths.length];
		for (int index = 0; index < paths.length; index++) {
			String[] array = getArray(paths[index]);
			if (array != null)
				for (String message : array) {
					if (message.equals("none")) {
						parsed[index] = null;
						continue;
					}
					message = replaces(message, null);
					if (prefix) message = aPrefix + message;
					prefix = false;
					if (parsed[index] == null || parsed[index].isEmpty()) parsed[index] = message;
					else parsed[index] += "\n" + message;
				}
		}
		broadcast(parsed);
	}


	public Message setPlayer(Player p) { current = p; return this; }

	private static void broken(String path) {
		Bukkit.getConsoleSender().sendMessage(t("&cLang file Broken!\n&7Path: " + path));
	}

	private static String[] getArray(String msg) {
		String[] array = Files.lang.get(msg);
		if (array == null) {
			broken(msg);
			return null;
		}
		else for (String message : array) if (message == null) { broken(msg); return null; }
		return array;
	}

	private static String replaces(String message, Player player) {
		if (message == null || message.equals("none"))
			return null;
		else {
			String m = message.toLowerCase();
			if (m.startsWith("j:")) {
				if (m.contains("%error"))
					message = message.replaceAll("%error", ePrefix);
				else if (m.contains("%success"))
					message = message.replaceAll("%success", sPrefix);
				else if (m.contains("%alert"))
					message = message.replaceAll("%alert", aPrefix);
			} else if (m.startsWith("%error"))
				message = ePrefix + message.substring(6);
			else if (m.startsWith("%success"))
				message = sPrefix + message.substring(8);
			else if (m.startsWith("%alert"))
				message = aPrefix + message.substring(6);
		}
		if (player != null)
			message = message.replaceAll("%p", player.getDisplayName());
		return message;
	}


	public static void send(String msg, CommandSender sender) {
		try {
			new Message(sender).msg(msg);
		} catch(Exception ex) {
			System.out.println("Couldn't send a Message!");
			ex.printStackTrace();
		}
	}
	public static void sendWithReplaces(String msg, CommandSender sender, String... replaces) {
		new Message(sender).msgWithReplaces(msg, replaces);
	}
}

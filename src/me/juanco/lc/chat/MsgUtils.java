package me.juanco.lc.chat;

import net.md_5.bungee.api.ChatColor;

public class MsgUtils {

	final String colors = ChatColor.ALL_CODES;

	public String center(String from) {
		int totalWidth = 0;
		int index = 0;
		boolean bold = false;
		boolean lastColor = false;
		while (index < from.length()) {
			char Char = from.charAt(index);
			if (lastColor) {
				if (colors.indexOf(Char) > -1) {
					if (Char == 'l' || Char == 'L') bold = true;
					else if (bold)
						switch(Char) { case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'r':
						case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '0': bold = false; }
				} else totalWidth += getWidth(Char, bold) + (bold ? 21 : 18);
				lastColor = false;
			} else if (Char == '�' || Char == '&')
				lastColor = true;
			else totalWidth += getWidth(Char, bold);
			index++;
		}
		int spaces = divideExact(divideExact(960 - totalWidth, 2), 12);
		index = 0;
		String sSpaces = "";
		while (index < spaces) {
			sSpaces += ' ';
			index++;
		}
		return sSpaces + from;
	}

	int getWidth(char Char, boolean bold) {
		return bold ? getBold(Char) : getWidth(Char);
	}

	int getWidth(char Char) {
		switch (Char) {
		case ' ': case 't': return 12;
		case 'i': case ';': case ',': case '.': case ':': case 'l': case '!': return 6;
		case 'f': case 'k': return 10;
		case '\'': return 9;
		default: return 18;
		}
	}

	int getBold(char Char) {
		int extra = 3;
		switch (Char) { case 'f': case 'k': case '\'': extra = 2; case ' ': extra = 0; }
		return getWidth(Char) + extra;
	}

	public int divideExact(int toDivide, int divisor) {
		return toDivide % divisor == 0 ? toDivide / divisor + 1 : toDivide / divisor;
	}
}

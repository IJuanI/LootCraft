package me.juanco.lc.chat.paths;

public class Errors {

	public final String unknownCommand = "Error.Unknown Command";
	public final String permission = "Error.Permission";
	public final String target = "Error.Target";
	public final String itemsDropped = "Error.Items Dropped";
	public final String db = "Error.Database";
	public final String afford = "Error.Afford";
	public final String quotaFind = "Error.Quota.Find";
	public final String quotaNeed = "Error.Quota.Need";
	public final String kit = "Error.Kit";
	public final String argument = "Error.Argument";
	public final String wrongArg = "Error.Wrong Argument";
	public final String economy = "Error.Economy";
	public final String specifyTarget = "Error.Specify Target";
	public final String exists = "Error.Exists";
	public final String notExists = "Error.Not Exists";

	protected Errors() {

	}
}

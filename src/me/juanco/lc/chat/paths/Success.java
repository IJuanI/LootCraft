package me.juanco.lc.chat.paths;

public class Success {

	public final String payQuota = "Success.Quota.Pay";
	public final String addQuota = "Success.Quota.Add";
	public final String clearQuota = "Success.Quota.Clear";
	public final String kit = "Success.Kit";
	public final String purchase = "Success.Purchase";

	public final String addLight = "Success.Light.Added";
	public final String removeLight = "Success.Light.Remove";

	public final Economy ECONOMY = new Economy();

	protected Success() {

	}
}

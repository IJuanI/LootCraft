package me.juanco.lc.chat.paths;

public class Pvp {

	public final String combat = "Error.Pvp.Combat";
	public final String exitSelf = "Error.Pvp.Exit.Self";
	public final String exitOther = "Error.Pvp.Exit.Other";

	public final String queue = "Success.Pvp.Queue";
	public final String win = "Success.Pvp.Won";

	public final String dequeue = "Alert.Pvp.Dequeue";
	public final String found = "Alert.Pvp.Found";
	public final String wait1 = "Alert.Pvp.Waiting1";
	public final String wait2 = "Alert.Pvp.Waiting2";
	public final String lose = "Alert.Pvp.Lost";
	public final String arena = "Alert.Pvp.Arena";
	
	public final String highWep = "Alert.Pvp.HighWep";

	protected Pvp() {

	}
}

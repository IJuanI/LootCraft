package me.juanco.lc.chat.paths;

public class Holograms {

	public final String exists = "Alert.Hologram.Exists";
	public final String notExists = "Alert.Hologram.Not Exists";

	public final String create = "Success.Hologram.Create";
	public final String delete = "Success.Hologram.Delete";

	public final String removeLine = "Success.Hologram.Line Remove";
	public final String addLine = "Success.Hologram.Line Add";

	public final String command = "Success.Hologram.Command";

	public final String clone = "Success.Hologram.Clone";
	public final String moveHere = "Success.Hologram.Teleport";

	protected Holograms() {

	}
}

package me.juanco.lc.chat.paths;

public class Messages {

	public static final Errors ERRORS = new Errors();
	public static final Success SUCCESS = new Success();
	public static final Alerts ALERTS = new Alerts();
}

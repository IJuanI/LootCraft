package me.juanco.lc.chat.paths;

public class Arena {

	public final String exists = "Alert.Arena.Exists";
	public final String notExists = "Alert.Arena.Not Exists";

	public final String create = "Success.Arena.Create";
	public final String delete = "Success.Arena.Delete";
	public final String warp = "Success.Arena.Set Warp";

	protected Arena() {

	}
}

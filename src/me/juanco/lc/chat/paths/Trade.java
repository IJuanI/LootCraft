package me.juanco.lc.chat.paths;

public class Trade {

	public final String acceptO = "Success.Trade.Start";
	public final String acceptT = "Alert.Trade.Start";
	public final String declineY = "Alert.Trade.Decline.You";
	public final String declineO = "Alert.Trade.Decline.Other";
	public final String request = "Error.Trade.Request";
	public final String still = "Alert.Trade.Still";

	public final String alreadyOQ = "Error.Trade.Already.Owner Queued";
	public final String alreadyOI = "Error.Trade.Already.Owner Invited";
	public final String alreadyTQ = "Error.Trade.Already.Target Queued";
	public final String alreadyTI = "Error.Trade.Already.Target Invited";
	public final String trading = "Error.Trade.Already.Trading";

	public final String started = "Success.Trade.Started";
	public final String level = "Error.Trade.Level";

	public final String cancelY = "Alert.Trade.Cancel.You";
	public final String cancelO = "Alert.Trade.Cancel.Other";
	public final String cancel = "Alert.Trade.Cancel.Server";

	public final String cancelQY = "Alert.Trade.Cancel.Queue.You";
	public final String cancelQO = "Alert.Trade.Cancel.Queue.Other";
	public final String expireO = "Alert.Trade.TimeOut.Owner";
	public final String expireT = "Alert.Trade.TimeOut.Target";

	protected Trade() {

	}
}

package me.juanco.lc.chat.paths;

public class Economy {

	public final String get = "Success.Economy.Get";
	public final String add = "Success.Economy.Add";
	public final String remove = "Success.Economy.Remove";
	public final String set = "Success.Economy.Set";

	public final String added = "Alert.Economy.Added";

	protected Economy() {

	}
}

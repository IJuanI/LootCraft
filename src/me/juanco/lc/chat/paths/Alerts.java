package me.juanco.lc.chat.paths;

public class Alerts {

	public final Arena ARENA = new Arena();
	public final Pvp PVP = new Pvp();
	public final Holograms HOLO = new Holograms();
	public final Trade TRADE = new Trade();

	public final String CCTuto = "Alert.Crate Chest.Tutorial";
	public final String CCSuccess = "Alert.Crate Chest.Success";
	public final String CCError = "Alert.Crate Chest.Error";

	protected Alerts() {

	}
}

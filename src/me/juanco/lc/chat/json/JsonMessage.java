package me.juanco.lc.chat.json;

import org.json.simple.JSONObject;

public class JsonMessage extends JsonBuilder {

	private String id;

	protected JsonMessage(String path) {
		id = path;
	}

	protected void append(String text) {
		super.addLine(text);
	}

	protected void append(JSONObject object) {
		super.addSegment(object);
	}

	@Override
	public JsonBuilder addLine(String line) {
		return this;
	}

	@Override
	protected void addSegment(JSONObject segment) {
	}

	@Override
	public JsonSegment advAddLine(String line) {
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return id.equals(obj);
		return super.equals(obj);
	}
}

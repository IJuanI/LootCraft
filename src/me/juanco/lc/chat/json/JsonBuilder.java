package me.juanco.lc.chat.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class JsonBuilder {

	private JSONObject object = new JSONObject();
	private JSONArray extra = new JSONArray();

	protected JsonBuilder() {
		object.put("text", "");
	}

	public JsonBuilder addLine(String line) {
		extra.add(line);
		return this;
	}

	public JsonSegment advAddLine(String line) {
		return new JsonSegment(this, line);
	}

	protected void addSegment(JSONObject segment) {
		extra.add(segment);
	}


	public String end() {
		object.put("extra", extra);
		return object.toJSONString();
	}

	public String getRawText() {
		StringBuilder builder = new StringBuilder();

		for (Object obj : extra)
			if (obj instanceof String)
				builder.append(obj);
			else if (obj instanceof JSONObject)
				builder.append(((JSONObject) obj).get("text"));

		return builder.toString();
	}
}

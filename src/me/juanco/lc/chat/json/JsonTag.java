package me.juanco.lc.chat.json;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

public class JsonTag {

	private List<Entry> entries = new ArrayList<Entry>();
	private String text;

	protected JsonTag(String value) {
		System.out.println(value);
		String[] data = value.split(",");
		text = data[0];

		for (int i = 1; i < data.length; i++) {
			if (data[i].charAt(0) == ' ')
				data[i] = data[i].substring(1);
			String[] dat = data[i].split("=");
			entries.add(new Entry(dat[0].toLowerCase(), dat[1].toLowerCase()));
		}
	}

	protected JSONObject asJsonObject() {
		JsonSegment seg = new JsonSegment(null, text);

		for (Entry entry : entries)
			switch (entry.key.toLowerCase()) {
			case "suggest":
				seg.clickEvent(JsonClickEvent.suggest_command, entry.value);
				break;
			case "run":
				seg.clickEvent(JsonClickEvent.run_command, entry.value);
				break;
			case "link":
			case "url":
				seg.clickEvent(JsonClickEvent.open_url, entry.value);
				break;
			case "achievement":
				seg.hoverAchievement(entry.value);
				break;
			case "insert":
			case "insertion":
				seg.insert(entry.value);
				break;
			case "hover":
				seg.hoverString((Object[]) entry.value.substring(1, entry.value.length() - 1).split("\"; \""));
				break;
			}

		return seg.get();
	}

	private class Entry {

		private final String key, value;

		private Entry(String key, String value) {
			this.key = key;
			this.value = value;
		}
	}
}

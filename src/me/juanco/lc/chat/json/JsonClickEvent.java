package me.juanco.lc.chat.json;

public enum JsonClickEvent {

	open_url, open_file, run_command, suggest_command, change_page;
}

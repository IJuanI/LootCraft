package me.juanco.lc.chat.json;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;

import me.juanco.lc.core.Main;
import me.juanco.lc.utils.IOUtils;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class JsonManager {

	private List<JsonMessage> messages = new ArrayList<JsonMessage>();

	public JsonBuilder newJson() {
		return new JsonBuilder();
	}

	public void sendJson(CraftPlayer player, String json) {
		player.getHandle().playerConnection.sendPacket(new PacketPlayOutChat(ChatSerializer.a(json)));
	}

	public void send(CraftPlayer player, String message) {
		send(player, message, null);
	}

	public void send(CraftPlayer player, String originalText, String path) {
		sendJson(player, create(originalText, path).end());
	}

	public JsonMessage create(String originalText, String path) {
		JsonMessage message = new JsonMessage(path);
		IOUtils u = Main.ioUtils;

		int start, end = -1;
		if ((start = u.indexOf(originalText, '{')) > -1 && (end = u.indexOf(originalText, '}')) > -1 && end > start) {

			for (; start > -1 && end > -1
					&& end > start; start = u.indexOf(originalText, '{'), end = u.indexOf(originalText, '}')) {
				if (start > 0)
					message.append(originalText.substring(0, start));

				message.append(new JsonTag(originalText.substring(start + 1, end)).asJsonObject());

				originalText = originalText.substring(end + 1);
			}

			if (!originalText.isEmpty())
				message.append(originalText);
		} else
			message.append(originalText);

		if (path != null)
			messages.add(message);

		return message;
	}

	public boolean contains(String path) {
		return Main.ioUtils.indexOf(messages, path) > -1;
	}

	public JsonMessage get(String path) {
		return Main.ioUtils.get(messages, path);
	}
}

package me.juanco.lc.chat.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class JsonSegment {

	private JSONObject object = new JSONObject();
	private JsonBuilder builder;

	protected JsonSegment(JsonBuilder builder, String message) {
		object.put("text", message);
		this.builder = builder;
	}

	public JsonSegment insert(String value) {
		object.put("insertion", value);
		return this;
	}

	public JsonSegment clickEvent(JsonClickEvent action, String value) {
		JSONObject event = new JSONObject();
		event.put("action", action.name());
		event.put("value", value);

		if (object.containsKey("clickEvent"))
			object.replace("clickEvent", event);
		else
			object.put("clickEvent", event);
		return this;
	}

	public JsonSegment hoverAchievement(String achievement) {
		JSONObject event = new JSONObject();
		event.put("action", "show_achievement");
		event.put("value", achievement);

		if (object.containsKey("hoverEvent"))
			object.replace("hoverEvent", event);
		else
			object.put("hoverEvent", event);
		return this;
	}

	public JsonSegment hoverString(Object... values) {
		JSONObject event = new JSONObject();
		event.put("action", "show_text");

		if (values.length < 2)
			if (values.length < 1)
				event.put("value", "");
			else
				event.put("value", values[0]);
		else {
			JSONArray text = new JSONArray();
			text.add("");
			for (Object val : values)
				text.add(val);
			event.put("value", text);
		}

		if (object.containsKey("hoverEvent"))
			object.replace("hoverEvent", event);
		else
			object.put("hoverEvent", event);
		return this;
	}

	public JsonBuilder end() {
		builder.addSegment(object);
		return builder;
	}

	protected JSONObject get() {
		return object;
	}
}

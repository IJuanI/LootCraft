package me.juanco.lc.scoreboard;

import org.bukkit.scoreboard.Scoreboard;

import me.juanco.lc.scoreboard.sidebar.LootSidebar;

public class UsedScoreboard {

	private final Scoreboard scoreboard;
	public LootSidebar sidebar;

	public UsedScoreboard(Scoreboard scoreboard) {
		this.scoreboard = scoreboard;
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Scoreboard)
			return scoreboard.equals(obj);
		return super.equals(obj);
	}

}

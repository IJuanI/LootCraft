package me.juanco.lc.scoreboard.underName;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.leaderboard.LootLeaderboard;
import me.juanco.lc.api.leaderboard.LootLeaderboardAnim;
import me.juanco.lc.core.Main;
import me.juanco.lc.match.tops.LoseLeaderboard;
import me.juanco.lc.match.tops.WinLeaderboard;

public class UNameManager {

	private List<LootLeaderboard> leaderboards = new ArrayList<LootLeaderboard>();
	private ConcurrentSet<Player> shown = new ConcurrentSet<Player>();
	// private ConcurrentSet<Entry> shown = new ConcurrentSet<Entry>();
	private List<Scoreboard> scoreboards = new ArrayList<Scoreboard>();
	private int index = 0;
	private int animation = 0;

	public UNameManager(Plugin plugin) {
		new UNameUpdaterTask(this).runTaskTimer(plugin, 20, 8);

		registerLeaderboards();
	}

	private void registerLeaderboards() {
		leaderboards.add(new WinLeaderboard());
		leaderboards.add(new LoseLeaderboard());
	}

	public void flush() {
		if (leaderboards.isEmpty())
			return;

		animation++;

		if (animation >= leaderboards.get(index).getAnimationSize()) {
			animation = 0;
			index++;
			if (index >= leaderboards.size())
				index = 0;
		}

		updateAll();
	}

	public void updateAll() {
		String id = leaderboards.get(index).getAnimation(animation).getID();

		for (Scoreboard scoreboard : scoreboards)
			scoreboard.getObjective(id).setDisplaySlot(DisplaySlot.BELOW_NAME);

		for (Player player : shown) {
			int value = leaderboards.get(index).getValue(player);
			String name = player.getName();

			try {
				player.getScoreboard().getObjective(id).getScore(name).setScore(value);
			} catch (Exception ex) {

			}
		}

		// for (Entry entry : shown) {
		// entry.value = leaderboards.get(index).getValue(entry.player);
		// String name = entry.player.getName();
		//
		// for (Scoreboard scoreboard : scoreboards)
		// scoreboard.getObjective(id).getScore(name).setScore(entry.value);
		// }
	}

	public void register(LootLeaderboard leaderboard) {
		leaderboards.add(leaderboard);
	}

	public void show(Player player) {
		if (!scoreboards.contains(player.getScoreboard())) {
			Scoreboard scoreboard = player.getScoreboard();
			scoreboards.add(scoreboard);

			for (LootLeaderboard leaderboard : leaderboards)
				for (LootLeaderboardAnim anim : leaderboard.getAnimations()) {
					Objective obj = scoreboard.getObjective(anim.getID());
					if (obj == null)
						obj = scoreboard.registerNewObjective(anim.getID(), "dummy");

					obj.setDisplayName(anim.getDisplay());
				}
		}

		LootLeaderboard current = leaderboards.get(index);
		String id = current.getAnimation(animation).getID();
		String name = player.getName();
		int value = current.getValue(player);

		// for (Scoreboard scoreboard : scoreboards)
		// scoreboard.getObjective(id).getScore(name).setScore(value);

		player.getScoreboard().getObjective(id).getScore(name).setScore(value);
		player.getScoreboard().getObjective(id).setDisplaySlot(DisplaySlot.BELOW_NAME);

		shown.add(player);
		// shown.add(new Entry(player, value));
	}

	public void hide(Player player) {
		shown.remove(Main.ioUtils.get(shown, player));

		for (LootLeaderboard leaderboard : leaderboards)
			for (LootLeaderboardAnim anim : leaderboard.getAnimations())
				player.getScoreboard().getObjective(anim.getID()).unregister();

		scoreboards.remove(player.getScoreboard());

		player.getScoreboard().resetScores(player.getName());
	}

	@SuppressWarnings("unused")
	private static class Entry {
		private final Player player;
		private int value;

		private Entry(Player player, int value) {
			this.player = player;
			this.value = value;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Player)
				return player.equals(obj);
			return super.equals(obj);
		}
	}
}

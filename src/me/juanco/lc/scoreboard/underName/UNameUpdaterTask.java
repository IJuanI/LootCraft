package me.juanco.lc.scoreboard.underName;

import org.bukkit.scheduler.BukkitRunnable;

public class UNameUpdaterTask extends BukkitRunnable {

	private final UNameManager uName;

	public UNameUpdaterTask(UNameManager uName) {
		this.uName = uName;
	}

	@Override
	public void run() {
		uName.flush();
	}

}

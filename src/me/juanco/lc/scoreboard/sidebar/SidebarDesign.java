package me.juanco.lc.scoreboard.sidebar;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Team;

import me.juanco.lc.scoreboard.ScoreboardManager;

public class SidebarDesign {

	private final String title;
	private boolean built = false;

	private final ScoreboardManager manager;

	private List<DesignEntry> entries = new ArrayList<DesignEntry>();

	public SidebarDesign(ScoreboardManager manager, String title) {
		this.title = title;
		this.manager = manager;
	}

	public String getTitle() {
		return title;
	}

	public void addLine(String text, boolean usePlaceholder) {
		entries.add(new DesignEntry(manager, text, usePlaceholder));
		built = false;
	}

	public String getLine(int index) {
		return entries.get(index).text;
	}

	public boolean hasPlaceholder(int index) {
		return entries.get(index).usePlaceholder;
	}

	public Team getTeam(int index) {
		return entries.get(index).team;
	}

	public DesignEntry getEntry(int index) {
		return entries.get(index);
	}

	public int getSize() {
		return entries.size();
	}

	public boolean built() {
		return built;
	}

	public void build() {
		if (built)
			return;

		for (DesignEntry entry : entries)
			if (!entry.checked)
				entry.check();

		built = true;
	}

	public void disable() {
		for (DesignEntry entry : entries)
			if (entry.team != null)
				entry.team.unregister();
	}

	protected class DesignEntry {
		private final String text;
		private final boolean usePlaceholder;
		private Team team = null;
		private boolean checked = false;

		private final ScoreboardManager manager;

		private DesignEntry(ScoreboardManager manager, String text, boolean usePlaceholder) {
			this.text = text;
			this.usePlaceholder = usePlaceholder;

			this.manager = manager;
		}

		private void check() {
			if (!usePlaceholder) {
				team = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(manager.newTeam());
				team.setNameTagVisibility(NameTagVisibility.ALWAYS);

				if (text.length() > 32) {
					String prefix = text.substring(0, 16);

					while (prefix.endsWith("."))
						prefix = prefix.substring(0, prefix.length() - 1);

					team.setPrefix(prefix);
					team.setSuffix(text.substring(16, 32));
				} else if (text.length() > 16) {
					String prefix = text.substring(0, 16);

					while (prefix.endsWith("."))
						prefix = prefix.substring(0, prefix.length() - 1);

					team.setPrefix(prefix);
					team.setSuffix(text.substring(16));
				} else
					team.setPrefix(text);
			}
			checked = true;
		}

		public boolean usePlacholder() {
			return usePlaceholder;
		}

		public Team getTeam() {
			return team;
		}
	}
}

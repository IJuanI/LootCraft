package me.juanco.lc.scoreboard.sidebar;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.juanco.lc.core.Main;
import me.juanco.lc.scoreboard.ScoreboardManager;
import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class LootSidebar {

	private List<LootPlayerSidebar> shown = new ArrayList<LootPlayerSidebar>();

	protected List<LootPlaceholder> usedPlaceholders = new ArrayList<LootPlaceholder>();
	protected List<SidebarHoldableEntry> placeholderLines = new ArrayList<SidebarHoldableEntry>();

	private List<Team> usedTeams = new ArrayList<Team>();

	protected final SidebarDesign design;
	protected final ScoreboardManager manager;

	public LootSidebar(ScoreboardManager manager, PlaceholderManager placeholders, SidebarDesign design) {
		if (!design.built())
			design.build();

		for (int index = 0; index < design.getSize(); index++)
			if (design.hasPlaceholder(index))
				placeholderLines.add(new SidebarHoldableEntry(design.getLine(index), index));

		for (LootPlaceholder placeholder : placeholders.getPlaceholders())
			for (SidebarHoldableEntry entry : placeholderLines)
				if (entry.check(placeholder))
					if (!usedPlaceholders.contains(placeholder))
						usedPlaceholders.add(placeholder);

		this.design = design;
		this.manager = manager;

		placeholders.bindPlaceholders(usedPlaceholders, this);
	}

	protected SidebarHoldableEntry getPlaceholderLine(int index) {
		return Main.ioUtils.get(placeholderLines, (Object) index);
	}

	public void showPlayer(Player player) {
		LootPlayerSidebar playerSidebar = new LootPlayerSidebar(player, this);

		if (!shown.contains(playerSidebar))
			shown.add(playerSidebar);
	}

	public LootPlayerSidebar getPlayer(Player player) {
		return Main.ioUtils.get(shown, player);
	}

	public void hidePlayer(Player player) {
		int index = Main.ioUtils.indexOf(shown, player);

		if (index > -1)
			shown.remove(index).hide();
	}

	protected boolean isShown(LootPlayerSidebar playerSidebar) {
		return shown.contains(playerSidebar);
	}

	protected Team getNewTeam(Player player) {
		Scoreboard scoreboard = null;
		if (player != null)
			scoreboard = player.getScoreboard();
		if (scoreboard == null)
			scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();

		Team team = scoreboard.registerNewTeam(manager.newTeam());

		team.setNameTagVisibility(NameTagVisibility.ALWAYS);

		usedTeams.add(team);

		return team;
	}

	public void updatePlaceholder(LootPlaceholder placeholder, Player player) {
		if (usedPlaceholders.contains(placeholder))
			for (SidebarHoldableEntry entry : placeholderLines)
				if (entry.equals(placeholder))
					if (player == null)
						for (LootPlayerSidebar playerEntry : shown)
							playerEntry.update(entry);
					else {
						LootPlayerSidebar playerEntry = Main.ioUtils.get(shown, player);
						if (playerEntry != null)
							playerEntry.update(entry);
					}

	}

	public void disable() {
		for (LootPlayerSidebar player : shown)
			player.hide();

		for (Team team : usedTeams)
			team.unregister();

		usedTeams.clear();
		shown.clear();
	}

	public void removeTeam(Team team) {
		usedTeams.remove(team);
		team.unregister();
	}
}

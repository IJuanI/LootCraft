package me.juanco.lc.scoreboard.sidebar;

import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.scoreboard.ScoreboardManager;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class SidebarManager {

	private final ScoreboardManager manager;

	private final LootSidebar defaultSidebar;

	public SidebarManager(ScoreboardManager manager, PlaceholderManager placeholders) {
		this.manager = manager;

		SidebarDesign mainDesign = generateMainDesign();

		defaultSidebar = new LootSidebar(manager, placeholders, mainDesign);
	}

	private SidebarDesign generateMainDesign() {
		SidebarDesign design = new SidebarDesign(manager, t("&2&lLoot&7-&6&lCraft"));

		design.addLine(t("&7--------------&7-----"), false);
		design.addLine(t("&2&l> &a&lMoney:"), false);
		design.addLine(t("&f&l{money}"), true);
		design.addLine(t("&4&l> &c&lPvP To&c&lkens"), false);
		design.addLine(t("&f&l{tokens}"), true);
		design.addLine(t("&7--------------&7-----"), false);
		design.addLine(t("&3&l> &b&lItem L&b&level"), false);
		design.addLine(t("&f&l�{item_level}&f&l�"), true);
		design.addLine(t("&6&l> &e&lWin...&6&l/&e&lLoss"), false);
		design.addLine(t("&f&l{win}/{lose}"), true);
		design.addLine(t("&7--------------&7-----"), false);
		design.addLine(t("&5&l> &d&lPayrol&d&ll"), false);
		design.addLine(t("&lNext: &a&l${payroll_amount}"), true);
		design.addLine(t("&f&lLeft: &d&l{payroll_time}"), true);

		return design;
	}

	private String t(String from) {
		return Message.t(from);
	}

	public void addPlayer(Player player) {
		defaultSidebar.showPlayer(player);
	}

	public void removePlayer(Player player) {
		defaultSidebar.hidePlayer(player);
	}

	public LootPlayerSidebar getPlayer(Player player) {
		return defaultSidebar.getPlayer(player);
	}

	public void disable() {
		defaultSidebar.disable();
		defaultSidebar.design.disable();
	}
}

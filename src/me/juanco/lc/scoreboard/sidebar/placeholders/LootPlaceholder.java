package me.juanco.lc.scoreboard.sidebar.placeholders;

import org.bukkit.entity.Player;

public abstract class LootPlaceholder {

	private final String name;

	public LootPlaceholder(String name) {
		this.name = name;
	}

	public abstract String getValue(Player player);

	public String getName() {
		return name;
	}
}

package me.juanco.lc.scoreboard.sidebar.placeholders.impl;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.kits.KitPlayer;
import me.juanco.lc.kits.Kits;
import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;

public class ItemLevelPlaceholder extends LootPlaceholder {

	private final String[] levelColors;
	private final String loading = Message.t("&d&l0");

	private final char color = ChatColor.COLOR_CHAR;

	public ItemLevelPlaceholder() {
		super("item_level");

		levelColors = new String[] { get('f'), get('a'), get('b'), get('e'), get('c'), get('2'), get('3'), get('6'),
				get('4') };
	}

	private String get(char letter) {
		StringBuilder builder = new StringBuilder().append(color).append(letter).append(color).append('l');

		return builder.toString();
	}

	@Override
	public String getValue(Player player) {
		KitPlayer user = Kits.getUser(player);

		if (user != null) {
			int ref = user.getLevel() / 15;
			if (ref > levelColors.length)
				ref = levelColors.length - 1;
			else if (ref < 0)
				ref = 0;

			return levelColors[ref] + user.getLevel();
		}

		return loading;
	}
}

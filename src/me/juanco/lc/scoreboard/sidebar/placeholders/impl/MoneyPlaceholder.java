package me.juanco.lc.scoreboard.sidebar.placeholders.impl;

import org.bukkit.entity.Player;

import me.juanco.lc.api.LootProperties;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.economy.LateUpdateTask;
import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class MoneyPlaceholder extends LootPlaceholder {

	private final PlaceholderManager placeholders;

	public MoneyPlaceholder(PlaceholderManager placeholders) {
		super("money");
		this.placeholders = placeholders;
	}

	@Override
	public String getValue(Player player) {
		LootProperties props = UserFile.getFile(player.getUniqueId()).props;
		if (props.get("money").equals("-1"))
			LateUpdateTask.queue("money", player, placeholders);
		else
			return props.get("money");
		return "Loading";
	}

}

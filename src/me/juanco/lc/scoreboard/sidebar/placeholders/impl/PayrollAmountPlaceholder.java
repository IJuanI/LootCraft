package me.juanco.lc.scoreboard.sidebar.placeholders.impl;

import org.bukkit.entity.Player;

import me.juanco.lc.core.Main;
import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;

public class PayrollAmountPlaceholder extends LootPlaceholder {

	private final Main core;

	public PayrollAmountPlaceholder(Main core) {
		super("payroll_amount");
		this.core = core;
	}

	@Override
	public String getValue(Player player) {
		return String.valueOf(core.getPayrolls().nextPayout(player));
	}

}

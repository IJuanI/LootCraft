package me.juanco.lc.scoreboard.sidebar.placeholders.impl;

import org.bukkit.entity.Player;

import me.juanco.lc.core.Main;
import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;

public class PayrollTimePlaceholder extends LootPlaceholder {

	private final Main core;

	public PayrollTimePlaceholder(Main core) {
		super("payroll_time");

		this.core = core;
	}

	@Override
	public String getValue(Player player) {
		return String.valueOf(core.getPayrolls().quotasLeft(player));
	}

}

package me.juanco.lc.scoreboard.sidebar.placeholders.impl;

import org.bukkit.entity.Player;

import me.juanco.lc.api.LootProperties;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;

public class LosePlaceholder extends LootPlaceholder {

	public LosePlaceholder() {
		super("lose");
	}

	@Override
	public String getValue(Player player) {
		LootProperties props = UserFile.getFile(player.getUniqueId()).props;
		return props.get("lose").equals("-1") ? "Loading" : props.get("lose");
	}
}

package me.juanco.lc.scoreboard.sidebar.placeholders.impl;

import org.bukkit.entity.Player;

import me.juanco.lc.api.LootProperties;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.economy.LateUpdateTask;
import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;

public class TokensPlaceholder extends LootPlaceholder {

	private final PlaceholderManager placeholders;

	public TokensPlaceholder(PlaceholderManager placeholders) {
		super("tokens");
		this.placeholders = placeholders;
	}

	@Override
	public String getValue(Player player) {
		LootProperties props = UserFile.getFile(player.getUniqueId()).props;
		if (props.get("tokens").equals("-1"))
			LateUpdateTask.queue("tokens", player, placeholders);
		else
			return props.get("tokens");
		return "Loading";
	}

}

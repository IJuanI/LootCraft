package me.juanco.lc.scoreboard.sidebar.placeholders;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.juanco.lc.core.Main;
import me.juanco.lc.scoreboard.sidebar.LootSidebar;

public class PlaceholderManager {

	private List<PlaceholderEntry> entries = new ArrayList<PlaceholderEntry>();

	public List<LootPlaceholder> getPlaceholders() {
		List<LootPlaceholder> list = new ArrayList<LootPlaceholder>();

		for (PlaceholderEntry entry : entries)
			list.add(entry.getPlaceholder());

		return list;
	}

	public void addPlaceholder(LootPlaceholder placeholder) {
		entries.add(new PlaceholderEntry(placeholder));
	}

	public void bindPlaceholders(List<LootPlaceholder> used, LootSidebar sidebar) {
		for (LootPlaceholder placeholder : used) {
			PlaceholderEntry entry = Main.ioUtils.get(entries, placeholder);
			if (entry != null)
				entry.addSidebar(sidebar);
		}
	}

	public void callPlaceholder(String placeholderName) {
		callPlaceholder(Main.ioUtils.get(entries, placeholderName), null);
	}

	public void callPlaceholder(String placeholderName, Player player) {
		callPlaceholder(Main.ioUtils.get(entries, placeholderName), player);
	}

	public void callPlaceholder(PlaceholderEntry placeholder) {
		callPlaceholder(placeholder, null);
	}

	public void callPlaceholder(PlaceholderEntry placeholder, Player player) {
		if (placeholder == null)
			return;

		for (LootSidebar sidebar : placeholder.getSidebars())
			sidebar.updatePlaceholder(placeholder.getPlaceholder(), player);
	}
}
package me.juanco.lc.scoreboard.sidebar.placeholders;

import java.util.ArrayList;
import java.util.List;

import me.juanco.lc.scoreboard.sidebar.LootSidebar;

public class PlaceholderEntry {

	private final LootPlaceholder placeholder;
	private List<LootSidebar> sidebars = new ArrayList<LootSidebar>();

	protected PlaceholderEntry(LootPlaceholder placeholder) {
		this.placeholder = placeholder;
	}

	public void addSidebar(LootSidebar sidebar) {
		sidebars.add(sidebar);
	}

	public void removeSidebar(LootSidebar sidebar) {
		sidebars.remove(sidebar);
	}

	public boolean hasSidebar(LootSidebar sidebar) {
		return sidebars.contains(sidebar);
	}

	protected LootPlaceholder getPlaceholder() {
		return placeholder;
	}

	protected List<LootSidebar> getSidebars() {
		return sidebars;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LootPlaceholder)
			return placeholder.equals(obj);
		else if (obj instanceof String)
			return placeholder.getName().equals(obj);
		return super.equals(obj);
	}
}

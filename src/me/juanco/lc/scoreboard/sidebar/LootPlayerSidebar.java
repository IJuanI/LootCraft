package me.juanco.lc.scoreboard.sidebar;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.juanco.lc.scoreboard.UsedScoreboard;
import me.juanco.lc.scoreboard.sidebar.SidebarDesign.DesignEntry;

public class LootPlayerSidebar {

	private List<PlayerSidebarEntry> entries = new ArrayList<PlayerSidebarEntry>();

	private final Player player;
	private final LootSidebar sidebar;
	private Scoreboard scoreboard;
	private Objective objective;

	private final String pathName;

	public LootPlayerSidebar(Player player, LootSidebar sidebar) {
		Scoreboard scoreboard = player.getScoreboard();
		UsedScoreboard used = sidebar.manager.getUsedScorboard(scoreboard);

		String name = player.getName();
		if (name.length() > 14)
			name = name.substring(0, 14);

		pathName = "ls" + name;

		this.player = player;
		this.sidebar = sidebar;
		this.scoreboard = scoreboard;

		if (used == null)
			used = sidebar.manager.useScoreboard(scoreboard);

		if (used.sidebar == null)
			used.sidebar = sidebar;
		else if (sidebar.isShown(this))
			updateAll();
		else
			show();
		// else {
		// used = sidebar.manager.asignNewScoreboard(player);
		// scoreboard = used.getScoreboard();
		// used.sidebar = sidebar;

		if (sidebar.isShown(this))
			updateAll();
		else
			show();
	}

	private void show() {
		objective = scoreboard.getObjective(pathName);

		if (objective == null || entries.isEmpty()) {
			if (objective == null)
				objective = scoreboard.registerNewObjective(pathName, "dummy");

			objective.setDisplaySlot(DisplaySlot.SIDEBAR);
			objective.setDisplayName(sidebar.design.getTitle());

			int size = sidebar.design.getSize();

			if (!sidebar.design.built())
				sidebar.design.build();

			for (int index = 0; index < size; index++) {
				DesignEntry designEntry = sidebar.design.getEntry(index);
				Team team;

				if (designEntry.usePlacholder()) {
					team = sidebar.getNewTeam(player);
					String text = sidebar.getPlaceholderLine(index).getText(player);
					if (text.length() <= 16)
						team.setPrefix(text);
					else {
						String prefix = text.substring(0, 16);

						while (prefix.endsWith("."))
							prefix = prefix.substring(0, prefix.length() - 1);

						if (text.length() > 32)
							team.setSuffix(text.substring(16, 32));
						else
							team.setSuffix(text.substring(16));
					}
				} else {
					team = designEntry.getTeam();
					if (!team.getScoreboard().equals(scoreboard)) {
						Team t = scoreboard.getTeam(team.getName());
						if (t == null)
							t = scoreboard.registerNewTeam(team.getName());

						t.setNameTagVisibility(NameTagVisibility.ALWAYS);
						t.setPrefix(team.getPrefix());
						if (team.getSuffix() != null)
							t.setSuffix(team.getSuffix());

						if (!team.getEntries().isEmpty())
						t.addEntry(team.getEntries().iterator().next());

						team = t;
					}
				}

				PlayerSidebarEntry entry = new PlayerSidebarEntry(team, sidebar.manager.nextEntryID(),
						designEntry.usePlacholder());

				objective.getScore(entry.entryID).setScore(size - index - 1);

				entries.add(entry);
			}

		} else
			updateAll();
	}

	private void updateAll() {
		if (objective == null)
			show();
		else
			for (SidebarHoldableEntry entry : sidebar.placeholderLines) {
				String text = entry.getText(player);
				Team team = entries.get(entry.getIndex()).team;
				if (text.length() <= 16) {
					team.setPrefix(text);
					team.setSuffix("");
				} else {
					int a = 16;

					while (text.charAt(a - 2) == ChatColor.COLOR_CHAR)
						a -= 2;

					team.setPrefix(text.substring(0, a));
					if (text.length() <= a + 16)
						team.setSuffix(text.substring(a));
					else
						team.setSuffix(text.substring(a, a + 16));
				}
			}
	}

	protected void update(SidebarHoldableEntry entry) {
		if (objective == null)
			show();
		else {
			String text = entry.getText(player);
			Team team = entries.get(entry.getIndex()).team;
			if (text.length() <= 16) {
				team.setPrefix(text);
				team.setSuffix("");
			} else {
				int a = 16;

				while (text.charAt(a - 2) == ChatColor.COLOR_CHAR)
					a -= 2;

				team.setPrefix(text.substring(0, a));
				if (text.length() <= a + 16)
					team.setSuffix(text.substring(a));
				else
					team.setSuffix(text.substring(a, a + 16));
			}
		}
	}

	public void refresh() {
		hide();
		show();
	}

	protected void hide() {
		for (PlayerSidebarEntry entry : entries) {
			if (entry.team != null)
				entry.team.removeEntry(entry.entryID);
			objective.getScoreboard().resetScores(entry.entryID);
		}

		objective.unregister();

		entries.clear();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		return super.equals(obj);
	}

	private class PlayerSidebarEntry {

		private final Team team;
		private final String entryID;
		// private final boolean placeholder;

		private PlayerSidebarEntry(Team team, String entryID, boolean placeholder) {
			this.team = team;
			this.entryID = entryID;

			if (team == null || entryID == null) {
				System.out.println("Team: " + team);
				System.out.println("Entry: " + entryID);
			} else
				team.addEntry(entryID);
			// this.placeholder = placeholder;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Team)
				return team.equals(team);
			if (obj instanceof String)
				return entryID.equals(obj);
			return super.equals(obj);
		}
	}
}

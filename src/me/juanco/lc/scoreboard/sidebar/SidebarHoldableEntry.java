package me.juanco.lc.scoreboard.sidebar;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.entity.Player;

import me.juanco.lc.scoreboard.sidebar.placeholders.LootPlaceholder;

public class SidebarHoldableEntry {

	private final String text;
	private final int index;

	private List<String> holderNames = new ArrayList<String>();
	private List<LootPlaceholder> placeholders = new ArrayList<LootPlaceholder>();
	private List<int[]> holdersData = new ArrayList<int[]>();

	protected SidebarHoldableEntry(String text, int index) {
		this.text = text;
		this.index = index;

		String temp = text;
		int toAdd = 0;
		while (true) {
			int index1 = temp.indexOf('{');
			int index2 = temp.indexOf('}', index1);

			if (index1 < 0 || index2 < 0)
				break;

			holderNames.add(temp.substring(index1 + 1, index2));
			holdersData.add(new int[] { index1 + toAdd, index2 + toAdd });
			temp = temp.substring(index2 + 1);
			toAdd += index2 + 1;
		}
	}

	public boolean check(LootPlaceholder placeholder) {
		if (holderNames.contains(placeholder.getName()))
			placeholders.add(placeholder);
		else
			return false;
		return true;
	}

	public String getText(Player player) {
		String parsed = text;


		for (LootPlaceholder placeholder : placeholders) {
			String value = placeholder.getValue(player);
			for (Integer index : getIndexes(placeholder.getName())) {
				int[] data = holdersData.get(index);

				StringBuilder builder = new StringBuilder(parsed.substring(0, data[0]));
				builder.append(value);

				String end = parsed.substring(data[1] + 1);
				int i = 0;
				int j = 0;

				while (end.startsWith("0")) {
					if (i < value.length())
						end.substring(1);
					else {
						end = end.substring(1);
						j++;
					}
					i++;
				}

				if (j > 0)
					while (j >= 0) {
						builder.append(' ');
						j--;
					}

				builder.append(end);

				parsed = builder.toString();
			}
		}

		return parsed;
	}

	private List<Integer> getIndexes(String holder) {
		List<Integer> list = new ArrayList<Integer>();

		int index = 0;
		Iterator<String> iter = holderNames.iterator();

		while (iter.hasNext()) {
			if (iter.next().equals(holder))
				list.add(index);
			index++;
		}

		return list;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return text.equals(obj);
		else if (obj instanceof LootPlaceholder)
			return placeholders.contains(obj);
		else if (obj instanceof Integer)
			return obj.equals(index);
		return super.equals(obj);
	}
}

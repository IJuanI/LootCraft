package me.juanco.lc.scoreboard;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.core.Main;
import me.juanco.lc.scoreboard.sidebar.SidebarManager;
import me.juanco.lc.scoreboard.sidebar.placeholders.PlaceholderManager;
import me.juanco.lc.scoreboard.underName.UNameManager;

public class ScoreboardManager {

	private static int teamId = 0;
	private static int entryId = 0;

	private static final char colorChar = ChatColor.COLOR_CHAR;
	private static final char[] colorCodes = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'k', 'l', 'm', 'n', 'o', 'r' };

	private List<UsedScoreboard> usedScoreboards = new ArrayList<UsedScoreboard>();

	private final SidebarManager sidebar;
	private final TagManager tags;
	private final UNameManager uName;

	public ScoreboardManager(Main core, PlaceholderManager placeholders) {
		sidebar = new SidebarManager(this, placeholders);
		tags = new TagManager(core.getFormatter());
		uName = new UNameManager(core);
	}

	public void addSidebar(Player player) {
		sidebar.addPlayer(player);
	}

	public void addPlayer(Player player, boolean delay) {
		asignNewScoreboard(player);

		tags.addTag(player);
		if (delay)
			LootClock.addTask(new DelayedSidebarTask(this, player));
		else
			sidebar.addPlayer(player);
	}

	public void removePlayer(Player player) {
		sidebar.removePlayer(player);
		uName.hide(player);
	}

	public void disable() {
		sidebar.disable();
	}

	public UsedScoreboard useScoreboard(Scoreboard scoreboard) {
		UsedScoreboard used = new UsedScoreboard(scoreboard);
		usedScoreboards.add(used);

		return used;
	}

	public boolean isScoreboardUsed(Scoreboard scoreboard) {
		return Main.ioUtils.indexOf(usedScoreboards, scoreboard) > -1;
	}

	public UsedScoreboard getUsedScorboard(Scoreboard scoreboard) {
		return Main.ioUtils.get(usedScoreboards, scoreboard);
	}

	public UsedScoreboard asignNewScoreboard(Player player) {
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		UsedScoreboard used = new UsedScoreboard(scoreboard);

		player.setScoreboard(scoreboard);
		usedScoreboards.add(used);
		uName.show(player);
		return used;
	}

	public String newTeam() {
		String name = "l.sb." + teamId++;

		Team team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam(name);

		if (team != null)
			team.unregister();

		return name;
	}

	public String nextEntryID() {
		StringBuilder sb = new StringBuilder();
		int index = entryId;
		boolean lock = true;

		while (lock || index >= 22) {
			int j = index;

			if (index >= 22) {
				j = index % 22;
				index /= 22;
				lock = true;
			} else if (lock)
				lock = false;

			sb.append(colorChar).append(colorCodes[j]);

		}

		entryId++;

		return sb.toString();
	}

	private class DelayedSidebarTask extends LootTask {

		private final ScoreboardManager scoreboardManager;
		private final Player player;

		public DelayedSidebarTask(ScoreboardManager scoreboardManager, Player player) {
			super(1, false);
			this.scoreboardManager = scoreboardManager;
			this.player = player;
		}

		@Override
		public void run() {
			if (player != null && player.isOnline())
				scoreboardManager.addSidebar(player);
		}

	}
}

package me.juanco.lc.scoreboard;

public class LootTeamedPlayer {

	private final String playerName;
	private String teamName;

	public LootTeamedPlayer(String playerName, String teamName) {
		this.playerName = playerName;
		this.teamName = teamName;
	}

	public void setTeam(String teamName) {
		this.teamName = teamName;
	}

	public boolean matches(String team) {
		return team.equals(teamName);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String)
			return playerName.equals(obj);
		return super.equals(obj);
	}
}

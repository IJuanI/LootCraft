package me.juanco.lc.scoreboard;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.juanco.lc.api.format.LootFormatter;

public class TagManager {

	private final LootFormatter formatter;

	protected TagManager(LootFormatter formatter) {
		this.formatter = formatter;
	}

	public void addTag(Player player) {
		String name = player.getName();

		if (name.length() > 11)
			name = name.substring(0, 11);

		boolean created = false;

		Scoreboard score = player.getScoreboard();
		Team team = score.getEntryTeam(name);

		if (team == null) {
			team = score.registerNewTeam("loot." + name);
			team.addEntry(name);
			created = true;
		}

		if (!team.getName().startsWith("loot.") || created) {
			team.setNameTagVisibility(NameTagVisibility.ALWAYS);

			team.setPrefix(formatter.getPrefix(player));
			team.setSuffix(formatter.getSuffix(player));
		} else
			updateTag(player);
	}

	public void updateTag(Player player) {
		Team team = player.getScoreboard().getEntryTeam(player.getName());

		if (team == null)
			addTag(player);
		else {
			team.setPrefix(formatter.getPrefix(player));
			team.setSuffix(formatter.getSuffix(player));
		}
	}
}

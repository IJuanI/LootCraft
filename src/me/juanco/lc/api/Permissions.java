package me.juanco.lc.api;

public class Permissions {

	public static final String crate = "crate.use";
	public static final String payroll = "payroll.use";
	public static final String payroll_others = "payroll.others";
	public static final String admin = "loot.admin";
	public static final String legend = "vip.legend";
	public static final String kit = "kit.use";
	public static final String helper = "loot.helper";
}

package me.juanco.lc.api.leaderboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.core.Main;
import me.juanco.lc.files.LootFile;

public abstract class LootLeaderboard {

	private final LootFile file;
	private final String path;

	private int idIndex = 0;
	private final String id;
	private static final char[] idList = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C',
			'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z' };

	private List<Entry> leaderboard = new ArrayList<Entry>();
	private Set<String> queue = new ConcurrentSet<String>();
	private List<LootLeaderboardAnim> animations = new ArrayList<LootLeaderboardAnim>();
	private List<String> alternative = new ArrayList<String>();
	private int index = 0;

	public LootLeaderboard(LootFile file, String path, String id) {
		this.file = file;
		this.path = path;
		this.id = id;

		if (!file.file.contains(path + ".data") || !file.file.isList(path + ".data"))
			file.file.set(path + ".data", new ArrayList<String>());

		int pos = 1;

		for (String data : file.file.getStringList(path + ".data")) {
			leaderboard.add(Entry.read(data, pos));
			pos++;
		}

		for (Entry entry : leaderboard)
			alternative.add(entry.player);

		if (file.file.contains(path + ".index"))
			index = file.file.getInt(path + ".index");

		if (file.file.contains(path + ".alternative") && file.file.isList(path + ".alternative"))
			alternative = file.file.getStringList(path + ".alternative");

		registerAnimations();

		LeaderboardUpdaterTask.register(this);
	}

	public void flush() {
		int actions = 0;
		int checks = 0;

		while (!queue.isEmpty()) {
			String player = queue.iterator().next();
			queue.remove(player);

			update(player, getRealValue(player));
			alternative.add(player);

			actions++;

			if (actions >= 8)
				break;
		}

		if (alternative.isEmpty() || leaderboard.isEmpty())
			return;

		while (actions < 8 && checks < 40) {
			String player = alternative.get(index);
			if (update(player, getRealValue(player))) {
				actions++;

				index++;
				if (index >= alternative.size()) {
					for (Entry entry : leaderboard)
						alternative.add(entry.player);
					index = 0;
					actions = 8;
				}
			}

			checks++;
		}
	}

	public int getValue(int position) {
		Entry entry = leaderboard.get(position);
		return entry == null ? -1 : entry.value;
	}

	public int getPosition(String player) {
		return Main.ioUtils.indexOf(leaderboard, player) + 1;
	}

	public int getValue(String player) {
		Entry entry = Main.ioUtils.get(leaderboard, player);
		if (entry != null)
			return entry.value;
		return 0;
	}

	public int getValue(Player player) {
		return getPosition(player.getName());
	}

	public void add(Player player) {
		queue.add(player.getName());
	}

	public boolean contains(Player player) {
		return getPosition(player.getName()) > -1;
	}

	public boolean update(String player, int value) {
		int pos = leaderboard.size()+1;
		boolean first = true;
		Entry entry = Main.ioUtils.get(leaderboard, player);

		if (entry != null) {
			if (value != entry.value) {
				pos = entry.position;
				first = false;
				if (value > entry.value && getValue(pos + 1) < value
						|| value < entry.value && getValue(pos - 1) > value) {
					entry.position = -1;
					leaderboard.remove(entry);
				}
			}
		} else
			entry = new Entry(player, value, -1);

		if (entry.position > -1) {
			if (entry.value != value)
				entry.value = value;
			return false;
		} else if (first && getValue(pos + 1) > value && getValue(pos - 1) < value) {
			entry.position = pos;
			leaderboard.add(entry);
		} else {
			int range = 1000;

			if (leaderboard.size() < 10)
				range = 1;
			else if (leaderboard.size() < 100)
				range = 10;
			else if (leaderboard.size() < 1000)
				range = 100;

			int cVal = getValue(pos);
			if (cVal > -1)
				if (cVal < value) {
					if (getValue(pos + 10) > value)
						range = 1;
					else if (getValue(pos + 100) > value)
						range = 10;
					else if (getValue(pos + 1000) > value)
						range = 100;
				} else if ((cVal = getValue(pos - 10)) < value && cVal > -1)
					range = 1;
				else if ((cVal = getValue(pos - 100)) < value && cVal > -1)
					range = 10;
				else if ((cVal = getValue(pos - 1000)) < value && cVal > -1)
					range = 100;

			range *= 10;
			while (range > 1)
				pos = getPosition(pos, value, range /= 10);

			pos = getPosition(pos, value, 1);

			entry.position = pos;
			leaderboard.add(pos, entry);
		}

		return true;
	}

	private int getPosition(int originalPos, int value, int range) {
		int mode = 0;

		while (true) {
			int cVal = getValue(originalPos);

			if (cVal < value) {
				if (mode == 2 || cVal < 0)
					break;
				else if (mode == 0)
					mode = 1;

				originalPos += range;
			} else if (cVal > value) {
				if (mode == 1)
					break;
				else if (mode == 0)
					mode = 2;

				originalPos -= range;
			} else
				break;
		}

		return originalPos;
	}

	public void save() {
		List<String> data = new ArrayList<String>();
		for (Entry entry : leaderboard)
			data.add(entry.encode());

		file.file.set(path + ".data", data);
		file.file.set(path + ".index", index);
		file.file.set(path + ".alternative", alternative);
		file.save();
	}


	public int getAnimationSize() {
		return animations.size();
	}

	public LootLeaderboardAnim getAnimation(int index) {
		return animations.get(index);
	}

	public List<LootLeaderboardAnim> getAnimations() {
		return animations;
	}

	protected void addAnimation(String display) {
		animations.add(new LootLeaderboardAnim(id + idList[idIndex], display));
		idIndex++;
	}

	protected abstract void registerAnimations();

	protected abstract int getRealValue(String player);

	private static class Entry {

		private final String player;
		private int value;
		private int position;

		private Entry(String player, int value, int position) {
			this.player = player;
			this.value = value;
			this.position = position;
		}

		private String encode() {
			StringBuilder builder = new StringBuilder(player);
			builder.append(' ').append(value);
			return builder.toString();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof String)
				return obj.equals(player);
			return super.equals(obj);
		}

		private static Entry read(String data, int position) {
			String[] info = data.split(" ");
			return new Entry(info[0], Integer.valueOf(info[1]), position);
		}
	}
}

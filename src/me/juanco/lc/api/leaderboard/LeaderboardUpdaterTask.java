package me.juanco.lc.api.leaderboard;

import java.util.ArrayList;
import java.util.List;

import me.juanco.lc.api.clock.LootClock;
import me.juanco.lc.api.clock.LootTask;

public class LeaderboardUpdaterTask extends LootTask {

	private static List<LootLeaderboard> leaderboards = new ArrayList<LootLeaderboard>();

	public LeaderboardUpdaterTask() {
		super(2, true, true);
		LootClock.addTask(this);
	}

	@Override
	public void run() {
		for (LootLeaderboard leaderboard : leaderboards)
			leaderboard.flush();
	}

	public static void register(LootLeaderboard leaderboard) {
		leaderboards.add(leaderboard);
	}
}

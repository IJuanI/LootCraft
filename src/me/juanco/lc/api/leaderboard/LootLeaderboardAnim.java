package me.juanco.lc.api.leaderboard;

import me.juanco.lc.chat.Message;

public class LootLeaderboardAnim {

	private final String id;
	private final String display;

	public LootLeaderboardAnim(String id, String display) {
		this.id = id;
		this.display = Message.t(display);
	}

	public String getID() {
		return id;
	}

	public String getDisplay() {
		return display;
	}
}

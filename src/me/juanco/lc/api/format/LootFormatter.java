package me.juanco.lc.api.format;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.juanco.lc.api.Permissions;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;

public class LootFormatter {

	private final Main core;

	private final String sep, owner, admin, mod, legend, ownerPref;

	private final String basePref = Message.t("&7[&b&l%s&7]");
	private final String baseSuff = Message.t("&7[&e&l%s&7]");

	public LootFormatter(Main core) {
		this.core = core;
		sep = Message.t(" &7: ");
		owner = Message.t("&7[&a&lO&b&lW&c&lN&d&lE&e&lR&7]");
		ownerPref = Message.t("&b&lOWNER &a");
		admin = Message.t("&7[&6&lAdmin&7]");
		mod = Message.t("&7[&5&lMod&7]");
		legend = Message.t("&7[&2&lLegend&7]");
	}

	public String normalFormat(Player player) {
		String prefix;
		String suffix;
		String name = Message.t(String.format(" &a%s ", player.getDisplayName()));
		char color;

		if (player.isOp())
			prefix = owner;
		else if (player.hasPermission(Permissions.admin))
			prefix = admin;
		else if (player.hasPermission(Permissions.helper))
			prefix = mod;
		else if (player.hasPermission(Permissions.legend))
			prefix = legend;
		else {
			int level = core.getLevelManager().getTotalLevel(player);
			prefix = String.format(basePref, level);
		}

		UserFile user = UserFile.getFile(player.getUniqueId());
		if (user.file.contains("Chat Color"))
			color = user.file.getString("Chat Color").charAt(0);
		else
			color = 'f';

		suffix = String.format(baseSuff, (String) user.props.get("win"));

		StringBuilder result = new StringBuilder(prefix);
		result.append(name);
		result.append(suffix);
		result.append(sep);
		result.append(ChatColor.COLOR_CHAR).append(color);

		return result.toString();
	}

	public String tabFormat(Player player) {
		String normal = normalFormat(player);
		normal = normal.substring(0, normal.length() - 10);

		int first = normal.indexOf(' ');
		int last = normal.lastIndexOf(' ') + 1;

		String prefix = normal.substring(3, first - 3);
		String name = normal.substring(first, last);
		String suffix = normal.substring(last + 3);

		StringBuilder result = new StringBuilder(prefix);
		result.append(name);
		result.append(suffix);

		return result.toString();
	}

	public String getPrefix(Player player) {
		String prefix;

		if (player.isOp())
			return ownerPref;
		else if (player.hasPermission(Permissions.admin))
			prefix = admin;
		else if (player.hasPermission(Permissions.helper))
			prefix = mod;
		else if (player.hasPermission(Permissions.legend))
			prefix = legend;
		else {
			int level = core.getLevelManager().getTotalLevel(player);
			prefix = String.format(basePref, level);
		}

		StringBuilder result = new StringBuilder(prefix);
		result.append(Message.t(" &a"));

		return result.toString();
	}

	public String getSuffix(Player player) {
		String suffix;

		suffix = String.format(baseSuff, (String) UserFile.getFile(player.getUniqueId()).props.get("win"));

		StringBuilder result = new StringBuilder(" ").append(suffix);

		return result.toString();

	}
}

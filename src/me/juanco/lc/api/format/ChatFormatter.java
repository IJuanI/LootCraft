package me.juanco.lc.api.format;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.juanco.lc.api.LootListener;

public class ChatFormatter extends LootListener {

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		e.setFormat(getMain().getFormatter().normalFormat(e.getPlayer()) + e.getMessage());
	}
}

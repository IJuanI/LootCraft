package me.juanco.lc.api.inventory;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public abstract class LootPurchaser {

	private final List<Player> locked = new ArrayList<Player>();

	protected void lock(Player player) {
		locked.add(player);
	}

	protected void closeInv(Player player, byte[] data) {
		if (!locked.contains(player))
			close(player, data);
		locked.remove(player);
	}

	public abstract void accept(Player player, int cost, byte[] data);

	public abstract void deny(Player player, int cost, byte[] data);

	public abstract void close(Player player, byte[] data);
}

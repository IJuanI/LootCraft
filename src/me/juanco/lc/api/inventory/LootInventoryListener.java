package me.juanco.lc.api.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.juanco.lc.api.LootListener;

public class LootInventoryListener extends LootListener {

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (!e.getEventName().equals("InventoryClickEvent") || e.getClickedInventory() == null
				|| !e.getClickedInventory().equals(e.getView().getTopInventory()))
			return;
		LootViewer view = LootInventory.getViewer((Player) e.getWhoClicked());
		if (view != null && view.inv != null)
			e.setCancelled(view.inv.onInventoryClick(e));
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		LootViewer view = LootInventory.getViewer((Player) e.getPlayer());
		if (view != null && view.inv != null)
			view.inv.onInventoryClose(e);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		LootInventory.removeViewer(e.getPlayer());
	}
}

package me.juanco.lc.api.inventory;

import org.bukkit.entity.Player;

public class LootViewer {

	private Player player;
	LootInventory inv;
	private Object data = null;

	protected LootViewer(Player viewer, LootInventory inv) {
		player = viewer;
		this.inv = inv;
	}

	protected void setInv(LootInventory inventory) {
		if (inv != null && player.getOpenInventory() != null)
			inv.close(player);
		inv = inventory;
	}

	protected void forceSetInv(LootInventory inventory) {
		inv = inventory;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@SuppressWarnings("unchecked")
	public <T> T getData() {
		return (T) data;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return player.equals(obj);
		else if (obj instanceof LootInventory)
			return inv.equals(obj);
		return super.equals(obj);
	}
}

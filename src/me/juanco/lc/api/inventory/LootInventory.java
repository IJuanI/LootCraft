package me.juanco.lc.api.inventory;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import me.juanco.lc.core.Main;

public abstract class LootInventory {

	private static List<LootViewer> viewers = new ArrayList<LootViewer>();

	protected boolean hasDebug = false;

	public void open(Player player) {
		preOpenInventory(player);

		int i = Main.ioUtils.indexOf(viewers, player);
		if (i > -1)
			viewers.get(i).setInv(this);
		else
			viewers.add(new LootViewer(player, this));

		openInventory(player);
	}

	public void close(Player player) {
		closeInventory(player);
	}

	public <T> T getData(HumanEntity player) {
		LootViewer viewer = getViewer(player);
		if (viewer != null)
			try {
				return viewer.getData();
			} catch (Exception ex) {
				onException(ex);
				return null;
			}
		return null;
	}

	protected final void onException(Exception ex) {
		if (hasDebug) {
			System.out.println(" ");
			System.out.println(this.getClass().getSimpleName() + " Debug (LootInventory)");

			debug(ex);

			System.out.println("======== End of Debug ========");
		}
	}

	public void setData(HumanEntity player, Object... data) {
		LootViewer viewer = getViewer(player);
		if (viewer != null)
			viewer.setData(data);
	}

	protected boolean onInventoryClick(InventoryClickEvent e) {
		if (e.getClickedInventory().equals(e.getView().getTopInventory()))
			if (e.getCurrentItem() != null)
				if (!e.getCurrentItem().getType().equals(Material.AIR))
					return inventoryClick(e);
		return false;
	}

	protected void onInventoryClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		int i = Main.ioUtils.indexOf(viewers, p);
		if (i > -1 && viewers.get(i).inv.equals(this))
			viewers.get(i).forceSetInv(null);

		inventoryClose(e);
	}

	protected void debug(Exception ex) {

	}

	protected abstract void preOpenInventory(Player player);

	protected abstract void openInventory(Player player);

	protected abstract void closeInventory(Player player);

	protected abstract boolean inventoryClick(InventoryClickEvent e);

	protected abstract void inventoryClose(InventoryCloseEvent e);

	protected static LootViewer getViewer(HumanEntity player) {
		return Main.ioUtils.get(viewers, player);
	}

	protected static void removeViewer(Player player) {
		int index = Main.ioUtils.indexOf(viewers, player);
		if (index > -1)
			viewers.remove(index);
	}
}

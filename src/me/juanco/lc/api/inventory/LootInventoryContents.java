package me.juanco.lc.api.inventory;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.core.Main;

public class LootInventoryContents {

	private ItemStack[] contents;

	public LootInventoryContents(int size) {
		Validate.isTrue(size % 9 == 0, "Chests must have a size that is a multiple of 9!");
		Validate.isTrue(size != 0, "Chests size should be major than 0!");
		contents = new ItemStack[size];
	}

	public LootInventoryContents(ItemStack[] contents) {
		setContents(contents);
	}

	public boolean add(ItemStack item) {
		for (int i = 0; i < contents.length; i++)
			if (contents[i] == null) {
				contents[i] = item;
				return true;
			}

		return false;
	}

	public void setItem(int slot, ItemStack item) {
		if (slot < 0)
			slot = 0;
		else if (slot >= contents.length)
			slot = contents.length - 1;

		contents[slot] = item;
	}

	public ItemStack getItem(int slot) {
		return contents[slot].clone();
	}

	public void multiSetItem(ItemStack item, int... slots) {
		for (int slot : slots)
			setItem(slot, item);
	}

	public void setContents(ItemStack[] contents) {
		Validate.notNull(contents, "LootInventory's Contents cannot be null!");
		Validate.isTrue(contents.length % 9 == 0, "Chests must have a size that is a multiple of 9!");
		this.contents = contents;
	}

	public ItemStack[] getContents() {
		return contents;
	}

	public Inventory getInventory(String name) {
		Inventory inv = Bukkit.createInventory(null, contents.length, name);
		inv.setContents(contents);
		return inv;
	}

	public void insertInto(LootInventoryContents target) {
		for (int slot = 0; slot < contents.length; slot++) {
			ItemStack item = contents[slot];
			if (Main.itemUtils.validate(item))
				target.setItem(slot, item);
		}
	}
}

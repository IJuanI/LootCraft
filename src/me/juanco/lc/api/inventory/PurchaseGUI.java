package me.juanco.lc.api.inventory;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.juanco.lc.api.LootProperties;
import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;

public class PurchaseGUI extends LootInventory {

	private static final String name = Message.t("&2&lConfirm Purchase");

	public static final ItemStack accept = Main.itemUtils
			.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 13), "&a&lAccept");
	public static final ItemStack deny = Main.itemUtils.get(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14),
			"&c&lDeny");

	private final LootProperties props = new LootProperties();

	public PurchaseGUI(LootPurchaser purchaser, int cost, Material skin, String loot, byte[] data) {
		Inventory inv = Bukkit.createInventory(null, 27, name);

		ItemStack info = Main.itemUtils.get(skin, "&6&lCost: &b$" + Main.ioUtils.getPrice(cost), "&d&lLoot: &a" + loot);

		for (int i = 0; i < inv.getSize(); i++) {
			if (i % 9 < 4)
				inv.setItem(i, accept);
			else if (i % 9 > 4)
				inv.setItem(i, deny);
			else if (i == 4)
				inv.setItem(i, info);
			else
				inv.setItem(i, Main.itemUtils.getSeparator());
		}

		props.set("purchaser", purchaser);
		props.set("inv", inv);
		props.set("data", data);
		props.set("cost", cost);
	}

	@Override
	protected void preOpenInventory(Player player) {
	}

	@Override
	protected void openInventory(Player player) {
		player.openInventory((Inventory) props.get("inv"));
	}

	@Override
	protected void closeInventory(Player player) {
		player.closeInventory();
	}

	@Override
	protected boolean inventoryClick(InventoryClickEvent e) {
		int pos = e.getSlot() % 9;
		if (pos == 4)
			return true;

		LootPurchaser purchaser = props.get("purchaser");
		Player player = (Player) e.getWhoClicked();

		purchaser.lock(player);
		closeInventory(player);

		int cost = props.get("cost");
		byte[] data = props.get("data");

		if (pos > 4)
			purchaser.deny(player, cost, data);
		else if (pos < 4)
			purchaser.accept(player, cost, data);

		return true;
	}

	@Override
	protected void inventoryClose(InventoryCloseEvent e) {
		((LootPurchaser) props.get("purchaser")).close((Player) e.getPlayer(), (byte[]) props.get("data"));
	}
}

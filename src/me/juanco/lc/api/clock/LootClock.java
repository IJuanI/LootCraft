package me.juanco.lc.api.clock;

import org.bukkit.scheduler.BukkitRunnable;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.core.Main;

public class LootClock extends BukkitRunnable {

	private static final ConcurrentSet<LootTask> tasks = new ConcurrentSet<LootTask>();

	public LootClock(Main core) {
		runTaskTimer(core, 20, 20);
	}

	@Override
	public void run() {
		for (LootTask task : tasks)
			if (task.flush())
				if (task.isPersistent())
					task.reset();
				else
					tasks.remove(task);
	}

	public static void addTask(LootTask task) {
		tasks.add(task);
	}
}

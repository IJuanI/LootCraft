package me.juanco.lc.api.clock;

public abstract class LootTask {

	private long delay;
	private final boolean persistent;
	private final boolean intensive;

	private Thread thread = null;

	private long elapsed = 0;

	public LootTask(long delay, boolean persistent) {
		this(delay, persistent, false);
	}

	public LootTask(long delay, boolean persistent, boolean intensive) {
		this.delay = delay;
		this.persistent = persistent;
		this.intensive = intensive;
	}

	protected boolean isPersistent() {
		return persistent;
	}

	protected void reset() {
		elapsed = 0;
	}

	protected boolean flush() {
		elapsed++;
		if (delay == elapsed)
			if (!intensive)
				run();
			else {
				if (thread == null)
					thread = new Thread(new IntensiveRunnable(this));
				thread.run();
			}
		else
			return false;
		return true;
	}

	public abstract void run();

	private class IntensiveRunnable implements Runnable {

		private final LootTask task;

		private IntensiveRunnable(LootTask task) {
			this.task = task;
		}

		@Override
		public void run() {
			task.run();
		}
	}
}

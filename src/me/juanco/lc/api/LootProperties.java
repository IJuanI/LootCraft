package me.juanco.lc.api;

import java.util.HashMap;

public class LootProperties {

	private final HashMap<String, Object> props = new HashMap<String, Object>();

	private String root = null;

	public void setRoot(String root) {
		this.root = root + ".";
	}

	public void resetRoot() {
		root = null;
	}

	public void set(String id, Object object) {
		props.put(fullID(id), object);
	}

	public <T> T get(Object obj) {
		return get(obj.toString());
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String id) {
		return (T) props.get(fullID(id));
	}

	public <T> T remove(Object obj) {
		return get(obj.toString());
	}

	@SuppressWarnings("unchecked")
	public <T> T remove(String id) {
		return (T) props.remove(fullID(id));
	}

	private String fullID(String id) {
		return root != null ? root + id : id;
	}
}

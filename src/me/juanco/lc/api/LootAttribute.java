package me.juanco.lc.api;

import java.lang.reflect.Field;
import java.util.UUID;

import javax.annotation.Nonnull;

import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import net.minecraft.server.v1_8_R3.NBTTagList;

public class LootAttribute {

	private final NBTTagList attributes;
	private final net.minecraft.server.v1_8_R3.ItemStack nmsStack;

	private static Field handle = null;
	private boolean unbreakable = false;

	public LootAttribute(@Nonnull ItemStack item) {
		if (item instanceof CraftItemStack)
			nmsStack = getNmsStack((CraftItemStack) item);
		else
			nmsStack = CraftItemStack.asNMSCopy(item);

		NBTTagCompound tag;

		if (nmsStack.getTag() == null)
			nmsStack.setTag(tag = new NBTTagCompound());
		else
			tag = nmsStack.getTag();

		if (tag.hasKey("AttributeModifiers"))
			attributes = tag.getList("AttributeModifiers", 10);
		else {
			attributes = new NBTTagList();
			tag.set("AttributeModifiers", attributes);
		}

		tag.set("HideFlags", new NBTTagInt(62));
	}

	public LootAttribute add(@Nonnull String name, double amount, @Nonnull AttributeType type) {
		NBTTagCompound data = new NBTTagCompound();
		data.setString("Name", name);
		data.setDouble("Amount", amount);
		data.setString("AttributeName", type.id);
		data.setInt("Operation", 0);
		UUID uuid = UUID.randomUUID();
		data.setLong("UUIDLeast", uuid.getLeastSignificantBits());
		data.setLong("UUIDMost", uuid.getMostSignificantBits());

		attributes.add(data);

		return this;
	}

	public LootAttribute unbreakable() {
		unbreakable = true;
		return this;
	}

	public ItemStack getStack() {
		if (unbreakable) {
			ItemStack item = CraftItemStack.asCraftMirror(nmsStack);

			ItemMeta meta = item.getItemMeta();
			meta.spigot().setUnbreakable(true);
			item.setItemMeta(meta);

			return item;
		}
		return CraftItemStack.asCraftMirror(nmsStack);
	}

	private static net.minecraft.server.v1_8_R3.ItemStack getNmsStack(CraftItemStack stack) {
		try {
			Field field = handle;

			if (field == null) {
				field = CraftItemStack.class.getDeclaredField("handle");
				field.setAccessible(true);
				handle = field;
			}

			return (net.minecraft.server.v1_8_R3.ItemStack) field.get(stack);
		} catch (Exception e) {
			throw new RuntimeException("Failed to read NMS stack.", e);
		}
	}

	public static enum AttributeType {
		Damage("generic.attackDamage"), Health("generic.maxHealth"), Speed("generic.movementSpeed"), Knockback(
				"generic.knockbackResistance");

		protected final String id;

		private AttributeType(String id) {
			this.id = id;
		}
	}
}

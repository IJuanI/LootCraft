package me.juanco.lc.api;

public class Placeholders {

	public static final String payrollAmount = "payroll_amount";
	public static final String payrollTime = "payroll_time";
	public static final String money = "money";
	public static final String tokens = "tokens";
	public static final String itemLevel = "item_level";
	public static final String win = "win";
	public static final String lose = "lose";
}

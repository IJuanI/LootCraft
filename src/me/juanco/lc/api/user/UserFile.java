package me.juanco.lc.api.user;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import io.netty.util.internal.ConcurrentSet;
import me.juanco.lc.api.LootProperties;
import me.juanco.lc.core.Main;

public class UserFile {

	private static final Set<UserFile> files = new ConcurrentSet<UserFile>();
	static File folder = new File(Main.folder.getPath() + "/users/");

	public final FileConfiguration file;
	public final File ioFile;
	private final UUID uuid;

	public final LootProperties props = new LootProperties();

	private UserFile(UUID uuid) {
		ioFile = new File(folder.getPath(), uuid.toString() + ".data");

		if (!folder.exists())
			folder.mkdirs();

		if (!ioFile.exists())
			try {
				ioFile.createNewFile();
			} catch (IOException ex) {
				Bukkit.getConsoleSender().sendMessage("Error when creating user file for " + uuid);
			}

		file = YamlConfiguration.loadConfiguration(ioFile);
		this.uuid = uuid;

		defaults();
	}

	public void save() {
		try {
			file.save(ioFile);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public UUID getUUID() {
		return uuid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UUID)
			return uuid.equals(obj);
		return super.equals(obj);
	}

	public static UserFile getFile(UUID uuid) {
		int index = Main.ioUtils.indexOf(files, uuid);
		if (index > -1)
			return Main.ioUtils.get(files, index);
		else {
			UserFile file = new UserFile(uuid);
			files.add(file);
			return file;
		}
	}

	public static UserFile[] getFiles() {
		return files.toArray(new UserFile[0]);
	}

	protected static void unload(UUID uuid) {
		int index = Main.ioUtils.indexOf(files, uuid);
		if (index > -1) {
			UserFile file = Main.ioUtils.get(files, index);
			files.remove(file);
			file.save();
		}

	}

	public static void disable() {
		for (UserFile file : files)
			file.save();
		files.clear();
	}
	
	private void defaults() {
		long[] currencies = Main.money.getAmounts(uuid);

		int win = 0;
		int lose = 0;

		props.set("money", Main.ioUtils.format(currencies[0]));
		props.set("tokens", Main.ioUtils.format(currencies[1]));
		props.set("win", String.valueOf(win));
		props.set("lose", String.valueOf(lose));
	}
}

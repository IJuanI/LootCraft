package me.juanco.lc.api.user;

import java.sql.ResultSet;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import me.juanco.lc.api.BanManager;
import me.juanco.lc.api.LootListener;

public class APIJoinListener extends LootListener {

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		UserFile.unload(e.getPlayer().getUniqueId());

		getMain().playerQuit(e.getPlayer());
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		UserFile.getFile(e.getPlayer().getUniqueId());

		getMain().playerJoin(e.getPlayer());
	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent e) {
		BanManager bans = getMain().getBans();

		ResultSet rs = bans.contains(e.getPlayer());

		if (rs != null) {
			e.setResult(Result.KICK_BANNED);
			e.setKickMessage(bans.getReason(rs));
		}
	}
}

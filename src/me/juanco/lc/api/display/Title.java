package me.juanco.lc.api.display;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class Title {

	private static final PacketPlayOutTitle clear = new PacketPlayOutTitle(EnumTitleAction.TITLE,
			ChatSerializer.a("{text:\"\"}"), 0, 0, 0);

	private final PlayerConnection conn;
	private final PlayerConnection[] conns;

	private PacketPlayOutTitle title = null, subTitle = null, timing = null;

	private Title(Player player) {
		conn = ((CraftPlayer) player).getHandle().playerConnection;
		conns = null;
	}

	private Title(Player... players) {
		conns = new PlayerConnection[players.length];

		for (int i = 0; i < players.length; i++)
			conns[i] = ((CraftPlayer) players[i]).getHandle().playerConnection;

		conn = null;
	}

	private Title setTitle(String title) {
		StringBuilder message = new StringBuilder("{text:\"");
		message.append(ChatColor.translateAlternateColorCodes('&', title));
		message.append("\"}");

		this.title = new PacketPlayOutTitle(EnumTitleAction.TITLE, ChatSerializer.a(message.toString()));

		return this;
	}

	private Title setSubTitle(String subTitle) {
		StringBuilder message = new StringBuilder("{text:\"");
		message.append(ChatColor.translateAlternateColorCodes('&', subTitle));
		message.append("\"}");

		this.subTitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, ChatSerializer.a(message.toString()));

		return this;
	}

	private Title setTiming(int in, int show, int out) {
		timing = new PacketPlayOutTitle(in, show, out);

		return this;
	}

	private void show() {
		if (title != null)
			send(title);

		if (subTitle != null)
			send(subTitle);

		if (timing != null)
			send(timing);
	}

	private void clear() {
		send(clear);
	}

	private void send(PacketPlayOutTitle packet) {
		if (conn != null)
			conn.sendPacket(packet);
		else
			for (PlayerConnection conn : conns)
				conn.sendPacket(packet);
	}

	public static void send(Player player, String title) {
		new Title(player).setTitle(title).show();
	}

	public static void send(Player player, String title, String subTitle) {
		new Title(player).setTitle(title).setSubTitle(subTitle).show();
	}

	public static void send(Player player, String title, String subTitle, int in, int show, int out) {
		new Title(player).setTitle(title).setSubTitle(subTitle).setTiming(in, show, out).show();
	}

	public static void send(Player player, String title, int in, int show, int out) {
		new Title(player).setTitle(title).setTiming(in, show, out).show();
	}

	public static void send(String title, Player... players) {
		new Title(players).setTitle(title).show();
	}

	public static void send(String title, String subTitle, Player... players) {
		new Title(players).setTitle(title).setSubTitle(subTitle).show();
	}

	public static void send(String title, String subTitle, int in, int show, int out, Player... players) {
		new Title(players).setTitle(title).setSubTitle(subTitle).setTiming(in, show, out).show();
	}

	public static void send(String title, int in, int show, int out, Player... players) {
		new Title(players).setTitle(title).setTiming(in, show, out).show();
	}

	public static void clear(Player player) {
		new Title(player).clear();
	}

	public static void clear(Player... players) {
		new Title(players).clear();
	}
}
package me.juanco.lc.api.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.Driver;

public class MySQL {

	private final Connection conn;
	private Statement stmt;

	public MySQL(Credentials credentials) throws SQLException {
		DriverManager.registerDriver(new Driver());
		conn = DriverManager.getConnection(credentials.getURL());
	}

	public void open() throws SQLException {
		stmt = conn.createStatement();
	}

	public boolean isOpen() throws SQLException {
		return stmt != null && !stmt.isClosed();
	}

	public void close() throws SQLException {
		stmt.close();
		stmt = null;
	}

	public ResultSet query(String sql) throws SQLException {
		return stmt.executeQuery(sql);
	}

	public int update(String sql) throws SQLException {
		return stmt.executeUpdate(sql);
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return conn.prepareStatement(sql);
	}

	public void exit() throws SQLException {
		conn.close();
	}
}

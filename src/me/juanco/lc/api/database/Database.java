package me.juanco.lc.api.database;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;

import me.juanco.lc.chat.Message;
import me.juanco.lc.chat.paths.Messages;

public class Database {

	static List<Table> tables = new ArrayList<Table>();

	public static void addTable(Table table) {
		tables.add(table);
	}

	private final MySQL sql;

	private PreparedStatement delete;
	private PreparedStatement drop;
	private PreparedStatement count;

	private PreparedStatement selectEcon;
	private PreparedStatement insertEcon;

	private PreparedStatement selectBan;
	private PreparedStatement insertBan;
	private PreparedStatement deleteBan;

	public Database(Credentials credentials) {
		try {
			sql = new MySQL(credentials);

			sql.open();

			for (Table table : tables)
				sql.update(table.getStatement());

			sql.close();

			selectEcon = sql.prepareStatement("select * from Economy where uuid = ?;");
			insertEcon = sql.prepareStatement("insert into Economy values(?, 0, 0);");

			selectBan = sql.prepareStatement("select * from Bans where uuid = ?;");
			insertBan = sql.prepareStatement("insert into Bans values(?, ?, ?);");
			deleteBan = sql.prepareStatement("delete from Bans where uuid = ?;");

			delete = sql.prepareStatement("delete from ? where ? = ?;");
			drop = sql.prepareStatement("drop table ?;");
			count = sql.prepareStatement("select COUNT(*) from ?;");

		} catch (SQLException ex) {
			new Message(Bukkit.getConsoleSender()).msg(Messages.ERRORS.db);
			ex.printStackTrace();
			throw new IllegalArgumentException();
		} catch (Exception ex) {
			Bukkit.getConsoleSender().sendMessage("Install JBDC Driver!");
			throw new IllegalArgumentException();
		}
	}

	public void exit() throws SQLException {
		sql.exit();
	}

	// Economy

	public ResultSet selectEcon(String uuid) {
		try {
			selectEcon.setString(1, uuid);
			return selectEcon.executeQuery();
		} catch (SQLException e) {
		}
		return null;
	}

	public boolean insertEcon(String uuid) {
		try {
			insertEcon.setString(1, uuid);
			insertEcon.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	// Bans

	public ResultSet selectBan(String uuid) {
		try {
			selectBan.setString(1, uuid);
			return selectBan.executeQuery();
		} catch (SQLException e) {
		}
		return null;
	}

	public boolean insertBan(String uuid, String reason, Date time) {
		try {
			insertBan.setString(1, uuid);
			insertBan.setString(2, reason);
			insertBan.setDate(3, time);
			insertBan.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public boolean deleteBan(String uuid) {
		try {
			deleteBan.setString(1, uuid);
			deleteBan.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	// General

	public boolean update(String database, String field, Object value, String field2, Object value2) {
		try {
			String v = value instanceof String ? "\'" + value + '\'' : value.toString();
			String v2 = value2 instanceof String ? "\'" + value2 + '\'' : value2.toString();

			sql.open();
			sql.update(String.format("update %s set %s=%s where %s=%s", database, field, v, field2, v2));
			return true;
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				sql.close();
			} catch (SQLException e) {
			}
		}
	}

	public boolean delete(String database, String field, String value) {
		try {
			delete.setString(1, database);
			delete.setString(2, field);
			delete.setString(3, value);
			delete.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public int countRows(String database) {
		try {
			count.setString(1, database);
			ResultSet rs = count.executeQuery();

			if (rs != null && !rs.wasNull() && !rs.isClosed() && rs.next())
				return rs.getInt(0);
			return -1;
		} catch (SQLException ex) {
			return -1;
		}
	}

	public boolean drop(String database) {
		try {
			drop.setString(1, database);
			drop.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
}

package me.juanco.lc.api.database;

import java.util.ArrayList;
import java.util.List;

public class Table {

	protected String name, key;
	protected List<String> fields = new ArrayList<String>();

	public Table(String name, String key) {
		this.name = name;
		this.key = key;
	}

	public String getStatement() {
		StringBuilder stmt = new StringBuilder("create table IF NOT EXISTS ");
		stmt.append(name);
		stmt.append("(");

		for (String field : fields)
			stmt.append(field).append(", ");

		stmt.append("PRIMARY KEY (").append(key).append("));");

		return stmt.toString();
	}

	public void addField(String name, String Type, long cap, boolean nullAllowed) {
		fields.add(createField(name, Type, nullAllowed, true, cap, false));
	}

	public void addField(String name, String Type, long cap, boolean nullAllowed, boolean unique) {
		fields.add(createField(name, Type, nullAllowed, true, cap, unique));
	}

	public void addField(String name, Object Type, boolean nullAllowed) {
		addField(name, Type.getClass(), nullAllowed);
	}

	public void addField(String name, Class<?> Type, boolean nullAllowed) {
		addField(name, Type.getSimpleName(), nullAllowed);
	}

	public void addField(String name, String Type, boolean nullAllowed) {
		fields.add(createField(name, Type, nullAllowed, false, 0, false));
	}

	public void addField(String name, String Type, boolean nullAllowed, boolean unique) {
		fields.add(createField(name, Type, nullAllowed, false, 0, unique));
	}

	public void autoIncrement(String name) {
		StringBuilder sb = new StringBuilder(name);

		sb.append(" bigint NOT NULL AUTO_INCREMENT");

		fields.add(sb.toString());
	}

	String createField(String name, String Type, boolean nullAllowed, boolean useCap, long cap, boolean unique) {
		StringBuilder sb = new StringBuilder(name);

		String type = getTypeName(Type, cap);
		sb.append(" ").append(type);
		if (useCap)
			sb.append("(").append(cap).append(")");
		if (unique)
			sb.append(" UNIQUE");
		if (!nullAllowed)
			sb.append(" NOT NULL");

		return sb.toString();
	}

	String getTypeName(String Type, long cap) {
		switch (Type) {
		case "Byte":
			return "bit";
		case "Short":
			return "smallint";
		case "Integer":
			return "integer";
		case "Long":
			return "bigint";
		case "Float":
			return "float";
		case "Double":
			return "double";
		case "Boolean":
			return "bool";
		case "Binary":
			return "binary";
		case "Date":
			return "datetime";
		case "Time":
			return "time";
		case "Year":
			return "year";
		case "Character":
			return "Char";
		default:
			if (cap < 256)
				return "varchar";
			else if (cap < 65536)
				return "text";
			else if (cap < 16777216)
				return "mediumtext";
			else if (cap < 4294967296L)
				return "longtext";
			else
				throw new IllegalArgumentException("Can't store more than 4GB of text!");
		}
	}

	public void addEnumField(String name, boolean nullAllowed, String... values) {
		StringBuilder sb = new StringBuilder(name).append(" ENUM(");
		
		boolean first = true;
		for (String value : values) {
			if (first)
				first = false;
			else sb.append(", ");
			
			sb.append(value);
		}
		
		sb.append(")");
		
		if (!nullAllowed)
			sb.append(" NOT NULL");
		
		fields.add(sb.toString());
	}
}

package me.juanco.lc.api;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.entity.Player;

import me.juanco.lc.chat.Message;
import me.juanco.lc.core.Main;
import me.juanco.lc.utils.IOUtils;

public class BanManager {

	private final Main core;
	private final String timeFormat = "\n&c&lTime Left: &f%s";

	public BanManager(Main core) {
		this.core = core;
	}

	public boolean ban(Player player, String reason, long seconds) {
		UUID uuid = player.getUniqueId();
		try {
			ResultSet rs = get(uuid);
			if (validate(rs))
				return true;
			else {
				if (reason.length() > 64)
					reason = reason.substring(0, 64);

				Date time = new Date(System.currentTimeMillis() + seconds * 1000L);

				rs = insert(uuid, reason, time);

				if (validate(rs))
					return false;
			}
		} catch (SQLException ex) {
		}

		return true;
	}

	public String getReason(Player player) {
		return getReason(get(player.getUniqueId()));
	}

	public String getReason(ResultSet rs) {
		try {
			if (validate(rs)) {
				String reason = rs.getString("Reason");

				Date pardonDate = rs.getDate("Time");
				long left = pardonDate.getTime() - System.currentTimeMillis();

				StringBuilder time = new StringBuilder();

				if (left < IOUtils.secondMs)
					time.append("NONE");
				else {
					int timeType = 0;

					while (left > IOUtils.secondMs) {
						if (timeType != 0)
							time.append(", ");

						if (left > IOUtils.monthMs) {
							long months = left / IOUtils.monthMs;
							left -= months * IOUtils.monthMs;

							time.append(months).append(months > 1 ? " Months" : " Month");

							timeType = 1;
						} else if (left > IOUtils.dayMs) {
							long days = left / IOUtils.dayMs;
							left -= days * IOUtils.dayMs;

							time.append(days).append(days > 1 ? " Days" : " Day");

							if (timeType > 0)
								break;

							timeType = 2;
						} else if (left > IOUtils.hourMs) {
							long hours = left / IOUtils.hourMs;
							left -= hours * IOUtils.hourMs;

							time.append(hours).append(hours > 1 ? " Hours" : " Hour");

							if (timeType > 1)
								break;

							timeType = 3;
						} else if (left > IOUtils.minuteMs) {
							long minutes = left / IOUtils.minuteMs;
							left -= minutes * IOUtils.minuteMs;

							time.append(minutes).append(minutes > 1 ? " Minutes" : " Minute");

							if (timeType > 2)
								break;

							timeType = 4;
						} else {
							long seconds = left / IOUtils.secondMs;
							left -= seconds * IOUtils.secondMs;

							time.append(seconds).append(seconds > 1 ? " Seconds" : " Second");

							break;
						}
					}
				}

				reason = reason + String.format(timeFormat, time.toString());

				return Message.t(reason);
			}
		} catch (SQLException ex) {
		}
		return null;
	}

	public boolean pardon(Player player) {
		UUID uuid = player.getUniqueId();

		try {
			if (contains(uuid) && core.getDB().deleteBan(uuid.toString()))
				return true;
		} catch (SQLException ex) {
		}

		return false;
	}

	ResultSet get(UUID uuid) {
		if (core.getDB() == null || uuid == null)
			return null;
		return core.getDB().selectBan(uuid.toString());
	}

	public ResultSet contains(Player player) {
		try {
			ResultSet rs = get(player.getUniqueId());

			if (validate(rs)) {
				Date time = rs.getDate("Time");
				if (time.getTime() > System.currentTimeMillis())
					return rs;
			}
			return null;
		} catch (SQLException ex) {
			return null;
		}
	}

	boolean contains(UUID uuid) throws SQLException {
		ResultSet rs = get(uuid);

		if (validate(rs)) {
			Date time = rs.getDate("Time");
			if (time.getTime() > System.currentTimeMillis())
				return true;
		}
		return false;
	}

	boolean validate(ResultSet rs) throws SQLException {
		return rs != null && !rs.wasNull() && !rs.isClosed() && rs.getMetaData() != null && rs.next();
	}

	ResultSet insert(UUID uuid, String reason, Date time) throws SQLException {
		if (core.getDB() == null || uuid == null)
			return null;

		core.getDB().insertBan(uuid.toString(), reason, time);
		return get(uuid);
	}
}

package me.juanco.lc.api;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import me.juanco.lc.core.Main;

public class LootListener implements Listener {

	private static Plugin pl;

	public static void setup(Plugin plugin) {
		pl = plugin;
	}

	public LootListener() {
		Bukkit.getPluginManager().registerEvents(this, pl);
	}

	protected Main getMain() {
		return (Main) pl;
	}
}

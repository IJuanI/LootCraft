package me.juanco.lc.impl;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import me.juanco.lc.packets.PacketListener;
import net.minecraft.server.v1_8_R3.Item;
import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import net.minecraft.server.v1_8_R3.PacketPlayOutSetSlot;

public class HideAttributes extends PacketListener {

	private final Field c;
	private final NBTTagInt hide = new NBTTagInt(62);

	public HideAttributes() {
		super("PacketPlayOutSetSlot");

		try {
			c = PacketPlayOutSetSlot.class.getDeclaredField("c");
			c.setAccessible(true);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean listen(Object message, Player target) {
		try {
			ItemStack item = (ItemStack) c.get(message);
			
			if (item == null)
				return false;

			if (item.hasTag()) {
				if (item.getTag().hasKey("AttributeModifiers"))
					if (!item.getTag().hasKey("HideFlags") || !item.getTag().get("HideFlags").equals(hide)) {
						item.getTag().set("HideFlags", hide);
						return false;
					}
			} else {
				int id = Item.getId(item.getItem());

				if (id == 256 || id == 257 || id == 258 || id > 267 && id < 280 || id > 282 && id < 287) {
					item.setTag(new NBTTagCompound());

					if (!item.getTag().hasKey("HideFlags") || !item.getTag().get("HideFlags").equals(hide))
						item.getTag().set("HideFlags", hide);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}

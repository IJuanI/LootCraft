package me.juanco.lc.impl;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.juanco.lc.api.clock.LootTask;
import me.juanco.lc.api.user.UserFile;
import me.juanco.lc.core.Main;

public class FeatherUpdateTask extends LootTask {

	public FeatherUpdateTask() {
		super(26, true, true);
	}

	@Override
	public void run() {
		for (UserFile file : UserFile.getFiles())
			update(file);
	}

	public FeatherUpdateTask init() {
		for (Player player : Bukkit.getOnlinePlayers())
			UserFile.getFile(player.getUniqueId());

		return this;
	}

	private void update(UserFile file) {
		UUID uuid = file.getUUID();

		long[] currencies = Main.money.getAmounts(uuid);

		int win = 0;
		int lose = 0;

		file.props.set("money", Main.ioUtils.format(currencies[0]));
		file.props.set("tokens", Main.ioUtils.format(currencies[1]));
		file.props.set("win", String.valueOf(win));
		file.props.set("lose", String.valueOf(lose));
	}
}
